---
title: "Créer du contenu"
description: "Comment écrire ses pages et son contenu"
sort_by: "weight"
weight: 4
date: 2022-09-01
extra:
  parent: "prise_en_main/web.md"
---

Votre site est désormais complètement configuré. Avant de publier du contenu, il faut écrire celui-ci !

Vous pouvez, au choix, [l'écrire à la main](@/prise_en_main/à-la-main.md), ou vous [faire assister par un générateur](@/prise_en_main/avec-un-générateur.md).
