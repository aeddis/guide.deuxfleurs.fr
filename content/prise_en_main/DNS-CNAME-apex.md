---
title: "CNAME à l'apex"
description: "Les CNAME à l'apex d'une zone DNS"
date: 2023-04-19
weight: 3
extra:
  parent: "prise_en_main/mettre-place-DNS.md"
---

# Le problème

Dans le protocole DNS, il n'est pas possible de mettre un champ CNAME à l'apex d'une zone.

Concrètement, qu'est-ce que ça veut dire ? Si vous gérez la zone `example.com` et que vous
aimeriez faire pointer ce nom vers `garage.deuxfleurs.fr` pour que Deuxfleurs héberge votre
site web, la solution naturelle serait de configurer un CNAME. Dans un fichier de zone,
cela ressemblerait à :

     @    10800  IN  CNAME  garage.deuxfleurs.fr.

Hors, cela est interdit par le protocole DNS. Pourquoi donc ? Parce qu'un CNAME s'applique
à tous les types d'enregistrements, pas simplement les `A` (adresse IPv4) et `AAAA` (adresse IPv6).
Ainsi, la redirection du CNAME s'appliquerait également aux enregistrements comme `NS` et `MX`, ce qui
rentrerait en conflit avec ces enregistrements déjà existants dans votre zone.

[Une explication technique plus détaillée est disponible sur le site de l'ISC](https://www.isc.org/blogs/cname-at-the-apex-of-a-zone/).

# Solutions possibles

## Implémentations non-standard : ALIAS et CNAME flattening

Certains hébergeurs et logiciels DNS proposent une solution non-standard à ce problème.

Gandi permet de configurer un champ `ALIAS` à l'apex d'une zone qui pointe vers un autre
nom comme `garage.deuxfleurs.fr`. Cet enregistrement `ALIAS` ne sera jamais renvoyé directement
aux clients DNS : à la place, ce sont les serveurs DNS de Gandi qui vont dynamiquement consulter
les enregistrements `A` et `AAAA` de `garage.deuxfleurs.fr`, puis les renvoyer dans la requête
initiale pour `example.com`.

De manière confuse, [Cloudflare permet de mettre un enregistrement CNAME à l'apex d'une zone](https://blog.cloudflare.com/introducing-cname-flattening-rfc-compliant-cnames-at-a-domains-root/).
Comment font-ils, puisque c'est interdit ? En fait, ils utilisent exactement la même technique que Gandi, mais
ils ont choisi de réutiliser le terme `CNAME` là où Gandi appelle cela un `ALIAS`. C'est un choix de nom
très discutable puisqu'il ne s'agit pas vraiment d'un CNAME.

Très peu d'implémentations logicielles libre de serveur DNS faisant autorité proposent cette fonctionnalité.
Les logiciels classiques Bind, NSD et Knot ne l'implémentent pas. Parmi les autres logiciels couramment utilisés,
seuls [PowerDNS Authoritative Nameserver](https://doc.powerdns.com/authoritative/index.html) et [CoreDNS](https://coredns.io/) l'implémentent :
[PowerDNS avec la syntaxe ALIAS comme chez Gandi](https://doc.powerdns.com/authoritative/guides/alias.html), et
[CoreDNS avec la syntaxe CNAME abusive comme chez Cloudflare](https://coredns.io/explugins/alias/).

Il faut noter que c'est une technique plutôt complexe à implémenter correctement, puisqu'elle nécessite que le serveur DNS
faisant autorité joue un rôle de récurseur, ce qui n'est pas nécessaire en temps normal.

Au final, c'est une solution ad-hoc qui est très spécifique à certains fournisseurs
et logiciels, avec même plusieurs syntaxes possibles. Elle présente donc un risque fort
d'enfermement auprès de ces fournisseurs ou logiciels.

## En cours de standardisation : SVCB et HTTPS

Deux nouveaux types d'enregistrements DNS sont [en cours de standardisation : SVCB et HTTPS](https://datatracker.ietf.org/doc/draft-ietf-dnsop-svcb-https/).
Ce travail en cours couvre un spectre plus large, mais il résout en particulier
ce problème de CNAME à l'apex.

En mars 2023, ce document de travail en est à sa 12ème révision et n'a pas encore été publié
officiellement comme un standard (RFC). Cependant, il semblerait que [certains navigateurs web et certains logiciels
serveurs DNS aient déjà commencé à implémenter cette spécification depuis 2021 environ](https://serverfault.com/a/1075524).

## Solutions recommandées chez Deuxfleurs

Vous êtes responsable de votre nom de domaine, donc n'hésitez pas à expérimenter si
vous le souhaitez ! Si vous avez des retours sur l'utilisation de SVCB/HTTPS, nous sommes intéressés.

Cependant, Deuxfleurs recommande pour l'instant les solutions suivantes :

- utiliser un sous-domaine lorsque cela est possible

- sinon, utiliser l'implémentation non-standard de Gandi ou Cloudflare

En effet, la solution SVCB/HTTPS est encore en cours de standardisation en 2023
et va mettre de nombreuses années avant d'être déployée sur tous les terminaux.
Compter uniquement sur cette solution, c'est écarter de fait tous les clients un
peu anciens (vieux téléphones Android, anciennes versions de Windows ou d'Ubuntu
pas mises à jour), alors que Deuxfleurs cherche à éviter l'obsolescence et faire
en sorte que ces appareils restent utilisables le plus longtemps possible.
