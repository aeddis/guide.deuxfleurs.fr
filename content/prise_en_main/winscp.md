---
title: "WinSCP"
description: "Publier avec WinSCP"
weight: 40
extra:
  parent: 'prise_en_main/publier-le-contenu.md'
---


# Installation

Commencez par télécharger l'outil [WinSCP](https://winscp.net/eng/download.php).

![winscp_dl.png](/img/winscp_dl.png)

Installez le logiciel.

# Configuration

Lancez le. La fenêtre de connexion suivante devrait apparaître :

![](/img/winscp_login.png)

Vous devez :
  1. Vérifier que c'est bien "Nouveau site" qui est sélectionné
  2. Dans *protocole de fichier*, vous devez choisir *Amazon S3*
  3. Dans *nom d'hôte*, vous devez mettre `garage.deuxfleurs.fr`
  4. Dans *numéro de port*, vous devez mettre (ou plutôt laisser) `443`
  5. Dans *id de clé d'accès*, vous devez mettre l'identifiant de votre clé d'accès, exemple : `GK...`
  6. Dans *clé d'accès secrète*, vous devez mettre votre clé d'accès secrète.
  7. Maintenant, cliquez sur le bouton *Avancé*. (Cliquez bien sur le mot "Avancé" et non sur la flèche à droite). Une fenêtre s'ouvre :

![](/img/winscp_avance.png)

  1. Dans le menu de gauche, cliquez sur *S3*
  2. Dans la partie de droite, pour *Région par défaut*, inscrivez au clavier *garage*
  3. Toujours à droite, dans *Style URL*, choisissez *Chemin*
  4. Cliquez sur OK
  
Vous voilà de retour sur la fenêtre de connexion :

![](/img/winscp_sauvegarder.png)

 1. Cliquez sur *Sauver* (cliquez bien sur le texte et non sur la flèche noire à droite)
 
La fenêtre suivante apparaît :

![](/img/winscp_session.png)

 1. Dans *Enregistrer la sessions sous :*, donnez un nom qui identifie bien votre site web, c'est ce qui vous permettra de l'identifier dans la liste de connexion.
 2. Pour la case *Enregistrer le mot de passe*, nous vous conseillons de la cocher sauf si vous êtes sur un ordinateur public (bibliothèque, cybercafé, etc.) ou au travail.
 3. Pour la case *Créer un raccourci sur le bureau*, nous vous conseillons de la cocher, vous pourrez alors mettre votre site en ligne en un seul clic (ou presque) depuis votre bureau.
 4. Cliquez sur OK
 
Vous pouvez maintenant cliquer sur "Connexion".

# Envoyer votre site web

À gauche, naviguez jusqu'au dossier de votre site web.
Faites en un glisser déposer à droite à l'intérieur du dossier qui contient le nom de votre site web.

![commander.png](/img/winscp_commander.png)
