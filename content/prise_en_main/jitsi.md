---
title: "Jitsi"
description: "Visioconférence avec Jitsi"
weight: 40
extra:
  parent: 'prise_en_main/_index.md'
---

# Accéder à Jitsi

-   [🌐 Accéder via le navigateur](https://jitsi.deuxfleurs.fr)
-   [📥 Application Android (F-Droid)](https://f-droid.org/en/packages/org.jitsi.meet/)
-   [📥 Application Android (Google Play Store)](https://play.google.com/store/apps/details?id=org.jitsi.meet&hl=fr)
-   [📥 Application iOS](https://apps.apple.com/fr/app/jitsi-meet/id1165103905)

# Bien s'entendre pendant la réunion

Quelques conseils pour bien s'entendre pendant une réunion et passer un bon moment.

**Améliorez le son :** l'idéal est d'avoir un casque avec microphone ou des écouteurs avec microphone pour capter le son au plus près de votre bouche et empêcher l'écho (que votre microphone capte ce qui sort sur les hauts parleurs). Si vous n'avez pas de casques ou d'écouteurs ou que vous êtes plusieurs, les ordinateurs portables récents captent mieux le son que les anciens, et généralement votre téléphone captera mieux le son que votre ordinateur. De plus, si vous êtes plusieurs, vous devez savoir que les microphones sont directifs : si vous êtes proches et bien en face de l'ordinateur, on vous entendra bien, sinon on ne vous entendra pas du tout ! En groupe, tournez l'ordinateur vers la personne qui parle ou mettez-vous bien en face pour parler !

**Améliorez votre connexion :** la visioconférence est très sensible à la qualité de votre connexion internet. Si vous le pouvez, connectez votre ordinateur en filaire (cable ethernet) à votre *box* (routeur internet). Si vous souhaitez rester en sans-fil (wifi), essayez de vous rapprocher de votre *box*. Le type de connexion internet influencera également la qualité de votre visioconférence : la fibre est idéale, l'ADSL ou les réseaux mobiles (4G et 3G) sont plus incertains.

**Réduisez vos usages :** Activer la vidéo peut causer des interruptions ou dégrader la qualité du son. Si vous n'avez pas besoin de la vidéo, désactivez là ou garder là seulement pour certains participants. Si vous ne parlez pas, vous pouvez également couper votre microphone. Il est possible de passer en mode "talkie walkie" sur ordinateur avec la touche espace. Vous maintenez la touche espace, votre micro est activé. Vous arrêtez d'appuyer sur la touche espace, votre micro est coupé.

Si vous appliquez ces conseils, vous devriez arriver à communiquer sans peine. Bonnes communications !
