---
title: "Cryptpad"
description: "Cryptpad"
weight: 50
extra:
  parent: 'prise_en_main/_index.md'
---

# Accès à Cryptpad

-   [🌐 Accéder via le navigateur](https://pad.deuxfleurs.fr)

# Traitement de texte

Utilisez l'application "Texte" (ou "Code" si vous connaissez Markdown)

# Sondages / Prise de Rendez-vous / Enquêtes

Utilisez l'application Formulaire

# Gestion de projet / Listes de choses à faire

Utilisez l'application Kanban

# Autre

Les autres outils ne sont pas recommandés car on ne les utilise pas,
certains sont connus pour être buggés et lent comme le tableur.
Utilisez les à vos risques et périls !
