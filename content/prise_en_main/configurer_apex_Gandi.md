---
title: "Configurer l'apex chez Gandi"
description: "Paramétrer l'entrée DNS directement sur le nom de domaine de base"
date: 2023-12-04
weight: 1
extra:
  parent: "prise_en_main/mettre-place-DNS.md"
---

Supposons que vous avez loué `camille-michu.fr` chez Gandi.

D'abord, rendez-vous dans votre espace utilisateur Gandi, puis de là cliquez dans le menu à gauche sur «Nom de domaine» :

![dns1.png](/img/apex_gandi_1.png)

Une fois sur la page «Nom de domaines», votre nom de domaine devrait apparaître dans la liste, ici «camille-michu.fr». Cliquez dessus :
  
![dns2.png](/img/apex_gandi_2.png)

Dans le menu du haut, sélectionnez «Enregistrements DNS» :

![dns3.png](/img/apex_gandi_3.png)

Ensuite cliquez sur le bouton «Ajouter un enregistrement» :

![dns4.png](/img/apex_gandi_4.png)

Choisissez alors «ALIAS» comme type, et `garage.deuxfleurs.fr` comme nom d'hôte.

![dns5.png](/img/apex_gandi_5.png)

Maintenant que votre configuration DNS est réglée, vous pouvez [préparer votre contenu](@/prise_en_main/creer-du-contenu.md) !

> **Tiens bah d'ailleurs :** Deuxfleurs peut héberger vos e-mails [même s'ils portent un nom de domaine autre que `deuxfleurs.fr`](@/operations/email_personnalise.md).
