---
title: "Rclone Browser"
description: "Publier avec Rclone Browser"
weight: 30
date: 2021-11-25T14:31:35.570Z
dateCreated: 2021-11-25T14:02:30.997Z
extra:
  parent: "prise_en_main/publier-le-contenu.md"
---


Rclone browser est un outil simple qui vous permet d'accéder directement à Garage.

# Installation

Si vous êtes sous macOS ou Windows, vous pouvez télécharger la dernière version depuis la page [github de rclone-browser](https://github.com/kapitainsky/RcloneBrowser/releases) (descendez un peu pour voir les liens).

Sous Ubuntu (lubuntu, kubuntu, etc.), Debian et Linux Mint, cherchez et installez "Rclone Browser" dans la logithèque ("App Store") ou lancez directement depuis un terminal :

```bash
sudo apt-get install -y rclone-browser
```

---

Le paquet existe aussi pour d'autres distributions : 

```bash
sudo dnf install -y rclone-browser # fedora
sudo yay -S rclone-browser # arch linux
sudo nix-env -iA nixos.rclone-browser # nix os
```

# Premier lancement

Avant de lancer Rclone Browser, ouvrez un éditeur de texte.
Moi j'utilise "Éditeur de texte" sous Gnome aussi connu sous le nom de `gedit`. Sous Windows il s'appelle Bloc-Note Windows (ou `notepad.exe`).

![gedit.png](/img/rclone_gedit.png)

Entrez le texte suivant dedans, tout en remplaçant bien les lignes 5 et 6 par vos identifiants communiqués précédemment :

```toml
[garage]
type = s3
provider = Other
env_auth = false
access_key_id = <REMPLACEZ PAR VOTRE IDENTIFIANT DE CLE D'ACCESS>
secret_access_key = <REMPLACEZ PAR VOTRE CLE D'ACCES SECRETE>
region = garage
endpoint = garage.deuxfleurs.fr
bucket_acl = private
force_path_style = true
no_check_bucket = true
```

Maintenant enregistrez le à un endroit où vous ne le perdrez pas. Dans mon cas, j'ai choisi de le mettre dans Mon Dossier Personnel > Documents > Configuration et je l'ai appelé `rclone.conf`.

![rclone-conf.png](/img/rclone_conf.png)

Maintenant lancez "Rclone Browser" en cherchant l'application dans votre menu d'application

![capture_d’écran_de_2021-11-25_14-43-03.png](/img/rclone_menu.png)

L'application devrait se lancer :

![rclone_init.png](/img/rclone_init.png)

Cliquez sur "File" (Fichiers) en haut à gauche, puis sur "Preferences...". La fenêtre suivante s'ouvre :

![rclone-conf2.png](/img/rclone_conf2.png)

Repérez la deuxième ligne intitulée `rclone.conf location:`. Tout au bout de la ligne, cliquez sur le bouton avec les trois points `[...]`. Vous aurez alors la possibilité d'indiquer à Rclone Browser où se trouve le fichier de configuration créé juste avant (pour ma part, je rappelle, "Dossier personnel > Documents > Configuration > rclone.conf", on voit que le chemin complet vers le fichier apparaît maintenant dans le champs texte.

Appuyez sur OK, et la fenêtre principale devrait maintenant ressembler à ça :

![rclone-s3.png](/img/rclone_s3.png)

Cliquez sur "S3 garage" puis cliquez sur "Open". Si vous voyez des dossiers apparaître, c'est gagné !

![rclone-conn.png](/img/rclone_conn.png)

Vous pouvez maintenant naviguer dans vos dossiers, envoyer des fichiers sur Garage ou en récupérer. Attention, vous ne pouvez pas créer de "nouveaux dossiers" à la racine, mais vous pouvez créer des sous-dossiers dans les dossiers existants préalablement créés pour vous.

# Utiliser comme un drive

Ce n'est pas forcément très pratique de devoir passer par cette interface pour envoyer ou récupérer des fichiers. Ce qui serait génial, c'est que ces dossiers apparaissent sur votre ordinateur comme des dossiers normaux.

Pour cela, sélectionnez un dossier racine, moi j'ai choisi `quentin.bibliotheque` puis cliquez sur le bouton "Mount".

![rclone-mount.png](/img/rclone_mount.png)

Un sélecteur de fichier s'ouvre alors. Je vais dans "Dossier Personnel > Documents", je créer un nouveau dossier que j'appelle "Distant". Je vais dedans et clique sur "Choisir" (ou Ok).

Maintenant, quand je vais dans un explorateur de fichier, je vois les fichiers sur Garage comme si ils étaient sur mon ordinateur.

![nautilus-distant.png](/img/rclone_nautilus.png)

Je peux aussi accéder à ces documents depuis mon logiciel préféré, comme ici "LibreOffice Calc" :

![capture_d’écran_de_2021-11-25_15-01-42.png](/img/rclone_calc.png)



