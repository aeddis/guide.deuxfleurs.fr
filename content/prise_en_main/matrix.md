---
title: "Matrix"
description: "Messagerie Instantanée avec Matrix"
date: "2022-03-09T19:13:51.671Z"
dateCreated: "2021-11-09T12:13:36.265Z"
weight: 30
extra:
  parent: 'prise_en_main/_index.md'
---

Matrix est un réseau de communication instantanée que vous pouvez utiliser pour rester en contact
avec votre famille, avec vos amis ou avec votre collectif ou association.

# Accéder à Matrix

-   [🌐 Accéder via le navigateur](https://riot.deuxfleurs.fr)
-   [📥 Application Android (F-Droid)](https://f-droid.org/fr/packages/im.vector.app)
-   [📥 Application Android (Google Play Store)](https://play.google.com/store/apps/details?id=im.vector.app&hl=fr)
-   [📥 Application iOS](https://apps.apple.com/fr/app/riot-im/id1083446067)

# Guide d'inscription & d'utilisation

Adrien a écrit un [guide d'installation/utilisation du résau Matrix](https://guide.zinz.dev) qui devrait vous aider à vous lancer. 

# Nos salons Deuxfleurs

Deuxfleurs a une pléthore de salons sur Matrix. C'est vraiment à cet endroit qu'on discute le plus entre nous. Le mieux pour les découvrir, c'est de rejoindre notre « espace » (un annuaire de salons et de personnes) qui s'appelle [#la-cabane:deuxfleurs.fr](https://matrix.to/#/#la-cabane:deuxfleurs.fr) et qui ressemble à ça :

![espace_matrix_3salons.png](/img/matrix.png)

À l'intérieur de cet espace, on vous recommande notamment les 3 salons suivants : 

* [deuxfleurs::asso](https://matrix.to/#/#deuxfleurs:deuxfleurs.fr) où l'on parle de l'association.
* [deuxfleurs::forum](https://matrix.to/#/#forum:deuxfleurs.fr) où l'on discute beaucoup des questions de numérique et société mais pas que.
* [deuxfleurs::infra](https://matrix.to/#/#tech:deuxfleurs.fr) où l'on parle « techniques » (informatique, notamment).
* [deuxfleurs::usages](https://matrix.to/#/#usages:deuxfleurs.fr) où l'on parle « usages » des services fournis par DeuxFleurs: bugs, problèmes, questions, remarques, souhaits, idées.

