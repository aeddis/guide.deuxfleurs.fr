---
title: "Prise en main"
description: "Prise en main"
weight: 10
sort_by: "weight"
extra:
  parent: 'prise_en_main/_index.md'
---

Ce manuel vous accompagne dans la découverte de nos outils.
Pour vous aider à vous y retrouver, nous vous proposons 3 cas d'usages typiques : rester en contact avec ses proches, organiser une réunion,
publier une tribune.

# Je veux rester en contact avec mes proches

Si vous aimez échanger des messages texte, que ce soit dans des conversations privées ou en groupe,
vous serez intéressé·e par [Matrix](@/prise_en_main/matrix.md). 
Si vous connaissez **Signal**, **WhatsApp** ou **Telegram**, ça fonctionne de manière similaire.
L'application est disponible sur Android, iPhone et dans votre navigateur directement.
Elle permet de partager des messages textes, des messages vocaux, des photos, et des vidéos.
Vos échanges sont chiffrés (on dit parfois cryptés) de bout en bout et donc illisibles par une personne extérieure à la conversation, y compris par nous !

[↣ Apprendre à utiliser Matrix](@/prise_en_main/matrix.md)

Si vous préférez échanger de vive voix et voir directement votre interlocuteur·ice,
vous allez préférez [Jitsi](@/prise_en_main/jitsi.md).
Si vous connaissez **Facetime**, **Google Duo** ou l'appel vidéo **Facebook Messenger**, ça fonctionne de manière similaire.
L'application est disponible sur Android, iPhone et dans votre navigateur directement.
Vous pouvez activer ou désactiver votre webcam ou votre microphone, inviter jusqu'à 30 personnes ou encore partager votre écran.

[↣ Apprendre à utiliser Jitsi](@/prise_en_main/jitsi.md)


# Je veux organiser un évènement pour mon association ou entre amis

Pour prévenir tout le monde, envoyer un email

[↣ Apprendre à utiliser les emails](@/prise_en_main/email.md)

Pour planifier la réunion, utiliser l'outil "Formulaire" de Cryptpad.
Marche aussi pour faire des votes.

[↣ Apprendre à utiliser le formulaire Cryptpad](@/prise_en_main/cryptpad.md)

Pour discuter à distance, utiliser Jitsi

[↣ Apprendre à utiliser Jitsi](@/prise_en_main/jitsi.md)

Pour écrire collaborativement le compte rendu de réunion :

[↣ Apprendre à utiliser le pad Cryptpad](@/prise_en_main/cryptpad.md)


# Je veux publier une tribune sur Internet

Je veux que les gens puissent s'abonner à mon blog et être notifiés quand
je poste du nouveau contenu.

[↣ Apprendre à utiliser Plume](@/prise_en_main/plume.md)

Je veux créer un site web qui soit référencé dans les moteurs de recherche,
un espace où je puisse gérer finement mon contenu, et vers lequel je peux
envoyer des gens s'y référer

[↣ Apprendre à créer un site web](@/prise_en_main/web.md)


# URL des services

- <https://riot.deuxfleurs.fr> - messagerie instantanée Element (sur le réseau Matrix)
- <https://sogo.deuxfleurs.fr> - courrier électronique
- <https://alps.deuxfleurs.fr> - courrier électronique, interface plus légère
- <https://jitsi.deuxfleurs.fr> - outil de visioconférence
- <https://pad.deuxfleurs.fr> - pads et autres outils collaboratifs avec CryptPad
- <https://plume.deuxfleurs.fr> - plateforme de blog fédérée 
- <https://garagehq.deuxfleurs.fr> - plateforme d'hébergement de site web (voir documentation)
