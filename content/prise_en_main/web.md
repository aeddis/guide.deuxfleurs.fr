---
title: Site web
description: Héberger un site web
sort_by: weight
weight: 10
draft: false
date: 2023-04-26
extra:
  parent: prise_en_main/_index.md
---
Vous en avez marre de faire toute votre communication associative via Facebook ? Vous voulez créer votre propre site pour raconter vos dernières vacances ou publier vos poèmes ? Bref, publier en indépendance ? Deuxfleurs peut vous aider en vous fournissant l'hébergement. Suivez le guide !

1. [Statique ? Comment ça ?](@/prise_en_main/statique-comment-ça.md)
1. [Initialiser votre accès](@/prise_en_main/initialiser-votre-accès.md)
1. [Mettre en place le DNS](@/prise_en_main/mettre-place-DNS.md)
1. [Créer du contenu](@/prise_en_main/creer-du-contenu.md)
1. [Publier le contenu](@/prise_en_main/publier-le-contenu.md)

# Ils nous font confiance pour leur site web

- [declic-lelivre.com](https://declic-lelivre.com) - _Exploitation des données privées, surveillance généralisée, addiction au smartphone, disparition de pans entiers de l'économie... Les critiques du monde du numérique ne cessent de s'amplifier ; difficile d'ignorer les conséquences de l'utilisation d'Amazon, Facebook, Google, Instagram ou Netflix. Et s'il existait un autre Internet, respectueux de nos libertés ?_
- [envieappartagee.fr](https://www.envieappartagee.fr) - _Association Envie Appart'Agée, projet de coloc Alzheimer pour habiter et être accompagné autrement en Vendée._
- [wanderearth.fr](https://wanderearth.fr/) - _Blog voyage à vélo low-tech._
- [www.osuny.org](https://www.osuny.org/) - _Osuny est une solution technique spécialement conçue pour les universités, laboratoires de recherches et écoles supérieures permettant de créer des sites Web entièrement personnalisés, les plus sobres, les plus accessibles et les plus sécurisés possibles_.
- [giraud.eu](https://giraud.eu) - _Site d'un ingénieur en informatique_
- [anneprudhomoz.fr](https://anneprudhomoz.fr) - _De la terre, du fer, de l'eau, beaucoup de feu, un peu d'huile, encore du béton et du plâtre ! ... sans oublier le crayon à papier et les carnets._
- [courderec.re](https://courderec.re) - _Groupe de musique. De la pop synthé qui décoiffe._
- [colineaubert.com](https://colineaubert.com) - _D'un côté graphiste, illustratrice et conceptrice d'outils pédagogiques, je mets en images et en mots différents sujets scientifiques et culturels._
- [estherbouquet.com](https://estherbouquet.com) - _Esther Bouquet questions how narratives—both historical and literary—are being built by creating tangible experiences ranging from the size of the sheet of paper to the volume of a space; somewhere between writing, archiving, drawing, designing, and programming._
- [quentin.dufour.io](https://quentin.dufour.io) - _Portfolio et blog d'un ingénieur en informatique_
- [erwan.dufour.io](https://erwan.dufour.io) - _Portfolio et blog d'un passionné d'électronique_
- [luxeylab.net](https://luxeylab.net) - _Site d'un prof en informatique_
- [eric.dufour.io](https://eric.dufour.io) _- Appui au montage de projets de bioéconomie circulaire._
- [www.clairebernstein.photo](https://www.clairebernstein.photo/) - _Portfolio photographique._
- et de nombreux autres!



