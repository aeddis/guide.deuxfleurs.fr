---
title: "E-mails"
description: "E-mails"
weight: 20
extra:
  parent: 'prise_en_main/_index.md'
---

**ATTENTION**. Le service email est actuellement en phase de test.
Nous limitons le nombre de comptes et prévoyons de nombreuses maintenances dans les mois à venir.
La disponibilité, la stabilité ainsi que la qualité du service n'est pas assurée.
La configuration peut changer à tout moment, certaines fonctionnalités peuvent être désactivées.



# Accéder à vos emails

<ul>
  <li><a href='https://sogo.deuxfleurs.fr'>🌐 Accéder via le navigateur (SoGo)</a></li>
  <li><a href='https://alps.deuxfleurs.fr'>🌐 Accéder via le navigateur (alps, version allégée)</a></li>
  <li><a href='https://www.thunderbird.net/fr/'>📥 Thunderbird (PC/Mac)</a></li>
  <li><a href='https://k9mail.app/'>📥 K9 (Android)</a></li>
</ul>


# Configurer ses emails dans une application

| Protocole | Rôle | Hôte | Port | Chiffrement | Authentification | Certificat |
| -- | -- | -- | -- | -- | -- | -- |
| IMAP | Réception des emails | `imap.deuxfleurs.fr` | `993` | SSL/TLS | email+mdp | valide |
| SMTP | Envoi des emails     | `smtp.deuxfleurs.fr` | `465` | SSL/TLS | email+mdp | valide |
| Exchange ActiveSync | Tous | `https://sogo.deuxfleurs.fr` | `443` | SSL/TLS | email+mdp | valide |

# Utiliser un nom de domaine personnalisé (autre que @deuxfleurs.fr)

Deuxfleurs peut héberger vos e-mails même s'ils portent un nom de domaine différent, comme `jesuis@toutepuissante.net`. Louer un nom de domaine est payant et à votre charge, mais on peut s'occuper de les stocker et de les distribuer comme pour une adresse `@deuxfleurs.fr`. Voir ces deux pages du Guide :

* [Comment louer son nom de domaine](@/prise_en_main/mettre-place-DNS.md),
* [Comment faire héberger par Deuxfleurs une boîte mail au nom de domaine personnalisé](@/operations/email_personnalise.md).


