---
title: Avec un générateur
description: Créer du contenu avec un générateur
sort_by: weight
weight: 2
draft: false
date: 2022-09-01
extra:
  parent: prise_en_main/creer-du-contenu.md
---
Si vous souhaitez vous créer un blog, vous allez probablement avoir un site web avec de nombreuses pages : au moins une par billet que vous allez composer ! À la longue, on peut sans problème arriver à des dizaines de pages... Si on les écrit à la main, il va falloir copier/coller une bonne partie du code HTML, mais pas tout. Pour éviter ce travail rébarbatif, il existe ce qu'on appelle des générateurs de sites statiques. Ils vous permettent d'écrire dans une syntaxe très simple (Markdown) vos contenus, sans vous soucier du HTML ou du CSS, car ils emballeront eux-mêmes vos écrits dans des modèles prévus à cet effet. [Le guide que vous êtes en train de lire en ce moment même utilise un tel outil !](https://git.deuxfleurs.fr/Deuxfleurs/guide.deuxfleurs.fr)

### Choisir un générateur et un thème

On peut séparer **deux types de générateurs de site web statique** :

- Ceux qui demandent une bonne expertise en informatique (en **ligne de commande**) : [Hugo](https://gohugo.io/), [Jekyll](https://jekyllrb.com/), [Zola](https://www.getzola.org/), et bien d'autres... En général, ils se ressemblent beaucoup. À titre indicatif, pour ce guide, nous utilisons Zola. Nous baserons nos exemples sur lui. Si votre système d'exploitation est Linux, cherchez dans votre gestionnaire de paquet si vous trouvez l'un d'entre eux. Zola est ainsi facilement installable sur Arch, Ubuntu, et Fedora.

- Ceux qui sont accessibles aux non-spécialistes (avec une **interface graphique**) : [Publii](https://getpublii.com/), [Scribouilli](https://scribouilli.org/), [Zim](https://zim-wiki.org/)... Il s'agit alors d'installer un logiciel (le générateur) sur son ordinateur, qui va nous permettre de fabriquer notre site. Ledit générateur peut ensuite publier notre site et ses modifications sur Internet -- ou alors il faut le faire à la main. 

**Et le thème ?**
Chaque générateur de site statique propose des thèmes graphiques, pour donner à votre site une apparence unique. 
Par exemple; pour Zola, il faut aller [sur leur galerie](https://www.getzola.org/themes/). Si vous cliquez sur l'un d'entre eux, vous aurez des instructions sur comment l'installer. Il s'agit souvent de télécharger le thème dans le dossier correspondant à votre site, et de modifier le fichier `config.toml`, nécessaire pour Zola, afin de sélectionner le thème.

## Fonctionnement des générateurs en ligne de commande

On détaille l'utilisation d'un générateur en ligne de commande, en prenant l'exemple de Zola.

### Rédiger du contenu avec le langage Markdown

Les éditeurs en ligne de commande vous proposent généralement d'écrire les pages et billets de blogs de votre site dans un langage plus simple que HTML. C'est souvent Markdown.
 Rassurez-vous, il a été pensé pour être très simple et peut-être appris en quelques minutes. Voici un exemple :

```
# Ceci est le titre principal

## Ceci est un sous-titre

Ceci est du texte tout à fait normal. *Cette partie-ci du texte sera en italique*. **Cette partie-là sera en gras**.

## Ceci est encore un sous-titre

### Ceci est un sous-sous-titre

Voici du texte pour introduire la liste à puce qui va suivre :

* première élément de la liste
* second élément de la liste
* troisième élément de la liste
```

Quand on écrit du texte sans mettre quoi que ce soit autour ou avant, cela deviendra du texte tout à fait normal. On peut mettre un ou plusieurs dièses au début de la ligne pour en faire un titre. On peut mettre du texte en italique ou en gras avec des astérisques. Un astérisque en début de ligne provoque une liste à puces. Bref, cette syntaxe n'est pas compliquée, et vous pouvez facilement trouver plein de tutoriels en ligne pour la connaître. [Framasoft, par exemple, propose un bon guide](https://docs.framasoft.org/fr/grav/markdown.html).

Une fois votre fichier écrit, enregistrez-le avec l'extension `.md`, par exemple sous le nom `recette-tartre-au-citron.md`. L'idée est qu'à chaque page de votre site correspond un fichier `.md`, et ceux-ci seront regroupés dans un dossier. Vous trouverez [dans notre forge](https://git.deuxfleurs.fr/Deuxfleurs/guide.deuxfleurs.fr/src/branch/main/content) les fichiers Markdown écrits pour faire ce guide.

Cependant, il reste juste une petite chose à faire lorsque vous avez fini votre texte : écrire l'en-tête au dessus du markdown, qui donnera au générateur des informations importantes sur ce contenu. On encadre cet en-tête avec trois signes plus. Voici un exemple basé sur cette page :

```
---
title: "Avec un générateur"
description: "Créer du contenu avec un générateur"
date: 2022-09-01
---

# Titre

Texte de la page...
```

Comme vous l'avez peut-être compris, il s'agit de donner le titre de cette page, sa description, et sa date d'écriture. D'autres informations peuvent être rajoutées, cela dépend du générateur et du thème sélectionnés.

### Générer les pages

Vous avez donc des fichiers `.md` renfermant vos contenus, et un thème qui vous plaît. Avec un terminal, positionnez-vous dans le dossier racine de votre site projet. Si vous utilisez Zola, celui-ci devrait contenir un fichier `config.toml`, vous pouvez alors faire `zola build`. Cela va générer l'intégralité de votre site dans le dossier `public/`. Vous constaterez donc qu'il sera rempli de fichiers `.html` et `.css`, [vous êtes alors prêt(e) à passer à la publication](@/prise_en_main/publier-le-contenu.md) !
