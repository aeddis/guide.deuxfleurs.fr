---
title: "Compte unique"
description: "Compte unique pour tous les services"
weight: 10
extra:
  parent: "prise_en_main/_index.md"
---

Vous bénéficier d'un compte unique pour accéder à (presque) tous les services de Deuxfleurs.

# Guichet

Guichet est l'interface qui vous permet de changer votre mot de passe.
Depuis cette interface, vous pouvez aussi créer des liens d'invitations ou directement créer un compte pour votre entourage.

  - [🌐 Se rendre sur Guichet](https://guichet.deuxfleurs.fr)
