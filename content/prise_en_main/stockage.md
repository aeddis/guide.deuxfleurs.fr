---
title: "Stockage de fichiers"
description: "Stocker ses fichiers chez Deuxfleurs"
sort_by: "weight"
weight: 99
extra:
  parent: 'prise_en_main/_index.md'
---

Dans cette section, vous trouverez plusieurs tutoriels expliquant comment stocker vos fichiers chez Deuxfleurs sur Garage. Malheureusement, nous n'avons pas encore d'interface facile pour accéder à cette fonctionnalité: **les guides dans cette section sont donc à destination d'un public avancé**.
