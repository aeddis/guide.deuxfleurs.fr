---
title: "Hugo"
description: "Publier un site web sur Deuxfleurs avec Hugo"
weight: 10
extra:
  parent: 'prise_en_main/publier-le-contenu.md'
---

Configurez le fichier `~/.aws/credentials` de la manière suivante avec votre
identifiant de clef (`GKxxxx`) et votre clef secrète (`xxxxxx`), que vous
pouvez récupérer depuis [le
guichet](https://guichet.deuxfleurs.fr/website/configure#):

```ini
[default]
aws_access_key_id = GKxxxxxx
aws_secret_access_key = xxxxxxxx
```

Ensuite, ajoutez les lignes suivantes dans le fichier `config.toml` du dossier
de votre site web utilisé par Hugo:

```toml
[deployment]
order = [".jpg$", ".gif$", ".png$"]


[[deployment.targets]]
name = "deuxfleurs"
URL = "s3://votresiteweb.fr&endpoint=garage.deuxfleurs.fr&region=garage&s3ForcePathStyle=true"
```

Dans la ligne `URL`, remplacez `votresiteweb.fr` par le nom de domaine que vous souhaitez utiliser.

Une fois la configuration faite, vous pouvez publier votre site web en
exécutant simplement la commande suivante:

```
hugo deploy
```

Pour en savoir plus, consulter la documentation officielle à l'adresse:

* <https://gohugo.io/hosting-and-deployment/hugo-deploy/>

