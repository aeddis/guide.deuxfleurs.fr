---
title: "Guichet"
description: "Gestion de Guichet"
weight: 11
sort_by: "weight"
extra:
  parent: 'operations/_index.md'
---

Guichet, service interne qui sert notamment à la gestion des usager⋅es, aurait grandement besoin de votre aide en termes de développement. Voyez plutôt :

# Éditer un mot de passe 

AHAAAAHH, pauvre de vous, vous devez éditer un mot de passe.

> CALCULEZ LE HASH `SSHA512` À LA MAIN !!

Ou sinon, rendez-vous à la page de modification d'un utilisateur [Guichet](https://guichet.deuxfleurs.fr), puis cherchez le champ `userpassword` (et s'il n'existe créez ce champ en mettant `userpassword` dans le champ gauche de la dernière ligne de la section « Attributs »).

On s'intéresse désormais au champ à droite de `userpassword`, que vous souhaitez créer/modifier. Pour créer ledit hash du mot de passe, on vous recommande l'outil suivant (_one-liner_) :

    docker run --rm -it dxflrs/guichet:m1gzk1r00xp0kz566fwbpc87z7haq7xj cli -passwd

Entrez le mot de passe, et copiez-collez dans le champ sus-mentionné la fin de la ligne commençant par « Passowrd: » _en incluant `{SSHA512}`_. Validez. Si le nouveau champ n'apparaît pas, rechargez la page Guichet en cliquant dans la barre d'adresse et en appuyant sur Entrée (pour ne pas renvoyer la requête).

