---
title: "Juillet 2021"
description: "Juillet 2021: la BDD Synapse remplit nos disques"
date: 2022-12-22
dateCreated: 2022-12-22
weight: 20
extra:
  parent: 'operations/pannes.md'
---

# La BDD synapse rempli nos disques

Todo: finir ce blog post et le dupliquer ici https://quentin.dufour.io/blog/2021-07-12/chroniques-administration-synapse/

Le WAL qui grossissait à l'infini était également du à un SSD défaillant dont les écritures était abyssalement lentes.

Actions mises en place :
  - Documentation de comment ajouter de l'espace sur un disque différent avec les tablespaces
  - Interdiction de rejoindre les rooms avec une trop grande complexité
  - nettoyage de la BDD à la main (rooms vides, comptes non utilisés, etc.)
  - Remplacement du SSD défaillant

Actions à mettre en place :
  - Utiliser les outils de maintenance de base de données distribuées par le projet matrix
