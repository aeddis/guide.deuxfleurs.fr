---
title: "Prérequis pour un site"
description: "Prérequis pour un site"
date: 2024-05-30
dateCreated: 2024-05-30
weight: 10
extra:
  parent: 'operations/site.md'
---

# Qu'est-ce qu'un site géographique

Dans un *site géographique*, on installe une *grappe d'ordinateurs* au sein d'un *réseau local*, qu'on connecte au cluster de Deuxfleurs.

On peut distinguer deux types de sites : 

* _Dans un centre de données_ : chaque ordinateur de la grappe appartient au même centre de données, dispose d'une adresse IP fixe qui le connecte directement à Internet. 

	Dans ce cas-ci, **l'installation et l'administration sont assez simples** (on a moins de concepts à avaler que chez un particulier).
  Par contre, **nous ne sommes pas chez nous** : le propriétaire du centre de données peut accéder aux disques, et voit ce qui passe sur le réseau. **Chiffrement du disque vivement conseillé.**
  
* _Chez un particulier_ : la grappe est reliée à Internet par une *box*, qui filtre le trafic réseau selon des règles variables. La grappe se partage l'adresse IP de la box (qui peut changer régulièrement, être en IPv4 ou en IPv6). 

	Dans ce cas de figure, **l'installation comme l'administration demandent plus de connaissances** : pour caricaturer, on doit installer et administrer la grappe **malgré le Fournisseur d'Accès Internet** (FAI), qui considère *a priori* que son abonnement ne sert pas à héberger des services web.
  
  On aura affaire à sa box (NAT, pare-feu...), au manque de garanties concernant notre adressabilité (IPv4 dynamique, IPv6 ? ...), ce qui va nous mener à devoir faire du routage. Le nœud du problème, c'est que chaque ordinateur de la grappe n'aura pas pignon sur rue (pas d'adresse IP publique et fixe par machine).
  
  Néanmoins, **on est chez nous !** Votre disque dur - qui contient les données personnelles de vos usagers chéris - est sous vos yeux, bien au chaud. Le seul curieux qui voit passer votre trafic réseau est votre FAI : *rien de nouveau sous le soleil*.
 
# Pré-requis humains 

- assurer une sécurité physique vis-à-vis des machines et des données qu'elles contiennent
- pouvoir intervenir physiquement dans un délai de quelques jours en cas de panne d'une machine ou du réseau
- être d'accord pour partager son réseau et son IP avec l'infrastructure de Deuxfleurs (ou alors disposer d'une seconde connexion à Internet)

# Pré-requis techniques de base

- disposer d'une connexion fibre / FTTH avec un débit montant d'au moins 100 Mbps
- disposer d'une IPv4 publique dédiée et non partagée (la plupart des FAI proposent cette option)
- avoir un FAI qui fournit de l'IPv6
- avoir un routeur ou une box qui supporte UPnP
- pouvoir configurer le firewall IPv6 sur le routeur ou la box pour autoriser le trafic IPv6 entrant vers les noeuds Deuxfleurs
- ne pas déjà héberger des serveurs chez soi qui auraient besoin des mêmes ports TCP/UDP que Deuxfleurs (80, 443, 25, ...)

# Pré-requis supplémentaires pour le mail

Héberger un service mail demande des pré-requis supplémentaires :

- pouvoir définir le reverse DNS associé à son IPv4 publique.  Free le permet, ainsi que la plupart des FAI associatifs.  Orange et SFR ne le permettent pas.
- s'assurer que le FAI ne bloque pas le port 25 en entrée.  Certains FAI comme Free bloquent par défaut mais permettent de débloquer sur demande.  Orange ne permet pas de débloquer le port 25.

# Fournisseurs d'accès Internet recommandés

Pour l'instant, les FAI suivants ont été testés et remplissent tous les pré-requis :

- Free
- Belgacom
- Rézine
