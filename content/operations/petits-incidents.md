---
title: "Petits incidents"
description: "Petits incidents"
date: 2022-12-22
dateCreated: 2022-12-22
weight: 1000
extra:
  parent: 'operations/pannes.md'
---

- **2020** Publii efface le disque dur d'un de nos membres. Il a changé le dossier de sortie vers /home qui a été effacé

- **2021-07-27** Panne de courant à Rennes - 40 000 personnes sans électricité pendant une journée - nos serveurs de prod étant dans la zone coupée, deuxfleurs.fr est dans le noir - https://www.francebleu.fr/infos/faits-divers-justice/rennes-plusieurs-quartiers-prives-d-electricite-1627354121

- **2021-12:** Tentative de migration un peu trop hâtive vers Tricot pour remplacer Traefik qui pose des soucis. Downtime et manque de communication sur les causes, confusion généralisée.

  *Actions à envisager:* prévoir à l'avance toute intervention de nature à impacter la qualité de service sur l'infra Deuxfleurs. Tester en amont un maximum pour éviter de devoir tester en prod. Lorsque le test en prod est inévitable, s'organiser pour impacter le moins de monde possible.

- **2022-03-28:** Coupure d'électricité au site Jupiter, `io` ne redémarre pas toute seule. T est obligée de la rallumer manuellement. `io` n'est pas disponible durant quelques heures.

  *Actions à envisager:* reconfigurer `io` pour s'allumer toute seule quand le courant démarre.

- **2022-03-28:** Grafana (hébergé par M) n'est pas disponible. M est le seul à pouvoir intervenir.

  *Actions à envisager:* cartographier l'infra de monitoring et s'assurer que plusieurs personnes ont les accès.

- **2022-12-23:** Les backups de la production ne s'effectuaient pas correctement car Nomad ne voulait pas lancer les jobs pour cause de ressources épuisées (pas assez de CPU).

  *Action menée:* La préemption des jobs a été activée pour tous les schedulers Nomad, ce qui permet aux jobs de backup de virer les jobs de plus faible priorité pour pouvoir se lancer (ces derniers seront relancés sur une autre machine automatiquement).
