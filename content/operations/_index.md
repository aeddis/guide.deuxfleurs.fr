---
title: "Opérations"
description: "Opérations"
weight: 100
sort_by: "weight"
extra:
  parent: 'operations/_index.md'
---

Ce manuel recense notre savoir-faire technique, il a pour but d'accompagner nos opérateur·ices dans la réalisation de leurs tâches.

# Notre jargon

* _Un nœud_ (ou _node_ en anglais), c'est un **ordinateur unique** configuré pour fournir un **service** en collaborant avec d'autres.

* _Un site_ ou _une zone_, c'est un **ensemble d'ordinateurs qui sont situés au même endroit géographique**.

* _Une grappe_ (ou _cluster_ en anglais), c'est **un ensemble de nœuds** qui **coopèrent** pour fournir un **service**. Même si c'est normalement le cas, une grappe n'est pas forcément composée de plusieurs sites ! On peut avoir une grappe sur une seule zone.

	Une grappe est **gérée de façon cohérente** (avec le même système logiciel), **plus ou moins autonome** (elle ne dépend pas du reste du monde pour fournir des services web), **par une entité définie** (une personne physique ou un groupe de personnes).
  
* _Les opérateur·ices_ de la grappe ont **deux responsabilités** principales : **la maintenance** de la grappe, et **la protection des données hébergées**.

