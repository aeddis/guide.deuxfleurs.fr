---
title: "Installer NixOS en SSH"
description: "Installer NixOS en SSH sans écran ni clavier"
date: 2022-08-24
dateCreated: 2021-08-24
weight: 12
extra:
  parent: 'operations/noeud.md'
---

Quick tip avant d'oublier pour installer une de nos machines ThinkCentre via SSH sous NixOS ; c'est la seule solution quand on a pas d'écran ni de clavier sous la main.
Pré-requis : une clé USB, un ordi sous NixOS.

On va créer une image d'installation nous même qui démarre le SSH et configure root avec notre clé. On créer un fichier `iso.nix` :

```nix
{config, pkgs, ...}:
{
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>

    # Provide an initial copy of the NixOS channel so that the user
    # doesn't need to run "nix-channel --update" first.
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

  systemd.services.sshd.wantedBy = pkgs.lib.mkForce [ "multi-user.target" ];
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDT1+H08FdUSvdPpPKdcafq4+JRHvFVjfvG5Id97LAoROmFRUb/ZOMTLdNuD7FqvW0Da5CPxIMr8ZxfrFLtpGyuG7qdI030iIRZPlKpBh37epZHaV+l9F4ZwJQMIBO9cuyLPXgsyvM/s7tDtrdK1k7JTf2EVvoirrjSzBaMhAnhi7//to8zvujDtgDZzy6aby75bAaDetlYPBq2brWehtrf9yDDG9WAMYJqp//scje/WmhbRR6eSdim1HaUcWk5+4ZPt8sQJcy8iWxQ4jtgjqTvMOe5v8ZPkxJNBine/ZKoJsv7FzKem00xEH7opzktaGukyEqH0VwOwKhmBiqsX2yN quentin@dufour.io"
  ];
}
```

On construit l'image à partir de ce fichier de conf :

```bash
nix-build '<nixpkgs/nixos>' -A config.system.build.isoImage -I nixos-config=iso.nix

```

On le copie sur la clé USB :

```
dd if=result/iso/*.iso of=/dev/??? status=progress
sync
```

On branche la clé sur le serveur, on branche le serveur sur le réseau, on démarre le serveur et on surveille le routeur pour connaitre son IP (nom de domaine `nixos`).
Ensuite on se connecte dessus :

```
ssh root@192.168.1.X
```
