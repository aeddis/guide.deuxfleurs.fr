---
title: "Installer un site"
description: "Installer un site géographique"
date: 2024-05-30
dateCreated: 2024-05-30
weight: 19
extra:
  parent: 'operations/_index.md'
---

# Déployer un nouveau site géographique pour l'infrastructure Deuxfleurs

Dans un *site géographique*, on installe une *grappe d'ordinateurs* au sein d'un *réseau local*, qu'on connecte au cluster de Deuxfleurs.

Rajouter un nouveau site pour l'infrastructure Deuxfleurs est une opération qui demande de nombreuses étapes et peut avoir un fort impact sur la production.

Avant de se lancer, il faut en parler aux autres admins et [s'assurer qu'on remplit les prérequis](@/operations/prérequis-site.md).

Pour le réseau, nous avons un [guide de configuration de routeur sous OpenWrt](@/operations/site-openwrt.md)
