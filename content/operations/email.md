---
title: E-mails
description: Comprendre le fonctionnement des e-mails
weight: 10
draft: false
date: 2023-03-16
extra:
  parent: 'operations/_index.md'
---

## Serveurs

- SMTP: Postfix

- IMAP: Dovecot (fixé sur une machine avec un backup)

## Enregistrements DNS de la zone `deuxfleurs.fr`

| nom | type | valeur | signification |
| --- | ---- | ------ | ------------- |
| `@` | `MX` | `12 smtp.deuxfleurs.fr` | Serveur que chercheront à joindre les gens qui veulent envoyer un courrier à une addresse `@deuxfleurs.fr` |
| `smtp._domainkey` | `TXT` | `v=DKIM1; p=<clef publique>` | Enregistrement DKIM (voir ci-dessous) |
| `default._domainkey` | `TXT` | `v=DKIM1; p=<clef publique>` | Ancien enregistrement DKIM (voir ci-dessous) |
| `@` | `TXT` | `v=spf1 [...] -all` | Enregistrement SPF (voir ci-dessous) |
| `_dmarc` | `TXT` |  `v=DMARC1; [...]` | Enregistrement DMARC (voir ci-dessous) |
| `smtp` | `A` | défini par D53 | Addresse IPv4 pour parler au serveur Postfix |
| `imap` | `A` | défini par D53 | Addresse IPv4 pour parler au serveur Dovecot |
| `201.80.66.82.in-addr.arpa.` | `PTR` | `orion.site.deuxfleurs.fr.` | Reverse DNS indiquant le nom de domaine correspondant à l'IP du serveur Postfix |
| `orion.site` | `A` | `82.66.80.201` | Reverse DNS indiquant le nom de domaine correspondant à l'IP du serveur Postfix |

##  DKIM

Le mécanisme DKIM permet au serveur d'ajouter une signature sur les messages qui sortent de Deuxfleurs, pour que les destinataires puissent en attester la validité.
Il s'agit d'une signature RSA basée sur une paire de clefs privée/publique. La clef publique est donée dans l'enregistrement DNS DKIM, et la clef privée est connue
uniquement du serveur Postfix. La signature de chaque message est ajoutée dans un en-tête spécifique.

Référence : <https://www.cloudflare.com/learning/dns/dns-records/dns-dkim-record/>

**Exemple d'enregistrements DNS :**

```
default._domainkey.deuxfleurs.fr. 10800 IN TXT "v=DKIM1; h=sha256; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtdZp4qrgZR+6R7HeAkuLGJ/3L/6Ungvf5zwrMq6T8Tu931j2G4lYuPtsxyn9fZkT4y7DlX0waktLDB" "OCwf7X78nLEWjAFWiJTeWGRGhRdYRUFpscs9NUN0P+46jKlabibG3XTKd1DeAmywTu6o1oO03yiolrgKD1zgyDRFeUTfSwZIdPrdbcBSA1arda4WFtcBIrSygM9b4jtlqfQwGDrsMLbCBfVHDn4Wfm" "DWyNg0gDAkuLrYClNETk6aqIyj9fC8srKri0Qp3cRagCn+fjBvuxP35qWWJH7Rnnh/tuEDr1ufuNYO2KgJZ7JdMidUotxXE8cfU+OrEWQf4mIYeJ4wIDAQAB"

smtp._domainkey.deuxfleurs.fr.    1800  IN TXT "v=DKIM1; h=sha256; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtdZp4qrgZR+6R7HeAkuLGJ/3L/6Ungvf5zwrMq6T8Tu931j2G4lYuPtsxyn9fZkT4y7DlX0waktLDB" "OCwf7X78nLEWjAFWiJTeWGRGhRdYRUFpscs9NUN0P+46jKlabibG3XTKd1DeAmywTu6o1oO03yiolrgKD1zgyDRFeUTfSwZIdPrdbcBSA1arda4WFtcBIrSygM9b4jtlqfQwGDrsMLbCBfVHDn4Wfm" "DWyNg0gDAkuLrYClNETk6aqIyj9fC8srKri0Qp3cRagCn+fjBvuxP35qWWJH7Rnnh/tuEDr1ufuNYO2KgJZ7JdMidUotxXE8cfU+OrEWQf4mIYeJ4wIDAQAB"

default._domainkey.adnab.me.      3600  IN TXT "v=DKIM1; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDHd2zQXgGAoFX2CFaRqvWw1oBGhbUIRB5QXPxE9nvWwe/og5LjZBcnKoInPWsKYEz/f5kmpTDq4RZT3PMmjm+u5IuvyQ2LJcdIKSW6t8KWa7yztk2D87f3Lono6WJwvY8RHdGPqKS5RXfEdQFriXiSCAO5ZSQrNXQ5yiQ9T1ptGwIDAQAB; t=s"
```

**Structure :**

- Le nom de domaine est composé d'un selecteur (souvent `default`, on a ici aussi `smtp`) qui permet de distinguer différentes clefs pour signer les messages.
  Il est formé de la manière suivante: `<selecteur>._domainkey.<domaine>`
- La valeur est composé de plusieurs champs entre guillemets séparés par des `;`,  le champ `v` peut contenir la version de DKIM utilisée (ici `v=DKIM1`)
- Le champ `p` contient la clef publique
- Les autres champs sont optionels

**Application à la vérification d'une signature (exemple) :**

Prenons la signature suivante :

```
DKIM-Signature: v=1; a=rsa-sha256; c=simple/simple; d=deuxfleurs.fr; s=smtp;
	t=1679050854; bh=AkDk3Tm0bnC7b6dvTjRXJbThLE6h/IStsBGIYGa+q7c=;
	h=Date:To:From:Subject:From;
	b=Qll5ASi9DmD2rw9LK1vJOahE77Pd/HNDbmrkrOCt4S8Nu42WtJXXOtiwo9J3KGPzR
	 zA2Cw5oCUB0HW5ere8RkINsUj9X/nxOovxFaJw4LSrgEYxQh7unaGBs6Ecw6k2Aqc/
	 oMhNE2OVCSlLkJVUQbgzwBUcZuwndlki6yYoJXkSPMuZ4tFbhgjSaxneRgUvPocYw3
	 Vqc/yMEAbofrlaEf6nSNQZL+LSE4IEBeudsv3JodMn12OYAijhO0rrHHsinK9UkM3M
	 PEEISyatG6RzE6veh4VVv3PTyJMYouQI7fKNooLuDmlGsdSTV9HRo4UvQeOQT1SL/7
	 KvgJtR0Hqz3mQ==
```

Cette signature contient les champs suivants :

- `v` (obligatoire) indique la version, doit toujours être à `1`
- `a` (obligatoire) indique l'algorithme de signature, généralement `rsa-sha256`
- `d` (obligatoire) indique le domaine pour lequel la signature est produite
- `s` (obligatoire) indique le sélecteur de la clef DKIM utilisée (ici `smtp` pour utiliser la clef `smtp._domainkey.deuxfleurs.fr`)
- `h` (obligatoire) indique la liste des en-têtes signés
- `bh` (obligatoire) donne le hash du contenu du message
- `b` (obligatoire) donne la signature à proprement parler, qui signe les en-têtes `h` et le hash du contenu `bh`
- `t` (recomandé) donne le timestamp de la signature, i.e. sa date et son heure
- `c` est un paramètre additionnel de la méthode de calcul de la signature

**Chez Deuxfleurs:**

- L'en-tête de signature est rajouté par notre serveur Postfix.
- La clef privée est stockée dans Consul, et est injectée dans le conteneur Postfix au lancement.
- Les enregistrements DNS pour DKIM sont installés manuellement.
- Pour tous les autres domaines dont le courrier est traîté par les serveurs Deuxfleurs, on utilise un enregistrement DKIM en CNAME vers `smtp._domainkey.deuxfleurs.fr` pour que la gestion des règles soit centralisée via l'enregistrement défini sur les DNS Deuxfleurs.

## SPF

L'enregistrement SPF sert à aider le serveur de destination à déterminer si le message reçu est légitime ou non, en vérifiant des contraintes sur l'addresse IP par laquelle il a été reçu.
Normalement, c'est l'addresse IP du serveur SMTP de Deuxfleurs, donc on sait qu'on doit rejeter tous les messages venant d'autres addresses.

Références : <https://fr.wikipedia.org/wiki/Sender_Policy_Framework>

**Exemple d'enregistrements DNS :**

```
deuxfleurs.fr.  300     IN  TXT "v=spf1 mx:deuxfleurs.fr a:orion.site.deuxfleurs.fr ip4:82.66.80.201 ip6:2a01:e0a:28f:5e60::/64 -all"
adnab.me.       3600    IN  TXT "v=spf1 mx mx:adnab.me include:mx.ovh.com -all"
ietf.org.       1794    IN  TXT "v=spf1 ip4:50.223.129.192/26 ip6:2001:559:c4c7::/48 a:ietf.org mx:mail.ietf.org ip4:192.95.54.32/27 ip6:2607:5300:60:9ccf::/64 ip4:158.69.166.0/27 ip6:2607:5300:203:1b26::/64 ip4:4.31.198.32/27 ip6:2001:1900:3001:11::/64 include:_spf.google.com ~all"

```

**Structure :**

L'enregistrement commence par `v=spf1`, puis contient un ensemble de directives formées de la manière suivante:

- Un préfixe pouvant être `+` (résultat favorable), `?` (résultat neutre/aucune règle), `~` (entre le neutre et l'échec, utile pour déboguer), `-` (échec/défavorable). Le préfixe peut être omis, ce qui est interprété comme le préfixe `+`.
- Une paire type/valeur, avec les types suivants:
  - `mx` : utiliser un enregistrement DNS de type MX. L'enregistrement `MX` donne un ou plusieurs noms d'hôtes, qui sont eux-même des noms DNS. Ces noms sont ensuite résolus en `A` ou `AAAA` pour trouver les addresses correspondantes. Attention, un enregistrement `MX` n'est pas sensé pointer sur un `CNAME`, il doit pointer directement sur des enregistrements `A` et `AAAA` !
  - `ip4` : contient directement une plage d'addresses IPv4
  - `ip6` : contient directement une plage d'addresses  IPv6
  - `a` : contient un nom d'hôte à résoudre en `A` ou `AAAA` (pouvant utiliser des `CNAME`)
  - `include` : contient un nom de domaine ayant une autre règle SPF à inclure
  - `ptr` : désuet
- Ou bien le mot `all`, qui correspond à tous les expéditeurs dont l'addresse ne correspond pas aux autres règles

Par exemple, dans les exemples ci-dessus, voici comment interpréter les différentes règles:

- `mx:deuxfleurs.fr` : accepter le message si l'IP de l'expéditeur est trouvable en suivant les enregistrements `MX` associés à `deuxfleurs.fr`.
- `ip4:82.66.80.201` : accepter le message si l'IP de l'expéditeur est `82.66.80.201`
- `include:mx.ovh.com` : accepter le message si il serait accepté par la règle du domaine `mx.ovh.com` (consultable en faisant `dig TXT mx.ovh.com`)
- `a:ietf.org` : accepter le message si il vient de l'addresse IP de `ietf.org` (consultable en faisant `dig A ietf.org`)
- `-all` : rejeter strictement tous les messages venant d'une autre addresse IP


**Chez Deuxfleurs :**

- L'enregistrement SPF, installé manuellement, contient `mx:deuxfleurs.fr`, ce qui signifie que les addresses IP sont celles présentes dans l'enregistrement `MX` pour `deuxfleurs.fr`.
- Cet enregistrement est fixé manuellement pour pointer sur le serveur `smtp.deuxfleurs.fr`.
  L'enregistrement `A` pour `smtp.deuxfleurs.fr` est mis à jour automatiquement par D53 pour pointer vers l'IPv4 de la machine sur laquelle tourne Postfix.
- L'enregistrement SPF contient également `a:orion.site.deuxfleurs.fr`, qui contient également l'IPv4 de cette machine ; on garde cette règle en second recours au cas où il y aurait un problème avec la précédente, pour éviter de rejeter du courrier valide.
- L'enregistrement SPF contient également l'addresse IPv4 et la plage d'addresses IPv6 de la box à Orion (site où le serveur SMTP est actuellement déployé), pour en dernier recours éviter de rejeter des mails en cas de soucis avec les règles précédentes.
- **L'enregistrement SPF doit être mis à jour manuellement en cas de reconfiguration du serveur SMTP, en particulier si celui-ci change de site ou si les addresses IP changent.**
- Pour tous les autres domaines dont le courrier est traîté par les serveurs Deuxfleurs, on utilise un enregistrement SPF `include:deuxfleurs.fr` pour que la gestion des règles soit centralisée via l'enregistrement défini sur les DNS Deuxfleurs.

## DMARC

DMARC est un mécanisme qui permet de mieux contrôler la réaction des serveurs de destination en fonction des tests DKIM et SPF.
Par exemple, on peut préciser que tous les messages sont authentifiés par DKIM et SPF, et si un de ces tests échoue, le message doit nécessairement être rejeté.
On peut aussi demander à recevoir des rapports en cas d'échec.

Référence : <https://fr.wikipedia.org/wiki/DMARC>

**Exemple d'enregistrements DNS :**

```
_dmarc.deuxfleurs.fr. 300  IN TXT "v=DMARC1; p=reject; sp=reject; adkim=s; fo=1; aspf=s; ruf=mailto:prod-sysadmin@deuxfleurs.fr; rua=mailto:prod-sysadmin@deuxfleurs.fr; rf=afrf; pct=100; ri=86400"
_dmarc.adnab.me.      3600 IN TXT "v=DMARC1; p=reject; sp=reject; adkim=s; aspf=s; rua=mailto:postmaster@adnab.me!10m; ruf=mailto:postmaster@adnab.me!10m; rf=afrf; pct=100; ri=86400"
```

L'enregistrement peut contenir les champs suivants:

- `v=DMARC1` indique la version de DMARC utilisée
- `p` : procédure en cas d'échec avec le domaine principal (`none/quarantaine/reject`)
- `sp` : comme `p` mais s'applique aux sous-domaines
- `adkim` : indique si on doit appliquer la règle DKIM de manière stricte (`s`, le nom de domaine doit correspondre exactement) ou relaxée (`r`, des variations sur le nom de domaine sont permises)
- `aspf` : indique si on doit appliquer la règle SPF de manière stricte (`s`) ou relaxée (`r`)
- `ruf` : addresse mail à laquelle envoyer un rapport d'échec détaillé à chaque échec de validation
- `fo` : condition pour l'envoi d'un rapport d'échec détaillé (1 = si soit DKIM soit SPF a échoué)
- `rua` : addresse mail à laquelle envoyer un rapport d'échec aggrégé périodiquement
- `ri` : intervalle en secondes entre l'envoi des rapports aggrégés (86400 = 24h)
- `rf` : format des rapports (`afrf` est la seule valeur officiellement supportée)
- `pct` : proportion de messages à rejeter en cas d'échec

**Chez Deuxfleurs :**

- L'enregistrement DMARC est configuré pour rejeter de manière strict tout message ne passant pas la vérification SPF ou DKIM.
- Les rapports d'erreur doivent être envoyés à l'addresse `prod-sysadmin@deuxfleurs.fr` qui est consultés par les administrateurs systèmes de Deuxfleurs.
- Pour tous les autres domaines dont le courrier est traîté par les serveurs Deuxfleurs, on utilise un enregistrement DMARC en CNAME vers `_dmarc.deuxfleurs.fr` pour que la gestion des règles soit centralisée via l'enregistrement défini sur les DNS Deuxfleurs.

## Reverse DNS

L'enregistrement reverse DNS peut être utilisé par le serveur de destination
pour connaître le nom d'hôte correspondant à l'addresse IP du serveur qui a
envoyé le mail. Cet enregistrement doit idéalement correspondre à un nom de
domaine qui lui-même résoud à nouveau vers la même addresse IP.
L'application de cette règle de filtrage est à la discrétion des différents hébergeurs
mail, elle ne rentre pas dans le cadre défini par DMARC.

**Exemple d'enregistrements reverse DNS :**

```
201.80.66.82.in-addr.arpa.   86179 IN PTR orion.site.deuxfleurs.fr.
206.118.187.37.in-addr.arpa. 86400 IN PTR shiki.adnab.me.
```

