---
title: "Janvier 2020"
description: "Janvier 2020: changement imprévu d'adresses IP"
date: 2022-12-22
dateCreated: 2022-12-22
weight: 10
extra:
  parent: 'operations/pannes.md'
---

Le 20 janvier free a changé mon IP, un peu comme partout en France.
Ça concerne l'IPv4 et le préfixe IPv6.
Ici le bon vieux Bortzmoinsbien qui tweet : https://twitter.com/bortzmeyer/status/1351434290916155394

Max a update tout de suite l'IPv4 mais avec un TTL de 4h le temps de propagation est grand.
J'ai réduit les entrées sur les IP à 300 secondes, soit 5 minutes, le minimum chez Gandi, à voir si c'est une bonne idée.
Reste à update les IPv6, moins critiques pour le front facing mais utilisées pour le signaling en interne...

## Le fameux signaling
Ça pose un gros problème avec Nomad (et en moindre mesure avec Consul).
En effet, Nomad utilise l'IPv6 pour communiquer, il faut donc changer les IPs de tous les noeuds.
Problème ! On peut pas faire la migration au fur et à mesure car, changeant d'IP, les noeuds ne seront plus en mesure de communiquer.
On n'a pas envie de supprimer le cluster et d'en créer un nouveau car ça voudrait dire tout redéployer ce qui est long également (tous les fichiers HCL pour Nomad, tout le KV pour consul).
On ne peut pas non plus la faire à la bourrin en stoppant tous les cluster, changer son IP, puis redémarrer.
Enfin si, Consul accepte mais pas Nomad, qui lui va chercher à communiquer avec les anciennes IP et n'arrivera jamais à un consensus.

Au passage j'en ai profité pour changer le nom des noeuds car la dernière fois, Nomad n'avait PAS DU TOUT apprécié qu'un noeud ayant le même nom change d'IP. Ceci dit, si on utilise de facto le `peers.json` c'est peut être pas problématique. À tester.

Du coup, après moult réflexions, la silver bullet c'est la fonction outage recovery de nomad (consul l'a aussi au besoin).
Elle est ici : https://learn.hashicorp.com/tutorials/consul/recovery-outage
En gros, il faut arrêter tous les nodes.
Ensuite créer un fichier à ce path : `/var/lib/nomad/server/raft/peers.json`
Ne vous laissez pas perturber par le fichier `peers.info` à côté, il ne faut pas le toucher.
Après la grande question c'est de savoir si le cluster est en Raft v2 ou Raft v3.
Bon ben nous on était en Raft v2. Si vous vous trompez, au redémarrage Nomad va crasher avec une sale erreur :

```
nomad: failed to start Raft: error="recovery failed to parse peers.json: json: cannot unmarshal string into Go value of type raft.configEntry"
```

(je me suis trompé bien sûr).
Voilà, après il ne vous reste plus qu'à redémarrer et suivre les logs, cherchez bien la ligne où il dit qu'il a trouvé le peers.json.

## Les trucs à pas oublier

  - Reconfigurer le backend KV de traefik (à voir à utiliser des DNS plutôt du coup)
  - Reconfigurer l'IPv4 publique annoncée à Jitsi

## Ce qui reste à faire

 - Mettre à jour les entrées DNS IPv6, ce qui devrait créer :
   - digitale.machine.deuxfleurs.fr
   - datura.machine.deuxfleurs.fr
   - drosera.machine.deuxfleurs.fr
 - Mettre à jour l'instance garage sur io
