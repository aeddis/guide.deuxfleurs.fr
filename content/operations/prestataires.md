---
title: "Prestataires"
description: "Prestataires"
weight: 80
extra:
  parent: 'operations/_index.md'
---

# DNS

Gandi

# Pont IPv6

FDN

# Paquets

Docker Hub

# FAI

Free, SFR, et autres

# Électricité

EDF
