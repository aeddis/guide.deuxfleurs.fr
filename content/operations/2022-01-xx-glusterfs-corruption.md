---
title: "Janvier 2022"
description: "Janvier 2022: Corruptions GlusterFS"
date: 2022-12-22
dateCreated: 2022-12-22
weight: 30
extra:
  parent: 'operations/pannes.md'
---

# Corruption GlusterFS

Suite au redémarrage d'un serveur, les emails ne sont plus disponibles.
Il apparait que GlusterFS ne répliquait plus correctement les données depuis un certain temps.
Suite à ce problème, il a renvoyé des dossiers Dovecot corrompu.
Dovecot a reconstruit un index sans les emails, ce qui a désynchronisé les bàl des gens.
À la fin, certaines boites mails ont perdu tous leurs emails.
Aucune sauvegarde des emails n'était réalisée.
Le problème a été créé cet été quand j'ai réinstallé un serveur.
J'ai installé sur une version de Debian différente.
La version de GlusterFS était pinnée dans un sources.list, en pointant vers le repo du projet gluster
Mais le pinning était pour la version de debian précédente.
Le sources.list a été ignoré, et c'est le gluster du projet debian plus récent qui a été installé.
Ces versions étaient incompatibles mais silencieusement.
GlusterFS n'informe pas proactivement non plus que les volumes sont désynchronisées.
Il n'y a aucune commande pour connaitre l'état du cluster.
Après plusieurs jours de travail, il m'a été impossible de remonter les emails.

Action mise en place :
  - Suppression de GlusterFS
  - Sauvegardes journalière des emails
  - Les emails sont maintenant directement sur le disque (pas de haute dispo)

Action en cours de mise en place :
  - Développement d'un serveur IMAP sur Garage



