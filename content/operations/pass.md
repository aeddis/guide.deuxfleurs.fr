---
title: Le dépôt des secrets
description: Le dépôt des secrets
weight: 10
draft: false
date: 2024-03-16
extra:
  parent: operations/acces.md
---
We use [pass, 'the standard unix password manager'](https://www.passwordstore.org/), to manage our key store securely at Deuxfleurs. Getting access to our production involves publishing one's GPG key (through Gitea) and importing/verifying/signing every other sysadmin's key, before setting up `pass`. Lastly, you will be able to set your shell password on the desired cluster (`prod` or `staging`, at the time of writing).

Our process was adapted from [this Medium article](https://medium.com/@davidpiegza/using-pass-in-a-team-1aa7adf36592) — thanks, David!

## You are new and want to access the secrets repository

You need a GPG key to start with.
If you do not already have one, you can generate a new one with:

```bash
gpg2 --expert --full-gen-key
# Personnaly I use `9) ECC and ECC`, `1) Curve 25519`, and `5y`
# Pour le nom, il vous identifie vous, vous pouvez l'utiliser pour signer des mails par exemple.
```

(Il se peut que vous n'ayez que la commande `gpg` sur votre PC parce qu'il est moderne. Faites `gpg --version` pour vérifier.)

In any case, you need to export your public key. Upload the output (public key) of the following command to [your Gitea account](https://git.deuxfleurs.fr/user/settings/keys).

```bash
gpg2 --export --armor <your email address>
```

It is now publicly accessible at `https://git.deuxfleurs.fr/<your_gitea_username>.gpg`

### Inform the other sysadmins that you have published your key

Download the `secrets` repository:

```
mkdir -p ~/.password-store
cd ~/.password-store
git clone git@git.deuxfleurs.fr:Deuxfleurs/secrets.git deuxfleurs
```

Get the `<hexa_of_your_gpg_key>`, which you can get with `gpg --list-keys --keyid-format 0xLONG` .
It's in the format `0x0123456789ABCDEF`.
Beware to take the key of your main key (line begins with `pub`, not with `sub`).

- Add your key address to the `Import keys` section of the repo's `README.md`:

```
curl https://git.deuxfleurs.fr/<your_gitea_username>.gpg | gpg2 --import    # <hexa_of_your_gpg_key>
```

- Also add your `<hexa_of_your_gpg_key>` to the `.gpg-id` file.
- In [your Gitea settings](https://git.deuxfleurs.fr/user/settings), ensure your User visibility is `Public`.

Now `git commit/pull/push`, and tell yo' friends that you're all set.

### Import/verify/sign every other sysadmin's key into your keychain

You now need to import and sign every other sysadmin's GPG key. For example, import Quentin's key to your keychain as follow:

```bash
gpg2 --import <(curl https://git.deuxfleurs.fr/quentin.gpg)
gpg2 --list-keys
# pub   ed25519/0xE9602264D639FF68 2022-04-19 [SC] [expire : 2027-04-18]
#  Empreinte de la clef = 8023 E27D F1BB D52C 559B  054C E960 2264 D639 FF68
# uid                  [  ultime ] Quentin Dufour <quentin@deuxfleurs.fr>
# sub   cv25519/0xA40574404FF72851 2022-04-19 [E] [expire : 2027-04-18]
```

> How to read this snippet:- the key id: `E9602264D639FF68`
> - the key fingerprint: `8023 E27D F1BB D52C 559B  054C E960 2264 D639 FF68`

To perform the check, you need another communication channel (ideally physically, otherwise through the phone, Matrix if you already trusted the other person, or else). Compare the signature you read with the sysadmin's. If they match, you good.

Now that you have verified the key, sign it:

```bash
gpg --edit-key quentin@deuxfleurs.fr # by email
# or
gpg --edit-key E9602264D639FF68 # by key id
# gpg> lsign
# (say yes, maybe several times)
# gpg> save
```

Once you signed every sysadmin, ask an administrator to add your key to the secrets keystore. They will need to [Add a sysadmin](#add-a-sysadmin).

Once your fellow admin has finished their job, you are ready to install `pass`:

```bash
sudo apt-get install pass # Debian + Ubuntu
sudo yum install pass # Fedora + RHEL
sudo zypper in password-store # OpenSUSE
sudo emerge -av pass # Gentoo
sudo pacman -S pass # Arch Linux
brew install pass # macOS
pkg install password-store # FreeBSD
```

_Go to  for more information about pass_.

Finally check that everything works:

```bash
pass show deuxfleurs
```

If you see a listing, it worked. Last step is to select a shell password for yourself on the cluster you are now in charge of (`prod` or `staging`, at the time of writing).

Clone the nixcfg repository:

```
git clone git@git.deuxfleurs.fr:Deuxfleurs/nixcfg.git
cd nixcfg
```

Use the passwd utility to set your shell password:

```
./passwd
> Usage: ./passwd <cluster name> <username>
> The cluster name must be the name of a subdirectory of cluster/
```

This commited changes to Deuxfleurs' password store, do verify your modifications before pushing them:

```
cd ~/.password-store/deuxfleurs
git diff
git push
```

You should now be able to `ssh` into our infrastructure with a unified shell password. This is explicated in `nixcfg` repo's [README](https://git.deuxfleurs.fr/Deuxfleurs/nixcfg#how-to-operate-a-node). Be cautious, and enjoy!

> With great power comes great responsibility.

---

---

## You are a sysadmin and want to operate the secrets repository

## Initialise the pass store

> These instructions are for _bootstrapping_ the pass store. **Don't do it on Deuxfleurs' secrets repository** (don't be like ADRN). You would override the existing sysadmins and generally have a bad time.

Generate a new password store named deuxfleurs for you:

```
pass init -p deuxfleurs you@example.com
```

Add a password in this store, it will be encrypted with your gpg key:

```bash
pass generate deuxfleurs/backup_nextcloud 20
# or
pass insert deuxfleurs/backup_nextcloud
```

## Add a sysadmin

Make sure that you trust the keys of your new sysadmin:

```
$ gpg --edit-key jane@example.com
gpg> lsign
gpg> y
gpg> save
```

Now re-encrypt the secrets and push your modifications:

```
pass init -p deuxfleurs $(cat ~/.password-store/deuxfleurs/.gpg-id)
cd ~/.password-store/deuxfleurs
git commit
git push
```

They will now be able to decrypt any password:

```
pass deuxfleurs/backup_nextcloud
```

## Sharing with git

To create the repo _on bootstrap exclusively_:

```bash
cd ~/.password-store/deuxfleurs
git init
git add .
git commit -m "Initial commit"
# Set up remote
git push
```

To retrieve the repo:

```bash
mkdir -p ~/.password-store
cd ~/.password-store
git clone https://git.example.com/org/repo.git deuxfleurs
```




