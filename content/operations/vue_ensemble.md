---
title: "Vue d'ensemble"
description: "Vue d'ensemble"
sort_by: "weight"
weight: 0
extra:
  parent: 'operations/_index.md'
---

*Ajouter un schéma*

# Les composants

**Orchestrateur** - L'orchestrateur est en charge de placer les applications
sur les serveurs de sorte qu'en cas de crash, une application soit relancée ailleurs.
En fonctionnement nominal, l'orchestrateur se charge de placer les applications de sorte
à ce que les ressources soient utilisées au mieux. Nous utilison le logiciel Nomad à ces fins.

[↣ Accéder au code source de Nomad](https://github.com/hashicorp/nomad)

**Catalogue de service** - Le catalogue de service permet aux services ayant des dépendances, par exemple
Matrix a une dépendance sur PostgreSQL et Garage, de savoir comment contacter ces dépendances.
Pour ce faire, nous utilisons Consul, qui nous sert également à gérer nos secrets.

[↣ Accéder au code source de Consul](https://github.com/hashicorp/consul/)

**Base de données relationnelle** - Les bases de données relationnelles sont au coeur de 
la plupart des applications web car elles permettent de requêter efficacement des données structurées.
Nous utilisons PostgreSQL qui est un des standard du libre, et afin de le distribuer, nous utilisons
Stolon.

[↣ Accéder au code source de PostgreSQL](https://git.postgresql.org/gitweb/?p=postgresql.git;a=summary)  
[↣ Accéder au code source de Stolon](https://github.com/sorintlab/stolon)

**Base de données NoSQL** - Nous prévoyions certains développements spécifiques, 
pour lesquels nous avons le loisir de définir la structure de données.
Dans ce cas, nous pouvons l'optimiser pour Garage K2V qui est bien plus facile à opérer que PostgreSQL.


[↣ Accéder au code source de Garage](https://git.deuxfleurs.fr/Deuxfleurs/garage)

**Stockage objet** - De nombreuses applications ont besoin de stocker des fichiers également,
des données considérées non structurées dans ce cas. Ici l'enjeu n'est plus tant de pouvoir
requêter les données avec souplesse que de pouvoir stocker de grands volumes.
On peut prendre comme exemple les médias (photo, vidéo) partagés sur Matrix.
Nous utilisons l'API S3 de Garage pour ces besoins.

[↣ Accéder au code source de Garage](https://git.deuxfleurs.fr/Deuxfleurs/garage)

**Authentification** - Afin de faciliter la vie des usager-es, il est possible de proposer un système d'authentification unique.
Pour commencer, nous avons choisi le protocole LDAP qui a pour principal avantage d'être largement installé et compatible,
principalement grâce à son ancienneté. Nous utilisons notre propre implémentation, nommée Bottin.

[↣ Accéder au code source de Bottin](https://git.deuxfleurs.fr/Deuxfleurs/bottin)

**Conteneurs** - Afin de packager, publier et isoler les différentes applications qui fonctionnent sur nos serveurs,
nous utilisons des conteneurs Linux, et plus précisément, Docker. Afin de créer des images les plus petites possibles,
nous utilisons NixOS. Pour certaines vieilles images, nous utilisons encore Debian.

[↣ Accéder au code source de Docker](https://github.com/moby/moby)  
[↣ Accéder au code source de Nix](https://github.com/NixOS/nixpkgs)  
[↣ Accéder au code source de Debian](https://www.debian.org/distrib/packages)
