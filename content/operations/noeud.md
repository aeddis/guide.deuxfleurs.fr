---
title: "Installer un noeud"
description: "Déploiement d'un noeud"
date: 2022-08-23
dateCreated: 2021-08-23
weight: 20
extra:
  parent: 'operations/_index.md'
---

# Déployer un nœud au sein de l'infrastructure Deuxfleurs
Déployer un nœud au sein de l'infrastructure Deuxfleurs demande un certaine préparation et représente un processus particulier.

Avant de se lancer, [mieux vaut vérifier les prérequis pour y parvenir](@/operations/prérequis.md). Une fois ceci fait, on peut suivre [le guide décrivant la procédure](@/operations/guide_création_nœud.md).

Si vous avez une machine à installer, mais aucun écran & clavier à brancher dessus pour la configurer, référez-vous au [guide d'installation de NixOs en SSH](@/operations/SSH_sans_écran.md).
