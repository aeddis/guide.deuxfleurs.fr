---
title: "Stolon"
description: "Comment déployer Stolon"
date: 2022-12-22
dateCreated: 2022-12-22
weight: 11
extra:
  parent: 'operations/deployer.md'
---

Spawn container:

```bash
docker run \
  -ti --rm \
  --name stolon-config \
  --user root \
  -v /var/lib/consul/pki/:/certs \
  superboum/amd64_postgres:v11
```


Init with:

```
stolonctl \
  --cluster-name chelidoine \
  --store-backend=consul \
  --store-endpoints https://consul.service.prod.consul:8501 \
  --store-ca-file /certs/consul-ca.crt \
  --store-cert-file /certs/consul2022-client.crt \
  --store-key /certs/consul2022-client.key \
  init \
  '{ "initMode": "new",
     "usePgrewind" : true,
     "proxyTimeout" : "120s",
     "pgHBA": [ 
       "host all postgres all md5", 
       "host replication replicator all md5", 
       "host all all all ldap ldapserver=bottin.service.prod.consul ldapbasedn=\"ou=users,dc=deuxfleurs, dc=fr\" ldapbinddn=\"<bind_dn>\" ldapbindpasswd=\"<bind_pwd>\" ldapsearchattribute=\"cn\"" 
      ] 
   }'

```

Then set appropriate permission on host:

```
mkdir -p /mnt/{ssd,storage}/postgres/
chown -R 999:999 /mnt/{ssd,storage}/postgres/
```

(102 is the id of the postgres user used in Docker)
It might be improved by staying with root, then chmoding in an entrypoint and finally switching to user 102 before executing user's command.
Moreover it would enable the usage of the user namespace that shift the UIDs.


## Stolonctl alias

```
alias stolonctl='stolonctl --cluster-name chelidoine --store-backend consul --store-endpoints https://consul.service.prod.consul:8501 --store-ca-file /certs/consul-ca.crt --store-cert-file /certs/consul-client.crt --store-key /certs/consul-client.key'
```

## Upgrading the cluster

To retrieve the current stolon config:

```
stolonctl spec --cluster-name chelidoine --store-backend consul --store-ca-file ... --store-cert-file ... --store-endpoints https://consul.service.prod.consul:8501
```

The important part for the LDAP:

```
{
	"pgHBA": [
		"host all postgres all md5",
		"host replication replicator all md5",
		"host all all all ldap ldapserver=bottin.service.2.cluster.deuxfleurs.fr ldapbasedn=\"ou=users,dc=deuxfleurs,dc=fr\" ldapbinddn=\"cn=admin,dc=deuxfleurs,dc=fr\" ldapbindpasswd=\"<REDACTED>\" ldapsearchattribute=\"cn\""
	]
}
```

Once a patch is writen:

```
stolonctl --cluster-name pissenlit --store-backend consul --store-endpoints http://consul.service.2.cluster.deuxfleurs.fr:8500 update --patch -f /tmp/patch.json
```

## Log

- 2020-12-18 Activate pg\_rewind in stolon

```
stolonctl --cluster-name pissenlit --store-backend consul --store-endpoints http://consul.service.2.cluster.deuxfleurs.fr:8500 update --patch '{ "usePgrewind" : true }'
```

- 2021-03-14 Increase proxy timeout to cope with consul latency spikes

```
stolonctl --cluster-name pissenlit --store-backend consul --store-endpoints http://consul.service.2.cluster.deuxfleurs.fr:8500 update --patch '{ "proxyTimeout" : "120s" }'
```
