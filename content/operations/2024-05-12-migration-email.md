---
title: "Mai 2024"
description: "Mai 2024: perte d'une zone, migration des services email"
date: 2024-05-12
dateCreated: 2024-05-12
weight: 30
extra:
  parent: 'operations/pannes.md'
---

12 mai 2024 : coupure de la connexion fibre sur le site à Lille (scorpio).
Les services cryptpad et email sont impactés.  
Après plus de 24h de coupure, on décide de migrer les services email vers un autre site pour éviter de perdre des emails qui seraient dans la file de serveurs emails à destination de deuxfleurs.

# Infos infra

- Sites: scorpio (Lille), neptune (Orsay)
- machine à Lille dont on migre : ananas
- Orsay IPv4 : 82.67.87.112 , IPv6 : 2a01:e0a:2c:540::33
- Machines Orsay : {courgette,celeri,concombre}.machine.deuxfleurs.fr
  + courgette (12G RAM, 500G HDD, 500G SSD Samsung 980), avec postgres (pinned)
  + celeri (8G RAM, 2T HDD, 500G SSD Samsung 980), avec matrix
  + concombre (8G RAM, 500G SSD Samsung 980, 500G HDD), avec jitsi/plume/woodpecker

# Feuille de route emails

- vérifier l'existence et l'âge des backups emails
  + dernier backup: 9644354b  2024-05-11 02:00:05  89e2ffda19db              /mail
  + se convaincre que c'est un backup suffisamment récent par rapport à la coupure qui a commencé samedi 11 matin à 7h07
- ✅ choisir un serveur à Orsay
  + courgette a postgres pin sur cette machine, on choisit une autre machine parmi celeri/concombre
  + choix de celeri

- ✅ s'assurer que restic existe sur la machine destination et qu'il est en version 0.16 ou plus
- ✅ restore le backup avec restic (cf cheatsheet et le script restic_restore_gen) depuis la machine destination

- ✅ vérifier que les entrées SPF, DKIM, DMARC sont compatibles avec Orsay
  + entrée SPF à modifier sur Gandi
    * entrée mx qui devrait suffire ?
    * mais on pourrait ajouter neptune.site.deuxfleurs.fr (et enlever orion.site.deuxfleurs.fr qui n'existe plus)
  + rien à faire pour DKIM/DMARC?
    * Pour DKIM et DMARC rien à faire tant que la clef ne change pas, ce sont des politiques
- ✅ vérifier que le reverse-DNS de l'IP d'Orsay est bon
  + C'est fait:
    * dig +short -x 82.67.87.112: neptune.site.deuxfleurs.fr.
- ✅ vérifier que les autres entrées DNS (A,AAAA, MX) sont comme il faut :
  + ✅ MX deuxfleurs.fr (pas besoin de modifier, il pointe bien vers smtp.deuxfleurs.fr)
  + ✅ A smtp.deuxfleurs.fr (pas besoin de modifier à la main en fait, d53 s'en charge en fait !)
  + ✅ AAAA smtp.deuxfleurs.fr (pas besoin de modifier à la main en fait, d53 s'en charge en fait !)
  + ✅ A imap.deuxfleurs.fr (pas besoin de modifier à la main en fait, d53 s'en charge en fait !)
  + ✅ AAAA imap.deuxfleurs.fr (pas besoin de modifier à la main, d53 s'en charge !)

- ✅  comprendre où étaient stockés les données des emails sur ananas (chemin sur le FS)
  + a priori /mnt/ssd/mail d'après la feuille de route précédente
  + retrouvable dans le fichier hcl du service email -> /mnt/ssd/mail en effet
- ✅  stopper le service nomad email (FAIT)
  + ça devrait être OK vis à vis des enregistrement DNS et D53, car D53 ne gère pas le MX (si on retire un enregistrement DNS et quelqu'un fait une requête il va recevoir un NXDOMAIN qui sera peut être mis en cache pendant plus longtemps que l'on voudrait)

- ✅  pause pendant le transfert des données !

- ✅  faire des tests sur les données ?
  + ✅ verifier que le chemin est ok (/var/ssd/mail et pas /var/ssd/mail/mail/ - restic est piegeur)
  + ✅ il y a bien les users
  + que peut-on faire de plus que faire confiance à restic ?
- ✅  vérifier que les permissions (owner/group/etc) des fichiers sont ce qu'on veut
  + https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/cluster/prod/app/email/build/dovecot/entrypoint.sh
  + -> le conteneur fait le bon chown au démarrage, donc il n'y a rien à faire

- ✅ relire et modifier le fichier nomad pour le service email pour déployer sur la nouvelle machine
- ✅ démarrer le service nomad sur la nouvelle machine
- ✅ tester les accès (IMAPS: 993, SMTPS: 465, SMTP: 25)
  + ✅ openssl s_client -connect host:port en IPv4 et IPv6 direct
  + ✅ vérifier que ça marche aussi avec {smtp,imap}.deuxfleurs.fr
  + ✅ vérifier avec thunderbird
  + ✅ vérifier SoGo
    * interface web (OK)
    * Exchange Active Sync
  + ✅ vérifier Alps

ℹ️ DÉBLOQUER LE PORT 25 DE LA FREEBOX PARDIOU !

- ✅ vérifier la réception des emails
  + ✅ depuis gandi (+ autres ?)
  + ✅ depuis microsoft (OK)
  + ✅ depuis google (OK)
  + ✅ depuis 6clones https://www.6clones.fr/emails
- ✅ vérifier l'envoi d'emails
  + ✅ Le port 25 est bloqué sur la freebox 😭 😭 😭 😭 😭 😭 😭 😭
  + ✅ chez gandi (+ autres ?)
  + 🟧 chez microsoft (en spam)
  + ✅ chez google
  + ✅ avec mailtester -> https://www.mail-tester.com/
  + ✅ vers 6clones https://www.6clones.fr/emails

- ✅  configurer le backup des emails pour backuper la nouvelle machine au lieu d'ananas
  + lancer un backup pour vérifier que ça fonctionne

- ✅  Commit + Push les modifications qui ont été faites sur nixcfg !!!!

- ⌛ réputation IP
  + ✅ Spamhaus https://www.spamhaus.org/query/ip/82.67.87.112 a l'air bon
  + ✅ Microsoft
    * http://postmaster.live.com/snds/ (impossible à faire car on ne détient pas les IP / il n'y a pas besoin de le faire)
    * Office 365 Anti-Spam IP Delist Portal  https://sender.office.com/
  + ✅ Baracuda Networks  https://barracudacentral.org/ (OK: "not listed as poor")
  + ✅ Trend Micro  http://www.mail-abuse.com/cgi-bin/lookup?ip_address=82.67.87.112
    * BAD "dial-up user list". Demande de délistage soumise (avec email de contact prod-sysadmin@deuxfleurs.fr).
    * on a reçu un email disant que c'est délisté; à rechecker bientôt ?
  + ⌛ Spam-RBL https://spam-rbl.fr/bl.php?ip=82.67.87.112
    * partiellement listé
  + ✅ Abusix https://lookup.abusix.com/search?q=82.67.87.112
    * not listed!
  + ✅ SORBS http://www.sorbs.net/cgi-bin/dulexclusions
    * OK!
    * Du coup c'est très cool, on va ameliorer notre delivery !
  + Pour info seulement :
    * Meta checker -> https://multirbl.valli.org/lookup/82.67.87.112.html
      ** partiellement listé: rbl.rbldns.ru ; a recheck dans qq heures d'après leur documentation
    * DNS checker -> https://dnschecker.org/ip-blacklist-checker.php?query=82.67.87.112
      ** partiellement listé: https://matrix.spfbl.net/82.67.87.112 veut $2

Une fois Lille de retour :
- ⌛ renommer le dossier contenant les données mail en mail_deprecated
- ⌛ faire un diff entre les données sur ananas et les données du backup que l'on a restore
- ⌛ migrer les données du diff si on peut ?
  + On peut probablement se baser sur les dates de création des fichiers sur ananas


## Exemple de message à envoyer aux blocklists

### Reason for removal request

> [EN] Deuxfleurs (deuxfleurs.fr) is an experimental & alternative non-profit email provider. This is why we are sending (legitimate!) emails from a residential IP address. We are sending mainly organic emails and few transactional emails (we don't do marketing or newsletter emails). We have strict policies to ensure that emails sent from our servers are legitimate, here is a non-exhaustive list of our trust policy: account creation is manually validated, our SMTP server is authenticated (no SMTP open relay), SPF, DKIM & DMARC are configured, our servers are closely monitored. We operate from a static IPv4 and IPv6 block bound to the fiber line allocated by the ISP, hence the dial-up classification. Thanks in advance for keeping the Internet an open place!

> [FR] Deuxfleurs (deuxfleurs.fr) est une association fournissant des services d'hébergement email, et nous hébergeons nos services derrière des adresses IP résidentielles.
> Nous envoyons principalement des emails non-automatisés (ni emails marketing ou démarchage). Nous implémentons des politiques strictes pour garantir que les emails envoyés depuis nos serveurs sont légitimes : les comptes sont validés manuellement par les administrateurs, notre serveur SMTP est authentifié (pas de relai SMTP ouvert), nous avons configuré SPF, DKIM et DMARC, et nos serveurs sont monitorés.
> Nos serveurs opèrent derrière des IPv4 et IPv6 statiques liés à une ligne fibre FTTH (IPs probablement allouées précédemment par le FAI à des lignes DSL).
> Merci d'avance !

### Reason why the IP address is not in static allocation

> This is a static allocation from the ISP. This used to be an IP address associated with DSL lines from our ISP, now recycled for FTTH lines.

# Feuille de route cryptpad

- problème : les backups sont probablement toujours faits depuis un ancien déploiement cryptpad
  + confirmer ça -> oui en effet
- si c'est bien ça, quelle taille font les données cryptpad ? Adrien peut-il les uploader en 4g en se connectant en local à ananas ?
  + Probablement autours de 1.5/2Gio, vu que c'est ce que Quentin a récupéré du backup qui n'est pas si vieux que ça
  + c'est l'ordre de grandeur de quand on a fait la snapshot de migration aussi
...
- ✅ changer la source des backups cryptpad vers le nouveau déploiement

# Cheatsheet

## Backups

Pour lister et récupérer un backup, depuis le dépot nixcfg:

```
./restic_summary
./restic_restore_gen mail <ID> /mnt/ssd
```

## Comparer les données entre deux dossiers

```
rclone hashsum md5 /mnt/storage/mail-bckp-2/ --output-file mail-hdd.sum
rclone hashsum md5 /mnt/ssd/mail/ --output-file mail-ssd.sum
sort mail-ssd.sum | md5sum
sort mail-hdd.sum | md5sum
```

# Idées pour le futur
- les backups de cryptpads qui sont cassés => réfléchire a une stratégie de *tests* de backups
- on pourrait setup un MX secondaire qui queue indéfinimenet et essaie de push sur le primaire, pour pas perdre d'emails entrants en cas de down de la zone principale. /!\ a bien backup aussi cette queue
  + MX secondaire
  + sur le primaire, penser a bind la queue postfix aussi (ajd on pourrait perdre des emails récement reçu, mais pas encore délivré)
- on a encore des backups de plume, mais ils sont vieux. p-e plus besoin maintenant que les médias sont sur s3?
- hcl de backups => cron deprecated, use crons instead (ça accept les listes de cron maintenant)
