---
title: Nom de domaine personnalisé
description: Nom de domaine personnalisé (+ migration d'une boîte existante)

weight: 10
draft: false
date: 2023-03-16
extra:
  parent: 'operations/email.md'
---

Deuxfleurs peut héberger vos e-mails, même s'ils ne finissent pas en `@deuxfleurs.fr` ! 

> Vous voulez votre propre nom de domaine mais ne savez pas comment faire ? [On vous l'explique ici](@/prise_en_main/mettre-place-DNS.md).

Nous vous expliquons ici comment héberger une boîte mail avec un nom de domaine vous appartenant, en incluant la récupération (optionnelle) des e-mails existants chez votre ancien fournisseur vers les serveurs de Deuxfleurs.

1. Si vous souhaitez rapatrier vos anciens e-mails de l'ancien fournisseur vers Deuxfleurs, sauvegardez vos e-mails sur votre machine :

	> Nous présentons une méthode fiable ([cf. ce guide](https://github.com/joeyates/imap-backup/blob/main/docs/migrate-server-keep-address.md)), mais qui demande de bonnes connaissances en informatique. Vous pourriez avoir besoin de l'aide d'un⋅e administrateur⋅ice de Deuxfleurs.
	>
	> Si vous lui déléguez complètement l'opération, iel aura besoin de vos mots de passe chez votre ancien opérateur mail, chez Deuxfleurs, et manipulera votre sauvegarde d'e-mails. On vous recommande dans ce cas de changer vos mots de passe avant et après l'opération, et de vous armer de confiance.

	* Faites de la place ! Chez votre fournisseur actuel, supprimez votre corbeille, vos vieux e-mails volumineux et inutiles, etc.

	* Téléchargez l'outil [`imap-backup`](https://github.com/joeyates/imap-backup/).

	* Utilisez `imap-backup setup` pour renseigner vos identifiants ainsi que le serveur IMAP de votre fournisseur mail actuel.

	* Lancez `imap-backup backup --accounts=<votre_adresse_mail>`, ce qui va télécharger tous vos e-mails sur votre ordinateur (c'est long).
	
1. Communiquez l'adresse courriel souhaitée à un⋅e administrateur⋅ice de Deuxfleurs. Iel devra réaliser les opérations suivantes :

	* ajouter le nom de domaine à la liste des [domaines gérés par Deuxfleurs dans le LDAP](https://guichet.deuxfleurs.fr/admin/ldap/ou=domains,ou=groups,dc=deuxfleurs,dc=fr). Pour cela, une fois sur la page guichet :
		* appuyer sur le bouton vert « +objet »
		* Dans `Identifiant`, on peut mettre l'identifiant de l'usager⋅e concerné⋅e
		* Dans `Description`, mettre quelque chose comme `Nom de domaine de XXX`
		* Dans `StructuralObjectClass`, mettre `groupOfNames`
		* Dans `ObjectClass`, mettre `groupOfNames`, puis, à la ligne, `top`, et, encore à la ligne, `dNSDomain`
		* Valider avec le bouton « Créer l'objet »
		* Ajouter un attribut `domain` et y mettre le nom de domaine de l'adresse courriel souhaitée
	* ajouter le nom de domaine à la [table de signature DKIM](https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/cluster/prod/app/email/config/dkim/signingtable). Une fois fait, iel devra la déployer :
		* il faut lancer le `tlsproxy` du dépôt `nixcfg` en visant `prod`
		* sur un autre terminal, se connecter en SSH sur une de ces machines
		* sur encore un autre terminal, en local, à l'intérieur du dossier `nixcfg`, se situer à l'emplacement `cluster/prod/app/email/deploy`. Puis lancer `nomad plan email.hcl`. Si et seulement si la sortie paraît satisfaisante, alors lancer `noman run email.hcl`.
	* ajouter l'adresse courriel dans l'entrée `mail` de votre profil utilisateur LDAP. Il doit y avoir une adresse courriel par ligne, et il faut laisser l'adresse en `@deuxfleurs.fr` à la première. On peut ici ajouter autant d'alias que l'on veut, et tout sera reçu sur la même boîte de réception.

1. Vous devez ensuite rajouter les entrées pour votre nom de domaine en éditant votre zone DNS (si vous n'y comprenez rien, faites-vous aider de l'admin) :

	```
	# L'entrée MX pour recevoir les e-mails
	@  MX  10 smtp.deuxfleurs.fr.
	# L'entrée SPF pour autoriser notre IP à délivrer des e-mails en votre nom 
	@  TXT  "v=spf1 include:deuxfleurs.fr -all"
	# L'entrée DKIM pour autoriser notre postfix+opendkim à délivrer des e-mails en votre nom
	smtp._domainkey CNAME smtp._domainkey.deuxfleurs.fr.
	# L'entrée DMARC pour indiquer le comportement à adopter si les contraintes précédentes ne sont pas satisfaites
	_dmarc CNAME _dmarc.deuxfleurs.fr.
	```

1. À partir de ce point, vous pouvez envoyer et recevoir vos e-mails via notre infrastructure. [Suivez le Guide !](@/prise_en_main/email.md)

	Il faudra par contre attendre 24/48h pour que les changements DNS se propagent dans le monde entier. Vous pourriez encore recevoir quelques e-mails chez votre ancien fournisseur dans l'intervalle. Passé ce délai, vérifiez dans l'interface e-mail de votre ancien fournisseur.

1. Si vous avez sauvegardé vos e-mails sur votre machine, il est finalement temps de les envoyer sur les serveurs de Deuxfleurs (toujours avec l'aide optionnelle de l'admin, si vous êtes bloqué⋅e) :

	* Assurez-vous d'avoir terminé la sauvegarde en étape 1.

	* Trouvez sur votre machine le dossier contenant la configuration d'`imap-backup` et votre sauvegarde d'e-mails (`~/.imap-backup/` sur Linux). 

	* Modifiez manuellement le fichier de configuration d'`imap-backup` et le nom du dossier de votre sauvegarde comme expliqué dans [le guide sus-cité](https://github.com/joeyates/imap-backup/blob/main/docs/migrate-server-keep-address.md). Imaginons que votre adresse mail est `moi@chezmoi.fr` :

		* Remplacez à la ligne `username` votre e-mail par `moi-avant@chezmoi.fr` dans le fichier de configuration `config.json` d'`imap-backup`.
		* Dans le même fichier, à la ligne `local_path`, remplacez le nom du dossier `moi_chezmoi.fr` par `moi-avant_chezmoi.fr`.
		* Renommez le dossier `moi_chezmoi.fr` en `moi-avant_chezmoi.fr` dans le dossier d'`imap-backup`.

	* Utilisez `imap-backup setup` pour renseigner vos identifiants Deuxfleurs (toujours le même e-mail, et votre mot de passe chez nous), ainsi que le serveur IMAP de Deuxfleurs (`imap.deuxfleurs.fr`).

	* C'est le grand moment de la migration. Toujours en imaginant que votre adresse mail est `moi@chezmoi.fr`, lancez la commande suivante : 

		```imap-backup migrate moi-avant@chezmoi.fr moi@chezmoi.fr --destination-delimiter "."```

		La commande devrait produire peu de lignes, et vous devriez voir (par exemple sur [sogo.deuxfleurs.fr](https://sogo.deuxfleurs.fr)) vos anciens e-mails apparaître. Si ce n'est pas le cas, appelez à l'aide.

		Une fois cette longue commande terminée, c'est terminé. Bienvenu⋅e chez nous :)
