---
title: "Prérequis pour un nœud"
description: "Prérequis pour un nœud"
date: 2024-05-30
dateCreated: 2021-12-28T14:33:59.088Z
weight: 10
extra:
  parent: 'operations/noeud.md'
---


Dans ce guide, nous allons expliquer comment installer une grappe de serveurs, en vue d'un hébergement pour Deuxfleurs.



# Choix des ordinateurs

Héberger des services web est une tâche à la portée de la plupart des ordinateurs des 10 dernières années. Néanmoins, essayer de faire tourner correctement des applications lourdes sur une toute petite machine n'est pas une partie de plaisir. Alors, quitte à installer une nouvelle grappe, la vie sera plus agréable si ses machines disposent d'au moins :

* 4 Go de RAM
* 1 To de disque (dur ou SSD)

# Choix d'un site géographique

Dans la plupart des cas, le nouveau noeud sera ajouté à un site géographique existant.

Si il est envisagé d'installer un nouveau site géographique, voir : [guide d'installation d'un nouveau site géographique](@/operations/site.md)

