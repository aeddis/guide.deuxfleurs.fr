---
title: "Créer une BDD"
description: "Création d'une base de données pour une nouvelle application"
date: 2022-12-22
dateCreated: 2022-12-22
weight: 11
extra:
  parent: 'operations/deployer.md'
---

## 1. Create a LDAP user and assign a password for your service

Go to guichet.deuxfleurs.fr

  1. Everything takes place in `ou=services,ou=users,dc=deuxfleurs,dc=fr`
  2. Create a new user, like `johny`
  3. Generate a random password with `openssl rand -base64 32`
  4. Hash it with `slappasswd`
  5. Add a `userpassword` entry with the hash

This step can also be done using the automated tool `secretmgr.py` in the app folder.

## 2. Connect to postgres with the admin users

```bash
# 1. Launch ssh tunnel given in the README 
# 2. Make sure you have postregsql client installed locally
psql -h localhost -U postgres -W postgres
```

## 3. Create the binded users with LDAP in postgres + the database

```sql
CREATE USER sogo;
Create database sogodb with owner sogo encoding 'utf8' LC_COLLATE = 'C' LC_CTYPE = 'C' TEMPLATE template0;
```
