---
title: "Infrastructures"
description: "Infrastructures"
weight: 90
extra:
  parent: 'infrastructures/_index.md'
---

Ce manuel documente la dimension matérielle du numérique chez Deuxfleurs. On y recense les ordinateurs, le lieu où ils sont, les connexions réseaux nécessaires, l'énergie consommée, l'impact de fabrication, de fin de vie, etc.


