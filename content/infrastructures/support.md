---
title: "Support"
description: "Serveurs en support"
weight: 40
extra:
  parent: 'infrastructures/machines.md'
---

Les serveurs de support servent pour les sauvegardes et la supervision des serveurs de production (eg. Grafana, Minio).
De par leur rôle, ils participent au bon fonctionnement de la production.
Ils n'ont pas de données personnelles brutes mais les métriques collectées peuvent refléter certains comportement des usager·es
et les sauvegardes, bien qu'elles soient chiffrées, contiennent tout de même des données personnelles.

**Feuille de route :** Il est prévu de rationaliser l'usage de ces serveurs, c'est à dire voir si on peut mobiliser moins de ressources matériels tout en continuant
d'assurer le service de support.

# Suresnes (Mercure)

![Image d'illustration du serveur](/img/serv_dellr710.jpg)

Ce serveur est situé à domicile derrière une connexion FTTH Free.
Il est en charge de la surveillance de la production (métrologie, etc.) et des sauvegardes des systèmes de fichier et base de données.

| Désignation | Rôle | Quantité | Détails |
| -- | -- | -- | -- |
| [Microtik RB4011iGS+RM](https://mikrotik.com/product/rb4011igs_rm) | Routeur | x1 | Routeur et pare-feu, ports 1x10G SFP+ et 10x1G |
| Serveur Dell R710  | Hyperviseur | x3 | 2 socket, Xeon E5520 (4c8t @ 2.26Ghz) <br> 80Go RAM, 500GB NVMe, 1TB RAID matériel, réseau LACP 2x1G |

Seulement une partie du serveur est mise à disposition de Deuxfleurs :

| Désignation | Rôle | Quantité | Détails |
| -- | -- | -- | -- | 
| metro.mercure.site | LXC | x1 | 2 CPU, 2Go RAM, 25 GB NVMe |
| bkp.mercure.site (deprecated) | VM | x1 | 4 vCPU, 8Go RAM, 40 GB Block Storage |
| minio | S3 | x1 | Sert pour les sauvegardes |

# Rennes (Jupiter)

![Photo de la tour à Rennes](/img/serv_io.jpg)

Le serveur est situé à domicile derrière une connexion FTTH Free.
Il est en charge des sauvegardes de Garage.

| Désignation | Rôle | Quantité | Détails | Refs |
| -- | -- | -- | -- | -- |
| Tour un peu vieille | Serveur | x1 | AMD Phenom II X4 955 @ 3.2 GHz (4 cœurs)<br>4Go RAM, SSD 250Go + HDD 2To | `io` |
| Freebox Mini 4k | Routeur | x1 | 4 ports ethernet @ 1Gbit/s, WAN Fibre 1 Gbit/s symétrique | N/A |


