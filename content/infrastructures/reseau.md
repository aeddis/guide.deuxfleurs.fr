---
title: "Réseau"
description: "Réseau"
date: 2021-11-09T12:55:03.277Z
dateCreated: 2021-11-09T12:55:01.156Z
weight: 30
extra:
  parent: 'infrastructures/_index.md'
---

Cette page regroupe un résumé de tous les problèmes que vous pourriez rencontrer en voulant faire de l'auto hébergement avec "votre connexion internet".

# Côté Opérateur


## Congestion

### Congestion sur la livraison

Entre janvier et mars, les serveurs hébergés derrière une connexion Free ont eu des problèmes en soirée.
Le problème a été résolu depuis.

Plus d'informations ici : <https://www.aduf.org/viewtopic.php?t=286599&start=0>

### Congestion liée au peering

*À compléter*

## Adressage

### Pas d'IPv4 publique

Certains FAI ne donnent pas d'IPv4 publique du tout (même pas au niveau du routeur).
À la place, ils mettent en place un NAT nommé carrier-grade NAT que vous ne pouvez pas configurer.
Parfois, il suffit de les appels.

Exemple : Ora/Viti en Polynésie française

### IPv4 partagée

D'autres FAI ont trouvé une solution alternative à l'épuisement des adresses IPv4:
partager une addresse IP entre plusieurs abonnés. Cela peut se faire par exemple
en allouant 1/4 de la plage des ports disponibles à chacun de 4 clients partageant
la même addresse IP (par exemple les ports de 32768 à 49151).

Exemple : Free/FTTH (possibilité de demander une IP entière dans l'espace client)

### Pas d'IPv6 du tout

FAI connus pour ne pas proposer d'IPv6 sur la majorité des connections :

* SFR/FTTH

### IPv6 de mauvaisee qualité

*Ajouter des explications à propos du tunneling*

### Adresse IP publique dynamique

FAI connus pour proposer une adresse IP publique dynamique :

* Orange/ADSL (rotation quotidienne et à chaque resynchro)
* Orange/FTTH (rotation ~1 fois/mois)

## Autre

### Blocage du port 25 en sortie (impossibilité d'héberger un serveur email)

* Débloquable chez Free/\* dans l'interface (gratuit)
* Bloqué chez Orange/\* (sauf à passer sur une offre pro et encore...)
* Débloqué chez SFR/FTTH
* Débloqué chez Numéricable/Coaxial
* Inconnu chez Bouygues/\*

### Reverse DNS

*Expliquer pourquoi c'est utile*

* Orange : Ne propose pas de configurer le reverse
* SFR : Inconnu, probablement que non
* Numéricable : Inconnu, probablement que non
* Bouygues : Inconnu, probablement que non
* Free : Oui mais service cassé dans certains cas (récupérations d'IPv4)

# Chez vous

## Votre routeur ("box")

### NAT hairpinning

Vous avez besoin du NAT hairpinning pour accéder aux services publics que vous proposez derrière un NAT depuis le réseau interne du serveur. Typiquement quand vous n'avez que de l'IPv4 et qu'une seule IP publique portée par votre routeur.

*Ajouter de la doc sur le NAT hairpinning*

Routeurs connus pour avoir des problèmes de NAT hairpinning :

* Orange : Probablement toutes les box
  * Livebox 2 Sagem
  * Livebox 4 Sagemcom
* Bouygues : Suspicions sur toutes les box
* Free : 100% OK
* Numéricable : Inconnu
* SFR : Inconnu

### Problèmes de qualité du routeur

*À compléter*
