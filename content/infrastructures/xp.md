---
title: "Expérimentation"
description: "Expérimentation"
weight: 40
extra:
  parent: 'infrastructures/machines.md'
---

Les serveurs d'expérimentation servent à tester les nouvelles configurations, les nouveaux logiciels,
et le nouveau matériel. Ils permettent aux opérateur·ices de se familiariser avec leurs modifications et de minimiser l'impact d'un changement sur les serveurs de production,
et donc sur la disponibilité des services. Ces machines ne contiennent pas de données personnelles et ne sont pas critiques, elles n'ont pas besoin de rester tout le temps allumées.
Il n'est pas nécessaire d'être opérateur·ice pour gérer une de ces machines.

Ces serveurs sont organisés dans un cluster Nomad séparé de la production.  Ce cluster fonctionne uniquement en IPv6.
Cela permet de ne pas interférer avec le cluster de production lorsque des noeuds d'expérimentations et de production
cohabitent sur le meme site.

Chaque serveur est accessible en SSH en IPv6 via le nom `XYZ.machine.staging.deuxfleurs.org`.


# Orsay (Neptune)

![Photo d'illustration du Lenovo Tiny](/img/serv_m73tiny.jpg)

Cluster staging.

| Désignation | Rôle | Quantité | Détails | Refs |
| -- | -- | -- | -- | -- |
| ThinkCentre M73 Tiny | Serveur | x1 | 2 cœurs, 8Go RAM, HDD 500Go | caribou |


# Bruxelles (Bespin)

Cluster staging.

| Désignation | Rôle | Quantité | Détails | Refs |
| -- | -- | -- | -- | -- |
| ThinkCentre M73 Tiny | Serveur | x1 | 2 cœurs, 8Go RAM, HDD 500Go | df-pw5 |


# Rennes (Jupiter)

Cluster staging.

| Désignation | Rôle | Quantité | Détails | Refs |
| -- | -- | -- | -- | -- |
| ThinkCentre M73 Tiny | Serveur | x1 | 2 cœurs, 4Go RAM, HDD 500Go | origan |


# Nantes (Corrin)

Cluster staging.

| Désignation | Rôle | Quantité | Détails | Refs |
| -- | -- | -- | -- | -- |
| ThinkCentre M73 Tiny | Serveur | x1 | 2 cœurs, 4Go RAM, HDD 500Go | piranha |
