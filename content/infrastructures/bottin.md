---
title: "Bottin"
description: "Un annuaire LDAP pour le distribué"
date: 2021-11-09T12:40:01.746Z
dateCreated: 2021-11-09T12:39:59.725Z
weight: 10
extra:
  parent: 'infrastructures/logiciels.md'
---

# Bottin

Bottin est un annuaire LDAP distribué développé en Go visant la simplicité d'installation et d'utilisation (comparé à des solutions comme OpenLDAP ou Keycloak) tout en offrant la résilience que l'on peut attendre d'un système d'authentification. Il se base pour le stockage distribué sur [Consul](https://www.consul.io/).

## Statut du développement

Bottin est actuellement utilisé en production pour deuxfleurs. Il est cependant toujours en développement actif.
Le code de Bottin se trouve ici : <https://git.deuxfleurs.fr/Deuxfleurs/bottin>
