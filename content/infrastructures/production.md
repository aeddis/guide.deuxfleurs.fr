---
title: "Production"
description: "Production"
weight: 40
extra:
  parent: 'infrastructures/machines.md'
---

Les serveurs de productions sont ceux qui font tourner les services accédés par les usager·es (eg. Plume, Matrix, Cryptpad).
Si ils sont inaccessibles, alors les services ne fonctionnent plus. Et si une personne malveillante y accède, elle peut avoir accès à des données
personnelles des usager·es. C'est donc le rôle le plus critique.

**Feuille de route :** Bien que nous disposions aujourd'hui de 3 sites pour le cluster de production,
et que nous puissions fonctionner en mode dégradé sur 2 sites, nous allons développer de nouveaux sites
pour etre plus résilients en cas de panne longue d'un site.  Par ailleurs, un des sites (Bespin à Bruxelles)
n'est pas utilisable comme point d'entrée actuellement, c'est un travail en cours.

Chaque serveur est accessible en SSH en IPv6 via le nom `XYZ.machine.deuxfleurs.fr`.

# Orsay (Neptune)

![Photo des 3 serveurs à Orsay](/img/serv_neptune.jpg)

Les serveurs sont situés à domicile derrière une connexion FTTH Free.

Tous les services peuvent fonctionner sur ce site, et c'est également un point d'entrée public des services.

| Désignation | Rôle | Quantité | Détails | Refs|
| -- | -- | -- | -- | -- |
| ThinkCentre M710q Tiny | Serveur | x2 | 2 cœurs, 8Go RAM, SSD 500Go + HDD 500Go | celeri, concombre |
| ThinkCentre M710q Tiny | Serveur | x1 | 2 cœurs, 12Go RAM, SSD 500Go + HDD 500Go | courgette |
| D-Link DGS-108gl | Switch | x1 | 8 ports ethernet @ 1Gbit/s | |
| Box Free | Routeur | x1 | N/A | |

# Lille (Scorpio)

![Photo des 3 serveurs à Lille](/img/serv_scorpio.jpg)

Les serveurs sont situés à domicile derrière une connexion FTTH Free.

Tous les services peuvent fonctionner sur ce site, et c'est également un point d'entrée public des services.

| Désignation | Rôle | Quantité | Détails | Refs |
| -- | -- | -- | -- | -- |
| ThinkCentre M83 | Serveur | x2 | 2 cœurs, 12Go RAM, SSD 500Go + HDD 2To | ananas, abricot |
| Freebox | Routeur | x1 | N/A | N/A |

# Bruxelles (Bespin)

![Photo des 3 serveurs à Bruxelles](/img/serv_bespin.jpg)

Les serveurs sont situés à domicile derrière une connexion FTTH Belgacom.

Ce site n'est actuellement pas complètement utilisable pour Deuxfleurs, l'IPv4 publique étant partagée avec d'autres
usages.  Par conséquent, ce n'est pas un point d'entrée public des services.  Cependant, certains services peuvent
quand meme fonctionner sur ce site, et il participe pleinement au cluster Garage pour redonder le stockage.

| Désignation | Rôle | Quantité | Détails | Refs |
| -- | -- | -- | -- | -- |
| ThinkCentre M710q Tiny | Serveur | x3 | 2 cœurs, 8Go RAM, SSD 500Go + HDD 500Go | df-ymk, df-ymf, df-ykl |
| N/A | Switch | x1 | N/A | N/A |
| N/A | Routeur | x1 | N/A | N/A |

# Rennes (Dathomir)

Site en cours de construction pour améliorer la redondance en cas de panne d'un site.

Accès Internet : FTTH Free

# Nantes (Corrin)

Site en cours de construction pour améliorer la redondance en cas de panne d'un site.

Accès Internet : FTTH Rézine via Axione

# Anciens sites historiques

Ces sites ne sont plus en production.

## Atuin (Rennes)

![Photo des serveurs d'Atuin](/img/serv_atuin.jpg)

## Lyon (Orion)

![Photo des serveurs d'Orion](/img/serv_orion.jpg)
