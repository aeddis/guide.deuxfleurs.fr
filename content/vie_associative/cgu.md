---
title: "Conditions générales"
description: "Conditions générales d'utilisation"
weight: 105
extra:
  parent: 'vie_associative/_index.md'
---

En utilisant les services de Deuxfleurs, vous acceptez d'être lié par
les conditions suivantes.

## Évolution des conditions générales d\'utilisation

Deuxfleurs se réserve le droit de mettre à jour et modifier ces
conditions. Dans ce cas, Deuxfleurs informe les personnes concernées par
mail si elle le peut ou par un affichage sur le site.

## Accès aux services

Certains services sont en accès libre, d\'autre requièrent un compte.
Vous pouvez obtenir un compte par cooptation d\'un membre existant ou en
faisant une demande directement par email à l\'association. Certains
services sont soumis à l\'approbation préalable de Deuxfleurs selon les
ressources disponibles. La liste des services et leurs conditions
d'accès peut-être obtenue en contactant l\'association.

## Fonctionnement

### Délais de mise en service

Deuxfleurs propose ses services grâce à des bénévoles, de ce fait
Deuxfleurs ne s'engage sur aucun délai de mise en service. Nous essayons
toutefois de faire de notre mieux pour les fournir dans les 7 jours.

### Transmission sécurisée d'identifiants

Deuxfleurs ne vous demandera jamais de communiquer vos mots ou phrases
de passe. Lorsque Deuxfleurs doit vous transmettre un identifiant,
Deuxfleurs le fera via email ou en physique.

### Intervention en cas de panne

En cas de panne constatée et si aucun message n'atteste sur la page de
statut que Deuxfleurs est en train de corriger le dysfonctionnement,
vous devez faire un signalement via Matrix.

Deuxfleurs propose l'ensemble de ses services grâce à des bénévoles qui
feront ce qu'ils et elles peuvent pour résoudre les problèmes techniques
qui pourraient subvenir.

### Intégrité des données

Deuxfleurs réalise des sauvegardes quotidiennes afin d'éviter des pertes
de données. Les sauvegardes sont conservées selon cette politique
suivante : 1/jour jusqu'à 1 mois, 1/semaine jusqu'à 3 mois, 1/mois
jusqu'à 1 an. Les sauvegardes sont stockées de manière chiffrée avec un
mot de passe par l\'association à Suresnes.

Deuxfleurs ne peut être tenu responsable de l'intégrité des données et
des conséquences liées à une perte de données.

Deuxfleurs recommande aux usager·es d'avoir une sauvegarde locale afin
d'assurer l'intégrité de leurs données.

### Responsabilité de Deuxfleurs

Deuxfleurs est assujetti à une obligation de moyens. En cas de
défaillance, Deuxfleurs ne peut être tenu pour responsable des dommages
indirects tels que pertes d'exploitation, préjudices commerciaux, perte
de Clientèle, de chiffre d'affaires, de bénéfices ou d'économies prévus,
ou de tout autre préjudice indirect.

### Mésusage des services

Vous devez respecter les lois et réglementations en vigueur lors de
l'usage des services proposés que ce soit en matière de respect de la
vie privée, d'envoi de mails en grande quantité, de propriété
intellectuelle, de propos discriminatoires, d'appel à la haine, de
harcèlement, d'atteinte aux libertés fondamentales de personnes, etc.

En cas d'usage prohibé, Deuxfleurs peut se trouver dans l'obligation de
déclencher la suspension totale ou partielle du service, le retrait de
contenu, ou toute autre mesure que les lois et réglementations lui
imposent. Vous devez respecter les autres utilisateurs en faisant preuve
de civisme et de politesse. Deuxfleurs se réserve le droit de supprimer
tout contenu paraissant non pertinent ou contrevenant à ces principes,
selon son seul jugement.

Par ailleurs, si un ou une utilisatrice abuse du service, par exemple en
monopolisant des ressources machines partagées, son contenu ou son accès
pourra être supprimé, si nécessaire sans avertissement ni négociation.
Deuxfleurs reste seul juge de cette notion « d'abus » dans le but de
fournir le meilleur service possible à l'ensemble des usagers et
usagères.

### Réponse en cas de signalement ou suspicion de contenu illégal

Deuxfleurs tient à assurer la vie privée des utilisateurs de ses
services, mais doit également lutter contre le détournement de ces
derniers à des fins illégales. Deuxfleurs s'engage à ne pas mettre en
place de système automatisé de détection et filtrage de contenus
illégaux. En lieu et place est appliqué un protocole reposant sur trois
paliers, afin de limiter les activités de surveillance au très strict
nécessaire :
 - Le palier 0 est celui par défaut. Dans cette situation, les membres
   et administrateur·ice·s de Deuxfleurs ne peuvent inspecter que les
   métriques agrégées, à l'échelle d'un service ou de l'infrastructure,
   tels que les débits globaux de connexions et de transferts, ou les
   taux généraux d'erreurs et de succès, sans accéder aux métriques
   spécifiques d'un ou plusieurs utilisateurs.
 - Le palier 1 ne peut être accédé seulement si, suite à une décision
   publique et collective, les métriques précédentes sont jugées
   suspectes, ou, en guise d'exception, en cas de signalement de contenu
   illégal. Dans ce palier, les membres et administrateur·ice·s de
   Deuxfleurs peuvent accéder aux journaux des services, et ainsi
   inspecter notamment les adresses IP qui se sont connectées, leur
   géolocalisation, la durée des connexions, les agents utilisateur,
   ainsi que les métadonnées spécifiques au service, par exemple le nom
   des salons dans le cadre de visioconférences.
 - Le palier 2 ne peut être accédé seulement suite à une décision
   collective et publique face aux données inspectées au palier 1. Dans
   ce palier, un responsable de Deuxfleurs peut, sans dissimuler sa
   mission, accéder aux ressources concernées. Dans le cadre de
   communications vidéos, audios, ou textuelles, cela implique de se
   connecter au salon, avec un pseudonyme clair tel que «Modération
   Deuxfleurs». C'est à ce stade-là que se formalisera ou non un
   processus de signalement auprès des autorités.

### Devenir des services

Deuxfleurs peut par ailleurs choisir (de résilier des abonnements ou)
d'arrêter des services si Deuxfleurs estime ne plus être en mesure de
fournir lesdits services. Si Deuxfleurs en a la possibilité, elle fera
de son mieux pour laisser un délai suffisant pour permettre à tout le
monde de migrer sereinement.

### Support et conseil

Vous pouvez nous signaler des souhaits sur la création de futur service,
mais sachez que nous ne pourrons pas créer de nouveaux services en moins
de 6 mois. Deuxfleurs pourra toutefois vous rediriger vers des chatons à
même de répondre à vos demandes.

En dehors de dysfonctionnement technique, Deuxfleurs propose également
de vous aider dans la réalisation de votre projet avec les services de
Deuxfleurs selon l\'envie et le temps disponible de ses membres.
Deuxfleurs se réserve le droit de facturer le temps de support et de
conseil pour des projets importants après avoir prévenu les utilisateurs
en amont.

### Résiliation d'un compte

Si vous souhaitez résilier un compte, vous devez le signaler à
Deuxfleurs.

## Nos engagements

Deuxfleurs n'exploitera vos données personnelles que dans le cadre de
ces 5 finalités:

-   fournir le service pour lesquels vous avez transmis vos données
-   produire d'éventuelles statistiques anonymisées et agrégées
-   vous prévenir d'un changement important sur le service (panne,
    notification d'intrusion et de vol de données, changement
    d'interface, date d'arrêt du service\...)
-   obtenir votre avis sur les services et l'action de l'association
-   vous inviter à participer à un évènement de Deuxfleurs

Deuxfleurs ne transmettra ni ne revendra vos données personnelles (votre
vie privée nous tient - vraiment - à cœur). Votre contenu vous
appartient tout autant, toutefois, nous vous encourageons à le publier
sous licence libre si c'est pertinent.

Une modification du paragraphe précédent, contrairement au reste de la
présente charte, ne peut se faire simplement par une simple
notification. Si une telle modification devait survenir, elle :

-   Ne serait pas rétroactive
-   Demandera un accord explicite de votre part pour continuer à
    utiliser les services fournis par Deuxfleurs
-   Provoquera une révocation préalable à la modification auprès de tous
    les soutiens de Deuxfleurs ayant à cœur les problématiques de
    respect de la vie privée.

### Charte CHATONS

Deuxfleurs s'engage à respecter la charte du Collectif des Hébergeurs
Alternatifs, Transparents, Ouverts, Neutres et Solidaires dans le cadre
de son activité d'hébergeur et de fourniture de services en ligne.

Plus d'information sur la charte C.H.A.T.O.N.S. :
[chatons.org/charte](https://chatons.org/charte)

### Localisation des données

Vos données sont localisées en France et en Belgique, sur les serveurs
de nos membres. Un détail complet de l\'infrastructure et de
l\'emplacement des données par service est disponible sur notre
page [Infrastructures](https://guide.deuxfleurs.fr/infrastructures/machines/#zones).

### Devenir des données

Une fois le compte clôturé, Deuxfleurs peut procéder à la suppression
des données.

Certains services en libre accès permettent de configurer la péremption
des données, d'autres les conservent de façon permanentes, mais vous
pouvez demander leur retrait si vous pouvez prouver que vous en êtes
l'auteur⋅ice.

### Exercice de vos droits

Conformément à l'article 34 de la loi « Informatique et Libertés », vous
pouvez exercer les droits suivant en envoyant un mail à ca arobase
deuxfleurs point fr :

-   droits d'accès, de rectification, d'effacement et d'opposition
-   droit à la limitation du traitement
-   droit à la portabilité des données
-   droit de ne pas faire l'objet d'une décision individuelle
    automatisée

### RGPD

Vous et Deuxfleurs s'engagent à respecter la réglementation en vigueur
applicable au traitement de données à caractère personnel et, en
particulier, le règlement (UE) 2016/679 du Parlement européen et du
Conseil du 27 avril 2016 applicable à compter du 25 mai 2018, dite RGPD.

## Litige et juridiction compétente

Le droit applicable aux présentes est le droit français. En cas de
différent, les parties recherchent une solution amiable. Si la démarche
échoue, le litige sera tranché par le Tribunal de Grande Instance de
Rennes.

Le fait que l'usager ou Deuxfleurs ne se prévale pas à un moment donné
de l'une des présentes conditions générales et/ou tolère un manquement
par l'autre partie ne peut être interprété comme valant renonciation par
l'usager ou Deuxfleurs à se prévaloir ultérieurement de ces conditions.

La nullité d'une des clauses de ces conditions en application d'une loi,
d'une réglementation ou d'une décision de justice n'implique pas la
nullité de l'ensemble des autres clauses. Par ailleurs l'esprit général
de la clause sera à conserver en accord avec le droit applicable.
