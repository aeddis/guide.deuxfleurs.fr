---
title: "Mentions légales"
description: "Mentions légales"
weight: 110
extra:
  parent: 'vie_associative/_index.md'
---

# Éditeur et hébergeur

## Deuxfleurs

Deuxfleurs est l'éditeur et l'hébergeur de l'ensemble de ses services et contenus.\
Association loi 1901 déclarée en préfecture d'Ille-et-Vilaine le 29 janvier 2020.

Numéro RNA : W353020804 \
Numéro SIRET : 89961256800019

Siège social : \
RDC - Appt. 1 \
16 rue de la Convention \
59800 Lille \
FRANCE

# Direction de la publication

* Quentin Dufour
* Adrien Luxey

# Informatique et libertés

## Informations personnelles collectées

En France, les données personnelles sont notamment protégées par la loi n° 78-17 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l’article L. 226-13 du Code pénal et la Directive Européenne du 24  octobre 1995.

En tout état de cause Deuxfleurs ne collecte des informations personnelles relatives à l’utilisateur (nom, adresse électronique, coordonnées téléphoniques) que pour le besoin des services proposés par les sites du réseau Deuxfleurs. L’utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu’il procède par lui-même à leur saisie. Il est alors précisé à l’utilisateur des sites du réseau Deuxfleurs le caractère obligatoire ou non des informations qu’il serait amené à fournir.

## Rectification des informations nominatives collectées

Conformément aux dispositions de l’article 34 de la loi n° 48-87 du 6 janvier 1978, l’utilisateur dispose d’un droit de modification des données nominatives collectées le concernant. Pour ce faire, l’utilisateur envoie à Deuxfleurs :

* un courrier électronique à ca (arobase) deuxfleurs.fr
* un courrier à l’adresse du siège de l’association (indiquée ci-dessus) en indiquant son nom ou sa raison sociale, ses coordonnées physiques et/ou électroniques, ainsi que le cas échéant la référence dont il disposerait en tant qu’utilisateur du site Deuxfleurs.

La modification interviendra dans des délais raisonnables à compter de la réception de la demande de l’utilisateur.

## Limitation de responsabilité

Ce site comporte des informations mises à disposition par des communautés ou sociétés externes ou des liens hypertextes vers d’autres sites qui n’ont pas été développés par Deuxfleurs. Le contenu mis à disposition sur le site est fourni à titre informatif. L’existence d’un lien de ce site vers un autre site ne constitue pas une validation de ce site ou de son contenu. Il appartient à l’internaute d’utiliser ces informations avec discernement et esprit critique. La responsabilité de Deuxfleurs ne saurait être engagée du fait des informations, opinions et recommandations formulées par des tiers.

Deuxfleurs ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications techniques requises, soit de l’apparition d’un bug ou d’une incompatibilité.

Deuxfleurs ne pourra également être tenue responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site.

Des espaces interactifs (comme la solution de messagerie instantanée Matrix) sont à la disposition des utilisateurs sur le site Deuxfleurs. Deuxfleurs se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant, Deuxfleurs se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie…).

## Limitations contractuelles sur les données techniques

Deuxfleurs ne pourra être tenue responsable de dommages matériels liés à l’utilisation du site.

## Propriété intellectuelle

Les contenus sont publiés sous la responsabilité des utilisateurs.
