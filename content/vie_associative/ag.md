---
title: "Assemblées Générales"
description: "Assemblées Générales"
weight: 50
sort_by: "weight"
extra:
  parent: 'vie_associative/_index.md'
---

[↣ AG 2020](@/vie_associative/AG2020.md) - Assemblée générale constitutive 

[↣ AG 2021](@/vie_associative/AG2021.md) - Un an d'existence 

[↣ AG 2022](@/vie_associative/AG2022.md) - En plein développement de Garage

[↣ AG 2023](@/vie_associative/AG2023.md) - De nouvelles perspectives

[↣ AG 2024](@/vie_associative/AG2024.md) - Consolider les services de base (e-mail, sites statiques)
