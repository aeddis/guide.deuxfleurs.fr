---
title: "Ergonomie"
description: "Comment travailler durablement et en toute sécurité"
date: 2022-04-06T12:36:35.582Z
dateCreated: 2022-04-06T12:34:35.012Z
weight: 50
extra:
  parent: 'vie_associative/kb.md'
---

# Ergonomie

L'association emploie désormais plusieurs salarié.e.s qui travaillent à la maison sur poste informatique pour les beoins de Deuxfleurs. Offrons-leurs les meilleurs conditions de travail possible!

Pour être correctement installé pour travailler, suivre par exemple les recommandations de l'AST, ils ont toute une série de brochures ici: <https://www.ast35.fr/ergonomie>

Pour acheter du matériel (chaises, bureaux) reconditionnés, donc de qualité et pas cher, ici:

- <https://www.adopteunbureau.fr/>
- <https://bureaufute.fr/>
- <https://www.ecosiege.fr/>
