---
title: "Calendrier partagé"
description: "Calendrier partagé"
weight: 5
extra:
  parent: 'vie_associative/_index.md'
---

Le calendrier des événements internes et externes de l'association hébergé par  [SoGO](https://sogo.deuxfleurs.fr/SOGo/).

Tous les clients mails disposant d'un agenda devraient vous permettre d'afficher l'agenda Deuxfleurs d'une des trois façons suivantes.
Sur SoGO, il faut se rendre sur l'onglet « Agenda ». 
Sur le panneau de gauche, de cliquer sur le « + » (plus) en face de « Calendriers web », de renseigner l'adresse CalDAV ci-dessous (authentifié au non), et éventuellement de rentrer vos identifiants Deuxfleurs.



## Accès authentifié

(on voit tout et on peut tout modifier si on a un compte deuxfleurs)

Utilisez `<pseudo>@deuxfleurs.fr` pour vous identifier, pas seulement votre `<pseudo>`.

- CalDAV : <https://sogo.deuxfleurs.fr/SOGo/dav/lx/Calendar/22-63E29280-1-71341300/>
- WebDAV ICS : <https://sogo.deuxfleurs.fr/SOGo/dav/lx/Calendar/22-63E29280-1-71341300.ics>
- WebDAV XML : <https://sogo.deuxfleurs.fr/SOGo/dav/lx/Calendar/22-63E29280-1-71341300.xml>

## Accès public

(on voit seulement les trucs publics)

- CalDAV : <https://sogo.deuxfleurs.fr/SOGo/dav/public/lx/Calendar/22-63E29280-1-71341300/>
- WebDAV ICS : <https://sogo.deuxfleurs.fr/SOGo/dav/public/lx/Calendar/22-63E29280-1-71341300.ics>
- WebDAV XML : <https://sogo.deuxfleurs.fr/SOGo/dav/public/lx/Calendar/22-63E29280-1-71341300.xml>
