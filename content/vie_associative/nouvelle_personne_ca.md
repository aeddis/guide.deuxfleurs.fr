---
title: "Quand une personne rejoint le CA"
description: "Que faut-il faire pour l'accueillir ?"
date: 2022-03-02T09:00:00.009Z
dateCreated: 2022-03-02T08:58:55.011Z
weight: 60
extra:
  parent: 'vie_associative/kb.md'
---

# Comment accueillir une nouvelle personne au CA 

* L'ajouter au salon Matrix `deuxfleurs::bureau`.
* L'ajouter à la liste de diffusion `ca@deuxfleurs.fr` (*via* [Guichet](https://guichet.deuxfleurs.fr/)).
* Lui donner accès au compte en banque (contacter la banque).

	> TODO: Quels documents doit-elle fournir ?
  
* La former à l'utilisation de [HomeBank](http://homebank.free.fr/fr/) pour la comptabilité.
	Notre fichier de sauvegarde HomeBank étant stocké dans `git`, former l'administrateur⋅ice à `git`.

* S'assurer qu'elle dispose de comptes admin sur les services qui ne sont pas gérés par le [Bottin](@/infrastructures/bottin.md).
