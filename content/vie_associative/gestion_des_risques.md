---
title: "Gestion des risques"
description: "Gestion des risques"
weight: 120
extra:
  parent: 'vie_associative/_index.md'
---

Un risque se définit par la combinaison d'un danger, c'est à dire un évènement redouté, et sa probabilité d'occurence. La gestion des risques consiste à minimiser les risques en les évaluant et en mettant en place des solutions pour réduire leur impact.

# 🧰 Risques matériels

## Panne de composants informatiques

**Définition du risque**: Un serveur est composé d'un ensemble de pièces avec une durée de vie limitée. La panne d'une de ces pièces cause une interruption de service et possiblement une perte de données (comme dans le cas d'un disque dur par exemple). La probabilité de ces pannes est importante car certains composants ont une durée de vie limité, comme les disques durs, et de plus nous utilisons du matériel reconditionné qui a donc déjà été utilisé.

**Mesure mise en place**: Nous avons à disposition des pièces de rechange pour pouvoir réparer rapidement nos serveurs. De plus, nous avons en permanence plusieurs machines en production de sorte que lorsque l'une d'elle tombe en panne, les services qu'elle gérait sont automatiquement basculés sur les autres machines fonctionnelles. Les données sont répliquées sur 3 disques durs différents, de sorte que si un disque dur tombe en panne, cette dernière est toujours disponible sur 2 autres disques.

## Bug logiciel

**Définition du risque**: Un bug logiciel peut rendre un service indisponible voire impliquer une perte de données. Cette indisponibilité et cette perte de donnée, contrairement à une panne matériel, peut se propager à l'ensemble de nos serveurs.

**Mesure mise en place**: Les données, en plus d'être répliquées automatiquement, sont périodiquement sauvegardées via un logiciel indépendant sur une période d'au moins 2 mois sur un site distant (Suresnes), de sorte que le logiciel qui fournisse le service n'ait pas accès aux données de sauvegardes et ne puisse donc pas les effacer. Les services quant à eux sont définis déclarativement, dans chaque modification est archivée, de sorte qu'il est possible de revenir sur une version antérieure du service qui soit stable.

## Panne du réseau électrique ou internet

**Définition du risque**: Nous faisons appel à des prestataires externe pour l'électricité (eg. EDF) et la connexion Internet (eg. Free). Lors de travaux dans la rue, de tension sur le réseau (eg. froid ou confinement), ou simplement d'erreur de gestion, ces réseaux peuvent être coupés localement ou au niveau national. En résulte une indisponibilité des services pendant la coupure.

**Mesure mise en place**: Notre infrastructure est répartie sur des zones éloignées géographiquement (aujourd'hui Rennes et Orsay), de sorte qu'une panne locale ne nécessitera que de migrer les services d'une zone à l'autre. Pour le cas d'une panne au niveau national, nous acceptons ce risque : il est très faible et, la plupart du temps, le service est rétabli en quelques heures.

## Panne des autres prestataires et tiers-parties

**Définition du risque**: Deuxfleurs fait appel à un certains nombre de prestataires et tiers parties pour fournir ses services : bureau d'enregistrement DNS (Gandi), Hébergeur DNS (Gandi), certificats X.509 (Let's Encrypt), système d'exploitation (NixOS, Debian), logiciels (Docker Hub, Github, etc.). La panne ou le blocage par une de ces tiers-parties impacteraient les services de Deuxfleurs.

**Mesure mise en place**: Nous essayons de limiter le nombre de prestataires que nous utilisons et évitons de verrouiller fortement nos choix technologiques vis à vis d'un fournisseur particulier. En dehors de ces points, nous acceptons ces risques.

# 😵 Risques humains

## Erreur d'administration

**Définition du risque**: Une erreur d'administration des services peut aboutir à une interruption de services et à une perte de données sur l'ensemble de nos serveurs. Elle peut également générer beaucoup de stress pour la personne en charge de l'administration.

**Mesure mise en place**: Les données, en plus d'être répliquées automatiquement, sont périodiquement sauvegardées via un logiciel indépendant sur une période d'au moins 2 mois sur un site distant (Suresnes), de sorte que le logiciel qui fournisse le service n'ait pas accès aux données de sauvegardes et ne puisse donc pas les effacer. Les services quant à eux sont définis déclarativement, dans chaque modification est archivée, de sorte qu'il est possible de revenir sur une version antérieure du service qui soit stable. En cas d'erreur, la personne en charge de l'administration doit demander un accompagnement ou un relai pour limiter les risques d'aggravement de la situation et ne pas avoir à supporter cette situation stressante seule.

## Absence d'astreinte

**Définition du risque**: Que ce soit dans les datacenters au niveau matériel, ou dans les services informatiques, au niveau logiciel, de nombreuses organisations mettent en place des astreintes. Autrement dit, une ou plusieurs personnes identifiées doivent se rendre disponible sur des périodes données pour pouvoir intervenenir rapidement sur les serveurs. Dans le cas de Deuxfleurs, une astreinte empêcherait une personne de quitter son domicile pour des vacances ou même rendre visite à sa famille !

**Mesure mise en place**: Nous faisons en sorte d'avoir des serveurs chez des membres différents et d'avoir plusieurs personnes en mesure de réaliser une même opération de maintenance. Si un problème devait arriver pendant l'absence de la personne concernée, un autre membre devrait pouvoir prendre le relai dans les heures ou les jours qui viennent, par exemple en migrant les services chez lui ou en le réparant à distance.

## Perte d'expertise

**Définition du risque**: L'infrastructure que nous avons déployée demande des connaissances avancées en informatique détenues par une fraction des membres de l'association. Si cette fraction venait à quitter l'association, la maintenance de ses services pourrait ne plus être réalisée par les membres restants, mettant en péril sa pérennité.

**Mesure mise en place**: Nous essayons d'avoir toujours au moins deux personnes en mesure de réaliser une tâche. Nous essayons de documenter et standardiser notre infrastructure.

## Risques juridiques

**Définition du risque**: Deuxfleurs pourrait enfreindre la loi et voir sa pérennité engagée en cas de procès.

**Mesure mise en place**: Nous faisons une veille juridique concernant les hébergeurs. Nous mitigeons aussi ce risque via la co-optation qui permet de contrôler notre croissance et les personnes qui nous rejoignent.

# 👿 Malveillance

## Attaque informatique

**Définition du risque**: Une attaque informatique, venant d'un·e administrateur·ice ou d'une personne extérieure, peut aboutir à l'indisponibilité de nos services, à une perte de données, mais aussi à la fuite de ces dernières.

**Mesure mise en place**: Pour se protéger d'un·e administrateur·ice malveillant·e, le groupe d'administrateur·ices existant ne recrute que de nouvelles personnes sur cooptation et après avoir jugé cette personne de confiance. Nous prévoyions également de chiffrer de plus en plus de données côté client, de sorte qu'une personne en charge de l'administration ne soit pas en mesure de lire le contenu stocké sur les serveurs. Pour se protéger d'une personne extérieure, nous maintenons notre système à jour et avons entamé une démarche de défense en profondeur.

## Vol du matériel

**Définition du risque**: Une personne entrant par effraction chez un de nos membres pourrait voler les ordinateurs. Cet évènement peut impacter la disponibilité de nos services, l'intégrité de nos données, mais aussi la confidentialité de ces dernières.

**Mesure mise en place**: Nous acceptons ce risque pour le moment. Le chiffrement côté client, pour les services le supportant, permet de protéger la confidentialité des données. Nous prévoyions à terme de chiffrer les disques durs à froid en plus.

## Abus des services

**Définition du risque**: Une personne ayant des accès au service, où se les aillant fait voler, peut opérer un déni de service sur notre infrastructure, ou alors nuire à la réputation de notre service sur un réseau fédéré (eg. spam via email). Cela entraine une indisponibilité des services et/ou une dégradation plus ou moins durable de la qualité de ces derniers.

**Mesure mise en place**: Nous mitigeons actuellement ce risque via la cooptation, où nous faisons confiance aux nouveaux membres et les invitons à bien gérer leurs identifiants. 

