---
title: "Charte opérateur·ice"
description: "Charte opérateur·ice"
weight: 30
extra:
  parent: 'vie_associative/_index.md'
---

*Ce document est inspiré de [la charte du Parti Pirate](https://wiki.partipirate.org/images/4/45/Charte_de_l%27administrateur_-_Type.pdf)*.

# Missions

En acceptant ma nomination à la fonction d'opérateur·ice du système informatique de Deuxfleurs, je m'engage expressément à
respecter les termes des missions qui me sont confiées :
 - Œuvrer à garantir la stabilité des systèmes du système d'informations ;
 - Œuvrer à assurer la sécurité des systèmes du système d'informations face à une action malveillante externe
ou interne ;
 - Déployer de nouveaux services et participer à la rédaction de la documentation nécessaire ;
 - Soutenir les autres membres de l'association dans leurs missions.

Cette responsabilité est assurée à titre gratuit. En cas d'indisponibilité ou d'incompétence sur le domaine qui m'est
confié, je m'engage à en informer l'équipe technique ou, à défaut le Conseil d'Administration, dès que possible.

# Nature de la fonction

La nature de ma fonction est uniquement technique et est exécutée en dehors de toutes considérations stratégiques,
politiques et personnelles faisant preuve à tout instant d'éthique, de neutralité et de respect de la confidentialité des
données que je suis amené à manipuler.
Je m'engage :
 - À ne pas diffuser, céder ou manquer sciemment à la protection des données du système d'informations de Deuxfleurs ;
 - À ne pas utiliser à des fins personnelles les systèmes informatiques et services de Deuxfleurs ;
 - À ne pas introduire d'accès caché dans un système, permettant à une personne non autorisée d'en prendre le
contrôle ;
 - À coopérer en bonne entente avec les autres membres du groupe des opérateur·ices ;
 - À ne conserver aucune donnée à caractère confidentiel ;
 - À ne pas user de mes accès pour expressément usurper une identité, consulter des données auxquelles je n'ai
pas accès en tant que simple utilisateur ou permettre à un tiers de le faire sans le consentement explicite du
gestionnaire de ces informations ;
 - Et, enfin, à ne pas aller à l'encontre de l'intérêt de Deuxfleurs et de ses adhérents.

# Code Pénal

J'ai lu et compris les articles du Code Pénal ci-après :

**Article 314-1 du Code pénal**
« L'abus de confiance est le fait par une personne de détourner, au préjudice d'autrui, des fonds, des valeurs ou un
bien quelconque qui lui ont été remis et qu'elle a acceptés à charge de les rendre, de les représenter ou d'en faire un
usage déterminé. L'abus de confiance est puni de trois ans d'emprisonnement et de 375000 euros d'amende ».

**Article 323-2 du Code pénal**
« Le fait d'entraver ou de fausser le fonctionnement d'un système de traitement automatisé de données est puni de
cinq ans d'emprisonnement et de 75000 euros d'amende ».
Article 323-3 du Code pénal
« Le fait d'introduire frauduleusement des données dans un système de traitement automatisé ou de supprimer ou
de modifier frauduleusement les données qu'il contient est puni de cinq ans d'emprisonnement et de 75000 euros
d'amende »

# Code Civil

J'ai lu et compris les articles du Code Civil ci-après 

**Article 1240 du Code Civil**
« Tout fait quelconque de l'homme, qui cause à autrui un dommage, oblige celui par la faute duquel il est arrivé à le
réparer ».

**Article 1241 du Code Civil**
« Chacun est responsable du dommage qu'il a causé non seulement par son fait, mais encore par sa négligence ou par
son imprudence ».

**Article 1243 alinéa 1er du Code Civil**
« On est responsable non seulement du dommage que l'on cause par son propre fait, mais encore de celui qui est
causé par le fait des personnes dont on doit répondre, ou des choses que l'on a sous sa garde ».

# Démission

Je m'engage à agir en personne raisonnable, mon comportement s'il venait à s'écarter de cette éthique, pourrait
engager ma responsabilité civile vis-à-vis de Deuxfleurs et réglementairement vis-à-vis du Conseil d'Administration.

En tout état de cause, si un désaccord déontologique ou personnel me conduit à refuser d'agir, ou à démissionner de
ma fonction, je m'engage à transmettre tous les accès et informations en ma possession et nécessaires à mon
remplacement ponctuel ou permanent.
En cas de cessation de mon activité, et quel qu’en soit la cause, je m'engage à remettre l'ensemble des droits et accès,
aux autres opérateur·ices, ou à défaut aux instances légitimes de Deuxfleurs.
