---
title: "Installer Linux"
weight: 10
description: "Installer Linux"
date: 2021-12-30T15:23:52.320Z
dateCreated: 2021-12-30T15:23:50.137Z
extra:
  parent: 'formations/sysadmin.md'
---

On utilise un Live CD (ou Live USB - le support de stockage important peu)  pour lancer un système d'exploitation (celui présent dans le support de stockage) sur un ordinateur, sans toucher aux disques présents dans l'ordinateur.

Utiliser un Live CD, c'est la base pour configurer les disques d'une machine, installer un système d'exploitation (OS) dessus, accéder au disque quand on ne connaît pas le mot de passe administrateur. Bref, c'est la vie !

Ce guide se décompose ainsi : 

* On commence par expliquer comme obtenir une *image disque* ou ISO. Elle contient le système qu'on va mettre sur le Live CD. 
* On explique comment *flasher* l'ISO obtenue sur le support de stockage de notre choix (CD, USB, carte SD, disque dur externe : ce que vous avez sur la main, tant que c'est capable de contenir l'ISO et connectable à l'ordinateur cible). *Flasher*, c'est le verbe classe pour dire « copier l'image disque sur le support de stockage » (avouez que ça claque !)
* On découvre les arcanes du BIOS, ce mini-OS présent sur la carte-mère de l'ordinateur et dont le rôle (entre autres) est de savoir quel système démarrer aujourd'hui. Dans notre cas, on veut démarrer sur le Live CD, ce qu'il faudra donc expliquer à notre ami le BIOS.
* Et ça démarre !

## Obtention d'une image disque

Avant toute chose, il faut choisir quelle *distribution* de Linux on va mettre sur le Live CD. Une distribution Linux, c'est une *variante* de Linux. Parce qu'en fait, Linux en lui-même n'est qu'un *noyau* (*kernel* en anglais), pas tout à fait un OS. Il y a donc [mille et unes distributions de Linux](http://distrowatch.org/?language=FR), qui ont toutes leurs avantages et inconvénients.

Vous connaissez peut-être de nom les distributions Ubuntu, Debian, Fedora, Manjaro...
Nous, chez Deuxfleurs, on utilise la distribution NixOS (parce qu'elle garantit des configurations identiques entre machines, ce qui nous est très utile). On présentera donc comment générer une ISO pour Live CD NixOS. On présentera aussi comment télécharger une ISO de Debian, parce que c'est moins farfelu, très fiable, et que ça fait un bon système de bureau (si d'aventure, vous aviez envie d'utiliser Linux comme OS principal !).

### NixOS

> TODO

### Debian

> TODO

## Installation de l'ISO sur le support de stockage 

> TODO

## Démarrer l'ordinateur sur l'OS du Live CD

> TODO

## Et pour finir, on a un système d'exploitation qui marche !

> TODO (screenshots?)
