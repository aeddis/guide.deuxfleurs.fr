---
title: "Émancip'Asso"
description: "Émancip'Asso, la formation à destination des hébergeurs pour comprendre le milieu associatif"
weight: 32
extra:
  parent: 'formations/conf.md'
---

**Animatrice :** Angie de Framasoft et Anne-Laure d'Animafac  
**Prise de note :** ??  
**Compte-rendu :** Quentin de Deuxfleurs  
**Document source :** [Libreto Camps CHATONS 2022](https://libreto.sans-nuage.fr/camp-chatons-2022/vendredi+19+-+ateliers)  


# Contexte

Des assos aimeraient transitionner vers le libre mais souvent la transition est moyennement heureuse.
L'idée serait de faciliter cette transition en travaillant avcec les CHATONS sur la com, la formation, l'accompagnement, etc.

# Le projet

Le projet en détail (slide 7) : une formation pour les hébergeurs de services alternatifs (en présentiel, faisant l'objet d'une captation vidéo), sur un MOOC. Suivi d'une campagne de communication à destination des bénévoles

Sur le site, un espace qui permettrait de contribuer financièrement au développement de nouvelles fonctionnalités (crowdfunding mutualisation).
Première étape : constitution d'un copil en charge de trouver des sous

Constitution d'un copil composé de têtes de réseaux (CAC, Ceméa, Recia, chambres régionales de l'ESS - cress, e-graine, MFR, Ligue de l'Enseignement, le Mouvement Associatif, Maison des Actions Solidaires, Res Numerica, Oopale - DLA Culture, etc.) et CHATONS (Colibris, Sleto et Zourit).

[↣ Site d'appel aux dons pour Émancip'Asso](https://soutenir.emancipasso.org/fr/)

# Financement de la formation

Dont temps de travail : 38k  
Autres pôles de dépense : intervenant·es 4k, site web 15k, captation 7.5k  
Budget total : 92k (114k avec la valorisation)  
Financements : FPH 40k, (Fondation Credit Coopératif 20k, Afnic 12k, Un monde par tous 18k)  
Actuellement : 31.2k atteint.  

# Le déroulé

Voir le document : propostion de séquençage pédagogique de la formation Emancip'Asso

Du 16 au 20 janvier 2023, sans doute à Paris si possible
 - Compréhension du monde associatif (1/2 j.)
 - Panorama des usages numériques des associations (1/2 j.)
 - Instaurer du dialogue stratégique (1/2 j.)
 - Réaliser le diagnostic numérique d'une asso (1 j.)
 - Formation des utilisateur·rice support / assistance (1/2 j.)
 - Communication et design d'une offre de service (1/2 j.)
 - Développer son réseau et penser le travail en complémentarité (2h)
 - Bilan de la formation (1h30)
 
# Foire Aux Questions

**Coût de la formation :** gratuit pour les participant·es. Hébergement et transports non pris en charge, caisse solidaire.  
**Intervenant·es :** identifié·es / contacté·es  
**L'un des objectifs :** conscientiser les associations de la valeur de l'accompagnement (et du libre)  
**Cibles :** associations "moyennes" (i.e. avec des salarié·es et du budget)  
**Recherche de financement :** permettre aux intervenant·es d'avoir des clefs sur le financement associatif, afin d'accompagner les assos dans la recherche de sous pour mener leur transition au libre  
DLA Numérique : Opale / Mouvement Associatif : chargé des dispositifs Locaux d'Accompagnement sur le Numérique. Idée : CHATONS comme prestataires agréés DLA (jusque 5.000€)  
**Pour qui ?** : les CHATONS de toutes tailles et possiblement des hebergeurs pas membre du collectif dans un second temps  

# Besoins

 - Des sous
 - Une asso concernée sur l'accessibilité
 - Un lieu de formation (propositions : Oasis21 ? CICP ?)

