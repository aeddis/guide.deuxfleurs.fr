---
title: Grand RDV Vénissieux 2023
weight: 130
draft: false
date: 2023-09-30
extra:
  parent: formations/conf.md
---

**Rendre obsolète l'obsolescence numérique !**

# Quand est-ce que le numérique devient obsolète ?

## L'importance de comprendre les usages

Un mémoire par Léa Mosesso

<!--Cette enquête vise à mieux comprendre les problèmes logiciels rencontrés par les utilisateurs et utilisatrices de smartphones, et l’influence que ces problèmes ont sur le renouvellement des terminaux.-->

![Portrait de Théo (dessin), 27 ans, doctorant qui a participé à l'enquête 'vivre avec un smartphone obsolète <](/img/obs-phone.png)
*Les impacts environnementaux du numérique proviennent en grande
partie de la fabrication des terminaux. Il est donc nécessaire de
travailler à réduire la fréquence de leur renouvellement.
Ce mémoire interroge le rôle de l’obsolescence logicielle dans le
renouvellement des smartphones. Les critiques de l’obsolescence
logicielle sont généralement formulées d’un point de vue technique et
se concentrent sur les mises à jour. En étudiant non pas les éléments
techniques mais les usages, l’enquête qualitative présentée ici permet
d’identifier d’autre facteurs logiciels qui participent au sentiment
d’obsolescence. L’enquête fait ressortir de
nombreuses stratégies de prolongement utilisées par les utilisateur·ices
pour continuer à utiliser leur smartphone malgré son obsolescence.
Nos résultats remettent en question la frontière entre logiciel et
matériel et montre qu’il est nécessaire de s’intéresser autant à l’un qu’à
l’autre. Ils permettent aussi d’identifier des leviers pour prolonger la
durée de vie des appareils et ainsi réduire leur impact écologique.*

[↣ Page dédiée sur le site Limites Numériques](https://limitesnumeriques.fr/travaux-productions/obsolescence-logicielle-smartphone)  
[↣ Lire le mémoire (PDF, 100 pages)](https://limitesnumeriques.fr/media/pages/travaux-productions/obsolescence-logicielle-smartphone/analyse/1147b4d341-1677076662/vivre-avec-un-smartphone-obsolete.pdf)  
[↣ Lire l'article de recherche (PDF, 12 pages, Anglais)](https://hal.science/hal-04097867)

<!-- maybe somewhere une référence pour quantifier l'obsolescence -->

# Comment expliquer l'obsolescence ?

## Par une critique de l'économie du jetable

Une interview de Jeanne Guien par Laura Raim

![Miniature de la vidéo Pourquoi a t'on besoin de jeter ? <](/img/jeter.jpg) *Docteure en philosophie, Jeanne Guien travaille sur les questions de l’obsolescence et du consumérisme. Elle raconte l’histoire et l’économie du jetable et invite à s’interroger sur la notion même de déchet. Que mettons-nous dans nos poubelles ? Pourquoi produisons-nous autant de déchets ? Pourquoi a-t-on besoin de jeter ?*

[↣ La vidéo originale sur Arte.tv (24 min)](https://www.arte.tv/fr/videos/108567-011-A/pourquoi-a-t-on-besoin-de-jeter/)  
[↣ Copie de la vidéo sur Youtube (24 min)](https://www.youtube.com/watch?v=QbI_TpP_xn4)  
[↣ Version radio/podcast (24 min)](https://www.arteradio.com/son/61678910/pourquoi_a_t_on_besoin_de_jeter)  
[↣ Livre "Le consumérisme à travers ses objets" de Jeanne Guien (228 pages)](https://www.editionsdivergences.com/livre/le-consumerisme-a-travers-ses-objets)

## Par une critique de l'idéologie technicienne

Une interview de François Jarrige par Laura Raim

![Miniature de la vidéo "Et si on arrêtait le progrès"<](/img/jarrige.jpg)
*François Jarrige critique notamment l’idéologie “technosolutionniste”, selon laquelle l’innovation technologique pourrait résoudre tous les problèmes écologiques, sociaux, culturels et politiques. “L’idée ce n’est pas d’être pour ou contre la technique, c’est d’inventer d’autres systèmes techniques dans d’autres contextes sociaux et démocratiques,” résume-t-il.*

[↣ La vidéo originale sur Arte.tv (22 min)](https://www.arte.tv/fr/videos/103447-011-A/et-si-on-arretait-le-progres/)  
[↣ Copie de la vidéo sur Youtube (22 min)](https://www.youtube.com/watch?v=7T6N0Ohm778)  
[↣ Version radio/podcast (22 min)](https://www.arteradio.com/son/61676979/et_si_arretait_le_progres)  
[↣ Livre "Technocritiques - Du refus des machines à la contestation des technosciences" de François Jarrige (420 pages)](https://www.editionsladecouverte.fr/technocritiques-9782707178237)

## En cultivant d'autres rapports aux technologies

Un receuil de textes par l'association Ritimo

![Couverture du livre sur le low-tech de Ritimo <](/img/lowtech-ritimo.jpg)
*Depuis les années 2000 et la massification des « high tech », le monde a indubitablement changé de visage. Alors qu’elles sont présentées comme facilitant le quotidien, les technologies numériques posent de nouveaux problèmes en termes d’accès aux droits, de justice sociale et d’écologie. Consommation énergétique, extractivisme, asservissement des travailleur·ses du « numérique », censure et surveillance généralisées, inégalités face au numérique… autant de domaines dans lesquels les outils que nous utilisons, individuellement et collectivement, pèsent sur l’organisation des sociétés et sont au cœur de débats de vitale importance.
C’est donc en ce sens que cette publication explore le domaine des low tech (« basses-technologies », c’est-à-dire techniques simples, accessibles et durables) – par opposition aux high tech. En effet, questionner la place des technologies dans la société implique tout d’abord de poser un certain nombre de constats et d’analyses sur les problèmes que posent ces high tech, et qui ne sont pas toujours mis en évidence. Face à cela, comment penser des technologies numériques utiles et appropriables par le plus grand nombre, tout en étant compatibles avec un projet de société soutenable dans un contexte de crise environnementale et climatique qui s’accélère ?
Les technologies conçues et utilisées par les sociétés sont le reflet exact de la complexité de leur organisation interne, de leur mode de prise de décision et de leur relation avec le monde qui les entoure. Se réapproprier collectivement, démocratiquement et le plus largement possible les technologies afin d’en maîtriser les coûts et d’en mutualiser les bénéfices, tel est l’enjeu dans un monde où la crise politique, sociale et écologique se fait de plus en plus pressante.
Ce numéro de la collection Passerelle se veut un espace de réflexion sur les problématiques et les expérimentations d’alternatives autour des technologies numériques.*

[↣ Dossier au format web](https://www.ritimo.org/Low-tech-face-au-tout-numerique-se-reapproprier-les-technologies-8394)  
[↣ Dossier au format PDF (194 pages)](https://www.ritimo.org/IMG/pdf/pass21_low_tech_numeriques.pdf)  
[↣ Acheter le livre (194 pages)](https://www.ritimo.org/Low-tech-face-au-tout-numerique-se-reapproprier-les-technologies-8264)  

# Comment passer à l'action ?

## En prenant la mesure des choix déjà disponibles

[↣ Un guide dogmatique pour choisir votre ordinateur](https://plume.deuxfleurs.fr/~/PiedDeVent/Guide%20dogmatique%20pour%20choisir%20son%20ordinateur) - *Tom Goldoin* - Un guide pour acheter le bon ordinateur d'occasion, en sélectionnant des modèles professionnels très onéreux neufs mais bon marché en occasion, disposant de pièces réparables et ayant une conception de bien meilleure qualité en comparaison du matériel grand public.

[↣ Fairphone](https://www.fairphone.com) et [↣ Framework](https://frame.work) - *Entreprises* - Fairphone et Framework sont, respectivement, constructeurs de téléphone et d'ordinateur portable. Ils produisent des appareils facilement démontables (en quelques minutes, souvent sans outils) et s'engagent à fournir des pièces détachées pendant un certain temps. Fairphone prend également des engagements sociaux et environnementaux.

[↣ L'atelier soudé](https://atelier-soude.fr/) - *Association* - L’Atelier Soudé d'accompagnement à la réparation en autonomie. En vous rendant à une de ses permanences, les bénévoles de l'atelier soudé réparetont avec vous vos appareils électroniques défectueux (ordinateur, téléphone, télévision, enceinte, etc.).


[↣ Linux & Populus](https://eisenia.org/linuxpopulus/) et [↣ Lyon Association Libre Informatique Solidaire](https://lalis.fr/) - *Associations* - Ces associations reconditionnent d'anciens ordinateurs, sur le plan matériel comme logiciel, pour leur donner une seconde vie et les mettre à disposition à un prix solidaire tout en assurant un accompagnement technique à ses utilisateurs (remplacement de pièces, résolution de bugs logiciels, etc.).


[↣ Commown](https://commown.coop/) - *Coopérative* - Une coopérative qui propose des téléphones et ordinateurs en location, et qui se charge de la réparation en cas de défaut. En gérant de multiples exemplaires d'un même modèle de téléphone ou d'ordinateur, la coopérative peut assembler un exemplaire fonctionnel à partir de deux exemplaires (ou plus) défectueux.

[↣ Deuxfleurs](https://deuxfleurs.fr) - *Association* - Nos ordinateurs et téléphones fonctionnent en réseau : même pleinement fonctionnels, ils ne sont pas utiles si ils ne peuvent pas communiquer avec le reste du monde. Deuxfleurs fournit des services (email, site web, visio, etc.) avec le soucis permanent de la compatibilité avec le matériel plus ancien.

## En changeant les règles

[↣ Plaidoyer pour le droit à la réparation](https://fr.ifixit.com/Right-to-Repair) - *iFixit* - Nous devons ancrer notre droit à réparer ce qui nous appartient. L'accès aux pièces, aux outils et aux informations de réparation doit être équitable et abordable. [L'indice de réparabilité Français](https://fr.ifixit.com/News/64782/indice-de-reparabilite-deja-1-an-mais-cest-quoi) a été mis en place en s'inspirant des travaux de iFixit.

[↣ Plaidoyer pour l'intéropérabilité](https://www.laquadrature.net/2019/06/13/cest-quoi-linteroperabilite-et-pourquoi-est-ce-beau-et-bien/) - *La Quadrature du Net* - Une fois nos téléphones et ordinateurs réparés, il reste encore la question de les faire communiquer avec les services et le reste du monde qui évoluent en permanence. Imposer l'intéropérabilité, c'est s'assurer que les nouveaux services puissent communiquer avec les anciens, au moins en mode dégradé, grâce à des standards établis mondialement. Deux exemples d'intéropérabilité qui ont bien fonctionné : le web et les emails. 


## En faisant des choix technologiques l'affaire de toutes et tous

[↣ Technologie partout, démocratie nulle part](https://boutique.fypeditions.com/products/echnologies-partout-democratie-nulle-part-plaidoyer-pour-que-les-choix-technologiques-deviennent-l-affaire-de-tous) - *Irénée Régnauld, Yaël Benayoun* - L'urgence climatique, l’ubérisation, l’économie des petits boulots, les smart cities et la surveillance algorithmique nous ont brutalement fait prendre conscience des répercussions dramatiques des technologies. Alors que le progrès était censé servir le bien commun, il nous échappe. Nous le subissons. Malgré cela, la réponse apportée à tous les problèmes économiques et sociaux se borne à des solutions purement techniques. 
Irénée Régnauld et Yaël Benayoun révèlent et dénoncent les dogmes et les manœuvres qui permettent aux industries et aux pouvoirs publics de maintenir les citoyens et les travailleurs à l’écart des choix technologiques, en excluant tout processus démocratique. Ils montrent que notre arsenal juridique et nos institutions apeurées, voire serviles, sont incapables de contrer les servitudes imposées par les plateformes et les industries hyper capitalistes. 
Pour sortir de cette confiscation du progrès, les auteurs proposent des actions concrètes et réalistes qui replacent le débat démocratique et les revendications citoyennes au cœur du développement technologique, afin que la question du progrès devienne l’affaire de tous.


