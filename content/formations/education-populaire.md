---
title: "Éducation populaire"
description: "Éducation populaire chez les CHATONS"
weight: 30
extra:
  parent: 'formations/conf.md'
---

**Animateurs :** Quentin de Deuxfleurs  
**Prise de notes :** røseløve·de Attac et neil de 42l/La Contre Voie  
**Compte-rendu :** Quentin de Deuxfleurs  
**Document source :** [Libreto camps CHATONS 2022](https://libreto.sans-nuage.fr/camp-chatons-2022/samedi+20+-+ateliers)

Se réunir pour parler d'éducation car ce terme appraît sur le site Chatons.org, il est revendiqué mais pourtant il n'est pas perçu de la même manière par tou·te·s ! Comment se l'approprier ?

# Définir l'éducation populaire

Méthodologie GFEN : « Dans le mouvement on apprend pour SOI et AVEC les autres » , « on appelle cela l'auto-socio-constructivisme ! »

Différence avec la vulgarisation : avant tout répondre à un besoin, une envie. Le savoir vient en complément de ce besoin/envie, il n'est pas la finalité, simplement un moyen.

# Les limites de l'éducation populaire au sein des CHATONS

**Pas légitime** - Parfois, les personnes ne se sentent pas légitimes car n'ont pas les savoirs, échanger entre tekoss et non-sachant·e·s

**Pas notre métier** - Notre corps de métier est la technique et non l'éducation populaire : manque d'expérience/connaissance/assurance sur la question ?!

**Prend du temps, couteux** - 
On a trouvé le FDVA (Fonds pour le Développement de la Vie Associative) qui finance les formations de bénévoles. À titre d'exemple : 2 x 2500 € pour deux formations !
La subvention est apportée à Marsnet directement, et les formations sont proposé aux bénévoles de marsnet et au bénévoles des associations membres (180) ou sympathisantes

Au sujet des financements, à la contre-voie on aimerait avoir une équipe salariée pour intervenir dans des établisssements scolaires (lycées collège), pour sensibilier autour du libre ! Dans les milieux militants tout le monde sait pour les GAFAMS, mais dans le « grand public » pas trop !

Du coup FONJEP (agrément jeunesse et éduc' pop en tant qu'asso 15k€~ / an ) et autres FDVA pourraient vraiment être très intéressants ! L'agrément jeunesse et éduc' pop donne le droit d'aller dans les institutions de l'éducation nationale.

**Pas pour tout le monde** - Si quelqu'un·e ne voit pas le lien entre éduc' pop' et Chatons, c'est que c'est révélateur ! Tous les chatons ne font pas tous de l'éduc' pop', mais tout simplement que de l'hébergement ! Framasoft est assurément promoteur de l'éducation populaire, ce n'est pas le cas de tous les Chatons malgré que cela soit super important, alors comment insuffler la joie de l'éducation populaure aux autres Chatons, il y a une prise de conscience


**Ne pas s'arrêter à la documentation** - 
« Il faut sortir de du RTFM, il faut accompagner continuellement ! » « S'il suffisait d'avoir le manuel pour faire un truc ça ferait longtemps que tout le monde serait émancipé » « j'ai proposé à une membre de ma famille d'installer un téléphone libre, son téléphone /e/ ne fonctionnait par exemple pas la data, or je n'étais pas là pour aider donc sont point de vue était eronné vis-à-vis du logiciel lire »

**Attention à la pureté militante** - 
Le boulot fourni par le collectif Hacking Social et Horizon Gull dans leur guide sur l'autoritarisme et les moyens de manipulations est un outil émancipateur super intéressant (exemple vis-à-vis des thématiques de l'extrême droite déboulonnées). Un problème récurrent est aussi la « pureté militante», on peut pas être parfait tout de suite, côté sécu par exemple il faut élaborer son modèle de menace, la pureté militante est à mettre dans nos modèles de menace !


# À qui s'adresser ?

**Les contre-cultures et sous-cultures** - Sortir de l'entre-soi est éminament important ! Sortir de notre zone de confort et aller dans les environnements de contre~sous~cultures pour questionner autour de nos sujets. 

**Trouver un relai dans les assos** - « Faut pas essayer de convaincre le groupe ou l'asso, faut convaincre une ou plusieurs personnes à l'intérieur ». parfois, même pour les convaincu·e·s, il faudra de fait avoir beaucoup de temps à disposition


# Faire émerger le besoin

**Générer de la curiosité** - "En atelier HTML/CSS, parfois je fais une manipulation que je n'explique pas, mais en voyant le résultat, les personnes présentes ont envie de savoir comment faire."

**La sauvegarde des données** - Pour les services libres : la sauvagarde, ça parle beaucoup aux gens (perte de données,…) donc ça peut être une porte d’entrée.

**La vie privée** - Exodus-privacy a des ateliers « Café Vie-Privée » très utiles pour faire parler les gens autours de leurs pratiques.

# Prendre du recul

**Rester positif** - Il nous faut garder une posture horizontale, ne pas prendre les gens de haut, présenter des « alternatives cool », créer la curiosité pour ne pas être dans le côté « badant et lourd ».


**Repolitiser** - jouer sur la question politique : conférence gesticulée sur les questions politiques. J’envoie des infos à des membres de mon collectif concernant ce que font les GAFAM pour les sensibiliser, donc qu’est-ce qu’on fait chez Facebook ? Il faut donner envie d’en sortir.

**Ne pas tomber dans la comparaison**  - 
Arrêter de chercher des alternatives permet aussi de remettre en perspective l'ensemble des valeurs et l'éthique qu'on souhaite proposer. Requestionner les outils proprios met en valeur le coût environnemental par exemple, notamment les ressources. Si c'est des vidéos : alors proposer une application de podcasts ! (il faut) « Se méfier des comparaisons, de vouloir faire pareil en libre »

**Avoir une "démarche"** Il faut construire une démarche pour travailler cette bascule. Et cette démarche doit venir des personnes qui en ont besoin.
Recommandation de lecture [« L’éloge du carburateur »](https://www.editionsladecouverte.fr/eloge_du_carburateur-9782707160065)



# Partager nos expériences

**Du fun & des résultats immédiats** - Exemple d'un atelier HTML monté via un serveur local, les gens voient tout de suite le résultat, ce genre d'atelier en physique d'une journée permet également entre les temps HTML de parler logiciels libres et politique. [Référence : les pages membres de club1.fr](https://club1.fr/membres/).

**Mettre la technique de côté** - Les ateliers d'écriture, la cartographie, de nombreux formats d'ateliers centrés sur d'autres domaines que la technique permettent de parler des usages & besoins du numérique. Par Des formats d'ateliers vidéo sur Krita (logiciel de dessin) par exemple.

**Faire ensemble** - 
« Installer un Linux, c'est faire un effort ! On fait des installations depuis zéro, BIOS clés USB jusqu'aux installations de logiciels, cette démarche fonctionne car les personnes ne rappellent pas »

**Dégafamisation au collège** - 
Dans un GUL, une enseignante a invité des élèves dans un éco-lieu-collège, un Chatons a monté un atelier Dégafamisation, des exemples ont été pris depuis Nextcloud, grâce à l'aide du CHATONS (Pâquerette)

**Les formations de MarsNet** - Les deux formations qui marchent le mieux sont site web et outils collaboratifs.

**De Youtube à un Android libéré** - NewPipe est un pont d'entrée efficace pour proposer aux personnes utilisant YouTube, souvent F-Droid est installé en même temps et c'est une chouette porte d'entrée pour trouver d'autres logiciels libres. (NDLR : ça nous rappelle aussi que le smartphone est devenu hégémonique aujourd'hui mais qu'il est parfois délaissé dans notre action car casse-pied tellement il est fermé).

**Physicalliser** - En tant que travailleur dans l'audiovisiuel je vois la difficulté de la technique, côté Chatons, la communauté est forte et des échanges types ateliers ou même apéros est vraiment un moyen d'identification fort pour échanger, pourquoi pas des permanences publiques à un endroit physique ? Cela amoindrit la difficulté à trouver des informations techniques car les humains Chatons peuvent répondre en direct et être un super répère ! 

Marsnet fait des apéro-Chatons comme pour les GUL, « c'est super important d'avoir des rendez-vous mensuels car ça ressemble les gens de l'association, de présenter ce qu'on fait, ce que c'est les Chatons »

**Varier les médias** - Le support de la vidéo / diapo / présentation est super pour aider, par exemple à mettre sur le Wiki des Chatons, avec des fiches/howto et divers slides

# Aller plus loin en commun

**Partager les ateliers** - Dés qu'un Chaton a trouvé une idée d'atelier, des formulations, il pourrait être intéressant de partager cela entre nous ? Ateliers référencés à ce jour :
  - Atelier création site web chez club1.fr
  - [Network & Magic](https://code.ffdn.org/ljf/networkandmagic) de LJF -> peut-être problématiser l'atelier ? Sur la collecte de donnée, ou sur les acteurs de l'internet, etc.
  - La rivière du doute (cf atelier par neil de la contre voie)  
  - Atelier sobriété numérique, telecoop tourne là dessus

**Faire une rencontre Chatons.org ET mouvement d'éducation nouvelle**  - 
(GFEN/ICEM/Francas/CEMEA)


**Conférence gesticulée, la "démarche"** - Est-ce qu'une conférence gesticulée ne serait-elle pas intéressante ? Connaissez-vous le principe de la « démarche » en éducation populaire ? « ce qu'on fait avec un public, on le vit soi-même », « cela transfome les gens ! »

Une chouette ressource à voir : Conférence gesticulée : [« informatique ou liberté »](https://tube.conferences-gesticulees.net/w/60c6eca7-6328-4261-8fdd-5e561dd32ab9) par Lunar
