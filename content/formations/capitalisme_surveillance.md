---
title: "Atelier capitalisme de surveillance"
description: "Atelier capitalisme de surveillance"
weight: 40
extra:
  parent: 'formations/conf.md'
---

*Ces notes ont été écrite en préparation d'un atelier réalisé par Deuxfleurs lors d'une journée sur la sobriété numérique
organisée par Attac dans la région de Tarare le 18 juin 2022.*

# But de l'intervention

*Prévoir 10 minutes pour cette partie*

but de l'atelier = interroger la notion de sobriété numérique sous l'angle de la critique du capitalisme de surveillance.

quasi tout le monde parle d'écologie aujourd'hui, de la gauche à la droite, du militant XR à l'entreprise du CAC 40,
mais parfois sans préciser les valeurs ou l'idéologie sous-jacente - car il y en a toujours, y compris dans le "plus scientifique des discours".

 - les petits gestes pour sauver la planète, on reconnait la sur responsabilisation individuelle du néo libéralisme.
 - le retour à la terre, un peuple = une terre, etc. eco-fasciseme extreme droite.

prisme émancipation des individus / donc sociale / donc gauche.
opposition écologie <-> capitalisme = pas une approche nouvelle.  
par exemple : Hervé Kempf = co-fondateur site web Reporterre, "quotidien de l'écologie", ex. journaliste au Monde.  
son dernier livre = que crève le capitalisme.  

De manière plus large opposition :
  - capitalisme prone "accumulation" / "croissance infinie"
  - écologie prone "limitations physique" / "préservation vivant"  

capitalisme -> régime régulièrement en crise -> à chaque fois "réinvention" / "mutation".  
dernière mutation -> "capitalisme surveillance" = les mots de Shoshana Zuboff (universitaire americaine contempo).

Zuboff base sa définition sur celle du capitalisme tradi de Karl Polanyi (historien 20e siecle) càd 3 fictions :
 - vie humaine -> main d'oeuvre
 - nature -> immobilier
 - échanges -> monnaie

vie+nature+échange transformés en des choses qui peuvent être vendues+achetés de manière profitable.
Pour le "capitalisme de surveillance", Zuboff ajoute une 4ème fiction :
 - réalité -- (marchandisé/monétisée) -> "comportenent"

influence choix+comportement = nouvelle marchandise =  vente+achat manière profitable, marché

Comment s'organise le marché ? en 3 étapes :
 - "extraction" - collecter les données, aka pisteurs / tracking
 - "analyse" - traiter, croiser les données, aka data brokers
 - "influence" - exploiter ces données pour modifier des comportements

*Exemple en annexe si besoin*

Et les impacts écologiques équivalents :
 - obsolescence perçue - mon ordinateur/telephone est ralenti par cette collecte d'info - les 50 sites les + consultés aux USA mettent en moyenne 10 secondes à charger sur l'ordinateur des gens. Une fois les trackers supprimés, ils ne prennent plus que 3 secondes. lemonde.fr -> 5 sec avec tracker, 1.3 sec sans sur mon PC.
 - gaspillage - des milliers d'ordinateurs sont fabriqués et utilisés pour traiter ces infos avec des algos d'apprentissages, pas facile de trouver des chiffres fiables = secret industriel. mais articles scientifiques s'alarmant du cout ecologique de "l'ad tech" - càd ces boites qui collectent et traitent les données.
 - capacité à agir - nos infos donnent du pouvoir aux entreprises pour brouiller l'information qui permettrait une action+changement comportement (eg. Personne est à l'abri de l'effet de la pub, nos inquiétudes/luttes exploitées par entreprises -> eg. Total dit les emails polluent ou l'industrie du tabac paye scientifique semer confusion, cf Edward Bernays).

Une partie de ce capitalisme de surveillance se déroule en "contrepartie" de nos usages du numérique et où l'on peut agir dessus meme individuellement.
D'autres ont simplement lieu quoi qu'il arrive (camera videosurveillance, capteurs en tout genre, operateur mobile qui tracke quelle antenne on borne, etc.).
Individuellement, bcp + dur d'agir

**conclusion**  
c'est un choix de quantifier nos usages du numérique
sans découpler l'usage réel, du "surplus", qu'est le pistage et la pub.
En faisant le choix de traiter ce "surplus" de manière indépendante,
on peut le bloquer, améliorer notre qualité de vie, et in fine aussi agir pour la sobriété numérique,
en réduisant le besoin de renouvellement de nos équipements.
Le but de l'atelier c'est de voir ce qu'on peut bloquer à l'échelle individuelle (parfois en 3 clics)
et discuter de ce qu'on doit refuser à l'échelle collective.

# Les objectifs

 1. Comprendre le tracking/pub sur le web, repartir avec un bloqueur
 2. Identifier les apps Android qui trackent/pub, identifier des alternatives
 3. Comprendre l'importance des métadonnées de communication

# Les étapes

Cet atelier est en 3 étapes.
La plus importante et simple est l'étape 1.
Les étapes suivantes sont plus complexes et facultatives.

## Sur le web

![><](../bloque.png)

Présenter le site web bloquelapub.net
 - Préciser que ça bloque la pub ET le pistage
 - Qui est LQDN - laquadrature.net/
 - Attention aux bloqueurs de pub qui laissent passer la pub et/ou vous pistent !
   - [L'exemple AdBlock](https://www.01net.com/actualites/adblock-plus-les-liaisons-dangereuses-du-bloqueur-de-pub-647527.html)
   - [L'exemple Ghostery](https://antipub.org/alerte-ghostery-est-un-infiltre/) 

<center>~</center>

![><](../uBlock.png)

Installer le bloqueur de pub et de pisteurs sur PC :
 - Sur Firefox - https://addons.mozilla.org/fr/firefox/addon/ublock-origin/
 - sur Chrome - https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=fr

<center>~</center>

![><](../firefox.png)

Sur Android, seul Firefox supporte Ublock Origin. 
Si vous avez un téléphone suffisament récent avec de la place (~100 Mo), je vous recommande de l'installer :  

 - Depuis le Play Store : https://play.google.com/store/apps/details?id=org.mozilla.firefox  
 - Depuis F-Droid : https://f-droid.org/fr/packages/org.mozilla.fennec_fdroid/  

Ensuite cliquer sur les 3 points verticaux, puis Modules complémentaires, et chercher "Ublock Origin".  
Cliquez sur le + pour l'installer, et voilà c'est tout !

<center>~</center>

Avant/Après installation du bloqueur :

![><](../progres.png)
![><](../konbi.png)

Faire des essais en activant/désactivant le bloqueur de pub (ublock origin + protection pistage intégrée de Firefox).
Sur les sites :
 - meteofrance.com
 - marmiton.org
 - elle.fr
 - lemonde.fr
 - leprogres.fr
 - konbini.com


<center>~</center>

Se mettre sur un seul ordinateur, et s'approprier l'action du bloqueur de pub avec Lightbeam. Survoler les points
et prendre le temps d'identifier l'entreprise qui est derrière (exemple : xiti). 

![><](../lightbeam.png)

Attention au CNAME cloacking.  
Exemple : `buf.lemonde.fr` -> `dig +short buf.lemonde.fr` -> `buf-lemonde-fr-cddc.at-o.net.` -> Xiti

Notez que les services des CHATONS ne présentent pas cette même "galaxie" de pisteurs/pubs.

## Dans les apps

![><](../marmi.png)

 1. Demander à ce que chaque personne identifie 4/5 apps importantes pour elle.
 2. Aller sur le site web : https://reports.exodus-privacy.eu.org/fr/
 3. Chercher les applications et faire un top des pires apps

Ensuite :

  1. Identifier des applications alternatives sur https://f-droid.org/
  2. Installer F-Droid sur le smartphone
  3. Installer les apps identifiées.

<center>~</center>

Pour les apps dont on ne peut pas se passer, il est possible de bloquer un peu la pub/trackers :

![><](../adaway-logo.png)

Installation de adaway https://adaway.org/

![><](../adaway.png)



## Sur le réseau

Lancer le point d'accès WiFi, demander aux gens de s'y connecter.
Faire une capture Wireshark, filtrer par TLS par exemple : `tls.handshake.type == 1`
puis activer la résolution des noms (Vue -> Name resolution -> Network).
Aller dans statistiques, puis Endpoints, puis IPv4 - ou IPv6, activer la résolution des noms, classer par paquets.

![](../wireshark.jpg)

Prendre le temps de disséquer une capture :
  - Identifier les services contactés par le téléphone (par exemple ce téléphone Huawei communique régulièrement avec les serveurs de Huawei)
  - Montrer que le téléphone communique sur le réseau meme en veille (attention si il n'a plus beaucoup de batterie il est bcp plus silencieux)

Identifier 2 problèmes :
  - Mon téléphone me piste alors que je ne veux pas du tout -> Lineage, Murena, etc.
  - Je ne veux pas que n'importe qui sache que je contacte Doctissimo -> Tor


<center>~</center>

Parler de Tor :
 - Tor Browser : https://www.torproject.org/download/
 - Orbot : https://orbot.app/download

# Préparer l'atelier

Pour que l'atelier se déroule bien, il faut préparer votre machine

## Installer Lightbeam WE

Ref : https://github.com/mozilla/lightbeam-we

```
git clone --recursive https://github.com/mozilla/lightbeam-we.git
npm install
```

Puis dans Firefox :
  1. Taper `about:debugging` dans la barre d'URL
  2. Choisir "Ce Firefox"
  3. Aller dans le dossier `lightbeam-we/src` et choisir `manifest.json`
  4. Cliquer sur le bouton lightbeam

## Le hotspot

Connecter le tel Android, le mettre en partage de connexion.
Ensuite on utilise NetworkManager qui fait la magie pour nous.

Lancer un hotspot :

```
nmcli dev wifi hotspot
nmcli dev wifi show-password
```

Revenir sur la connexion wifi :

```
nmcli con
nmcli con up "Freebox Lyon"
```

## Wireshark

Pour lancer Wireshark en root sous Waylad, il faut xhost :

```
xhost +local:
sudo wireshark
```

Pour filtrer sur les requetes DNS :

```
dns
```


# Sources 

 - https://www.cairn.info/manuel-indocile-de-sciences-sociales--9782348045691-page-49.htm
 - https://journals.sagepub.com/doi/10.1057/jit.2015.5
 - https://www.pingdom.com/blog/trackers-impact-performance/
 - https://www.theguardian.com/commentisfree/2021/oct/11/advertising-industry-fuelling-climate-disaster-consumption
 - https://www.monde-diplomatique.fr/2020/03/BROCA/61553
 - https://www.sciencedirect.com/science/article/pii/S0195925517303505
 - https://www.radiofrance.fr/franceinter/ces-7-fausses-idees-qui-nuisent-a-la-prise-de-conscience-ecologique-9106792 
 - https://www.cairn.info/revue-du-crieur-2017-3-page-44.htm
 - https://lvsl.fr/limperatif-neoliberal-de-ladaptation-par-barbara-stiegler/

# Annexes

Focalise sur nos usages numérique :
 - Naviguer site web/lancer app smartphone. Les pages que je consulte, mes clics, mes recherches, le temps que je passe, etc. sont collectés
 - Ces données sont croisées avec d'autres données sur moi et/ou sur des groupes similaires pour connaitre mes préférences, mes intentions d'achat, inférer des données perso (genre ? tranche age ? categorie socio pro ? suis-je enceinte ? vais-je divorcer ? etc.)
 - Ces données vont être utilisées pour m'afficher des pubs, y compris m'induisant en erreur.

--> cas : Recherche avortement -> données récupérées par asso anti avortement -> signalement police. Contexte droit avortement remis en question.
les bouts de réalité = mes questionnements pour m'identifier afin de prévenir toute action de ma part, par la force ou la persuasion.

--> cas 1 : Vulgarisateur "Un Créatif" montre facile monter une arnaque "vente montre" facile acheter pubs ciblant les personnes susceptibles d'y croire.
les bouts de réalité = age, csp, etc. pour créer une fausse proximité afin de déclencher une vente

--> cas 3 : Cambridge Analytica -> ciblage des indécis politique avec fake news specifique -> impossible à debunk car vu par personne d'autre.
les bouts de réalité = mes peurs, mes craintes pour associer le candidat à leur prise en charge afin de déclencher un vote (meme si pr 2 personnes = 2 mesures contradictoires)

Les fournisseurs de communication (fournisseur access internet, operateurs mobiles, etc.):
  - Téléphone portable utilisant le réseau Orange -> votre géolocalisation collectée en permanence
  - Aggregée avec les données des 27 millions autres clients d'Orange pour créer une carte nationale à la maille fine de fréquentation des lieux, au cours du temps.
  - Données revendues aux commerçants / grand groupes (aka Flux Vision) pour se retrouver toujours plus dans notre passage -> comportement de plus en plus prédateur.
-> Permet par exemple à McDonald's ou Starbucks d'identifier discrètement où placer leurs commerces.
-> Permet de voir une manifestation se déplacer. Les mêmes données utilisées pendant le COVID (en France, pour documenter les parisiens fuyant la capitale, en Suisse pour sanctionner les regroupements).
-> Pas une problématique individuelle mais collective, car données du groupe
source : https://www.orange-business.com/fr/produits/flux-vision
--> beaucoup plus compliqué ici d'agir



