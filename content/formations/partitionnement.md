---
title: "Partitionnement"
description: "Redondance, partitionnement, chiffrement : comment configurer ses disques durs en vue d'une installation de serveur"
weight: 20
date: 2022-04-02T13:38:01.527Z
dateCreated: 2021-12-30T11:39:09.554Z
extra:
  parent: 'formations/sysadmin.md'
---

# Configurer les disques de son futur serveur

On part du principe que vous disposez d'un ordinateur, chez vous ou dans un centre de données, et vous êtes prêt⋅e à annihiler le contenu de ses disques pour en faire un beau serveur tout propre. (Il est tout à fait faisable de garder des données existantes sur les disques, mais c'est hors du cadre de cet article.)

On commencera par expliquer comment accéder aux disques pour les configurer, avant de traiter de partitionnement et de redondance (le fait d'écrire la même choses sur plusieurs disques, au cas où l'un d'entre eux casse). Un autre se focalise sur [le chiffrement des disques](@/formations/chiffrement_froid.md), dont le but est que leur contenu soit incompréhensible sans la clé (si vous vous faites voler le disque, ou que votre hébergeur est trop curieux).

## Préliminaire : accéder aux disques

Pour configurer les disques, il faut y avoir accès, comme s'ils étaient des disques durs externes. Donc, pas depuis le système d'exploitation (ou OS pour *operating system*) déjà installé sur la machine : lui, il utilise activement les disques. Mais depuis *un autre OS*. Qui sera Linux, puisqu'il dispose, d'office, de tous les outils pour ce faire, en ligne de commande.
Deux possibilités :

* _Vous disposez d'un accès physique à la machine_, et pouvez donc brancher écran, clavier, et insérer un live CD/USB Linux qui vous permettra de lancer Linux *depuis le live CD/USB*. 
   Si vous avez déjà installé un OS sur une machine, vous savez de quoi je parle. Sinon, référez-vous au [guide d'utilisation d'un Live CD](@/formations/installer_linux.md).
   
* _Vous n'avez pas accès physiquement à la machine_ (par exemple : elle est dans un centre de données). Auquel cas, le propriétaire de la machine doit avoir prévu le coup : vous devriez pouvoir vous connecter en SSH (ligne de commande à distance) à un *mode rescue* qui vous permettra d'agir sur les disques de la machine alors qu'elle est éteinte. Comment s'y prendre dépend de l'infrastructure logicielle du propriétaire du centre de données.

À partir de maintenant, on considère que vous avez accès aux disques que vous souhaitez configurer *via* la ligne de commande en mode administrateur. Pour vous en assurer, vous pouvez lancer la commande `fdisk -l` (en tant qu'administrateur) ou `lsblk` :

```bash
# fdisk -l
[...]
Disk /dev/sda: 1.8 TiB, 2000398934016 bytes, 3907029168 sectors
[...]
Disk /dev/sdb: 1.8 TiB, 2000398934016 bytes, 3907029168 sectors
[...]
Disk /dev/sdc: 1.8 TiB, 2000398934016 bytes, 3907029168 sectors
[...]
# lsblk
NAME MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sdb    8:16   0  1.8T  0 disk
sdc    8:32   1  1.8T  0 disk
sda    8:0    0  1.8T  0 disk
```

Comme vous pouvez le voir, je m'apprête à installer mon serveur sur une belle machine avec 3 disques de 2 To (= 1.8 Tio). Miam !

## Préparer son partitionnement

Partitionner un disque, c'est le découper en sous-volumes logiques, appelés partitions. 

Un exemple classique de partitionnement, c'est de découper son disque en deux pour disposer *et* de Linux *et* de Windows sur son ordinateur de bureau. Chaque système d'exploitation est installé sur sa partition - et c'est au démarrage qu'on décide si on lance Linux ou Windows aujourd'hui. 

Nous, on s'apprète à installer un serveur sous Linux. On ne cherche pas à faire vivre plusieurs OS sur un seul disque, mais plutôt à séparer les données d'un OS dans des partitions distinctes. On va donc se payer le luxe d'expliquer d'abord quelles stockées vont dans quels dossiers sous Linux, puis on parlera de redondance, avant de revenir sur les contraintes du partitionnement. On passera ensuite à [la pratique](#partitionnement-en-pratique).


### Comment sont organisées les données sur Linux

Comprenez d'abord qu'on « montera » un dossier du système Linux dans chaque partition de stockage. Listons donc les dossiers à considérer lors du partitionnement :

* `/`, c'est le dossier qui contient tout le système. 
  On peut mettre tout le système dans la même partition, mais c'est sous-optimal : imaginons que vous vouliez mettre à jour ou changer votre OS. Le plus simple serait de ne supprimer que les données système en gardant par ailleurs toutes les données utilisateur. Mais avec une seule partition contenant tout `/`, je vous souhaite bon courage pour démêler et transférer toutes ces données utilisateur !
* `/var` contient beaucoup trop de trucs qui ont tendance à manger tout l'espace disque :
  * **Les données utilisateur** : la plupart des services (bases de données, Docker, etc.) stockent par défaut tout leur état dans `/var`. On préfèrera configurer nos services pour qu'ils stockent leurs données ailleurs (voir `/data`) pour les démêler des données moins importantes du système.
    
  * **Les journaux d'accès**, stockés par défaut dans `/var/log`. Ce dossier peut croître soudainement de plusieurs Go/jour en cas d'attaque ([DDoS](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service), notamment).
  
    Ces **méta-données**, essence du [capitalisme de surveillance](https://www.cairn.info/revue-esprit-2019-5-page-63.htm), décrivent précisément l'activité de vos utilisateurs. Elles sont donc presque aussi précieuses que les données à proprement parler. 
    
    On automatise généralement leur suppression périodique ([`logrotate`](https://doc.ubuntu-fr.org/logrotate)), après une **durée de conservation** à définir en fonction de la loi ([6-12 mois en France](https://doc.ubuntu-fr.org/logrotate)) et de vos besoins de journalisation.
    
* `/data` (ou `/srv` ou autre) : c'est un dossier de notre invention (il n'existe pas par défaut dans Linux) dans lequel on mettra toutes nos **données utilisateur** en configurant les logiciels clients (bases de données, Docker etc.) pour qu'ils stockent leur état dedans.

  Ce sont les données **les plus précieuses** de votre serveur, et elles sont sous votre responsabilité : il vous revient d'en assurer la **permanence** (redondance, archivage) et la  **[sécurité](https://fr.wikipedia.org/wiki/S%C3%A9curit%C3%A9_des_syst%C3%A8mes_d%27information)** (chiffrement).
    
  Le fait de caler toutes nos données précieuses dans un seul disque facilite la sauvegarde ultérieure.
    
* `/home` contient les données des opérateurs de la machine. Il peut mériter sa propre partition pour que ces données survivent à une réinstallation.
* Et il y en a encore ! Tapez « partitionnement serveur Linux » dans un moteur de recherche, et vous verrez de vives discussions de tout âge justifier tout et son contraire ([exemple intéressant](https://fr.hotelarbredegel.com/895817-what-is-the-correct-way-TSKMON)).

À part cela, vous aurez sans doute besoin des partitions suivantes :

* Une partition **BIOS/EFI**, qui sert à expliquer à votre BIOS/EFI (le programme qui démarre votre ordinateur) où se situent vos systèmes d'exploitation. Nécessaire pour que le système démarre, donc. 
  Elle a besoin d'être sur le disque dur principal, et son format (BIOS ou EFI) dépend de l'âge de l'ordinateur (EFI est plus récent). La taille qu'elle doit faire dépend de son format, mais ça ne dépassera jamais 600 Mo.
  Si vous non plus, vous n'y comprenez rien, j'ai trouvé [ce guide](https://www.easeus.fr/partition-manager-tips/difference-entre-bios-et-uefi.html) très clair et complet.
* `/boot` contient les données d'amorçage du système, et pèse le plus souvent moins de 200 Mo.
  On a besoin de mettre ce dossier dans sa propre partition quand on fait du [Chiffrement à froid](@/formations/chiffrement_froid.md) sur la partition système (`/`), par exemple. 
* Une partition `swap`, qui sert de **RAM de secours super lente**. 
  Très utile si l'on n'a pas beaucoup de RAM, parce qu'elle évite que le serveur ne s'éteigne brutalement dès la limite atteinte. En lieu et place, l'ordinateur fonctionne ô-combien-lentement, mais vous laisse quand même la possibilité de tuer cette saleté de processus qui a mangé toute la RAM, sans avoir à redémarrer.
  
  * **Si vous avez moins de 4 Go de RAM**, c'est un choix prudent que de mettre une partition *swap* de quelques Go. Si vous comptez chiffrer votre disque, assurez-vous de chiffrer la *swap* ! Beaucoup d'informations personnelles pourraient y transiter.
  * **Si vous pensez avoir suffisamment de RAM pour ne jamais la remplir**, vous avez moins besoin de *swap*. Néanmoins, le noyau et certains logiciels aiment bien en avoir sous le coude. Donc, si la taille de votre disque n'est pas un problème, n'hésitez pas !

Vous l'aurez compris, il n'y a pas partitionnement parfait, et **c'est à vous de décider de votre schéma de partitionnement**. 

**En résumé :**
* Les données les plus précieuses sur un serveur, ce sont les données utilisateur dont vous aurez la responsabilité. 
* Les données système & de configuration sont moins importantes parce que (1) un système ça se réinstalle, et (2) vous devriez prendre l'habitude de faire des copies de toute votre configuration.
* Séparer données système & utilisateur, on se remercie de l'avoir fait le jour où on veut réinstaller le système.
* La *swap* c'est important quand on a pas beaucoup de RAM.

### Redondance des données

Parlons maintenant des stratégies qu'on peut mettre en œuvre, quand on dispose de plusieurs disques, pour diminuer le risque de perte de données grâce à la redondance (c'est à dire : le fait de copier une même information à plusieurs endroits à la fois).
**Si vous ne disposez que d'un disque sur votre ordinateur, n'êtes pas concerné⋅e. Vous pouvez passer directement au [partitionnement](#partitionnement-en-pratique).**

La redondance (entre autres) est permise par la technologie [RAID](https://fr.wikipedia.org/wiki/RAID_%28informatique%29) (pour « *Redundant Array of Independent Disks* » ou « ensemble redondant de disques indépendants »). Notez que RAID crée des volumes « logiques » à partir de partitions physiques (des bouts de disques, pas les disques entiers). RAID propose plusieurs architectures, en fonction de ses besoins ; en voici quelques unes :

* [_RAID0_](https://fr.wikipedia.org/wiki/RAID_%28informatique%29#RAID_0_:_volume_agr%C3%A9g%C3%A9_par_bandes) : Permet d'agréger des partitions, pour constituer un plus gros volume logique. Si vous avez 2 disques, disposant chacun d'une partition de 1 To en RAID0, vous disposez d'un volume logique de 2 To. Si un disque meurt, tout le volume est mort. On a donc, ici, multiplié le risque de panne par deux - ce n'est pas de la redondance.
* [_RAID1_](https://fr.wikipedia.org/wiki/RAID_%28informatique%29#RAID_1_:_Disques_en_miroir) : Un volume logique en RAID1 est constitué de partitions (une partition par disque physique) contenant à tout instant les mêmes données : les partitions sont « en miroir ». 

  * **Taille du volume logique** = taille de la partition la plus petite.
  * **Combien tolère-t-on de disques HS avant de perdre des données ?** Tous sauf un.

  Si vous avez 3 disques disposant chacun d'une partition de 1 To en RAID1, vous disposez d'un volume logique de 1 To, et vos données survivront tant qu'un des 3 disques survit.
  
* [_RAID5_](https://fr.wikipedia.org/wiki/RAID_%28informatique%29#RAID_5_:_volume_agr%C3%A9g%C3%A9_par_bandes_%C3%A0_parit%C3%A9_r%C3%A9partie) : Il a besoin d'au moins 3 partitions physiques sous-jacentes. Une des partitions contiendra un « bloc de parité », qui permettra de reconstituer les données si l'on perd n'importe lequel des disques. Les autres partitions contiendront des données.

  Considérant un volume RAID5 sur N partitions : 

  * **Taille du volume logique** = taille de la partition la plus petite x (N-1).
  * **Combien tolère-t-on de disques HS avant de perdre des données ?** Un seul.
  
* [_RAID6_](https://fr.wikipedia.org/wiki/RAID_%28informatique%29#RAID_6), c'est comme RAID5, avec 2 volumes de parité : on tolère la perte de 2 partitions, mais on a moins de stockage. RAID6 nécessite au moins 4 partitions sous-jacentes.

  * **Taille du volume logique** = taille de la partition la plus petite x (N-2).
  * **Combien tolère-t-on de disques HS avant de perdre des données ?** Jusqu'à deux.
  
* [_RAID10_](https://fr.wikipedia.org/wiki/RAID_%28informatique%29#RAID_10_(ou_RAID_1+0)) (ou RAID1+0) : on met un RAID0 sur un RAID1. Je vous laisse regarder par vous-même, il faut au moins 4-5 disques pour que ça vaille le coup : c'est pas pour tout le monde.

Que de choix ! Ici aussi, **c'est à vous de trouver un schéma de redondance adapté** à votre matériel et à vos besoins.

### Contraintes de partitionnement ou comment les éviter avec LVM

Le partitionnement c'est chiant : on est limité à un certain nombre de partitions par disque, les partitions doivent être contiguës, modifier un schéma de partitionnement une fois l'OS installé est risqué... Et enfin : **si on veut chiffrer ses données, on ne peut chiffrer qu'une partition à la fois, et ça demande une clé différente par partition !**

Heureusement qu'il y a [LVM](https://doc.ubuntu-fr.org/lvm) ! C'est un outil permettant la création de partitions logiques : donnez-lui une grosse partition (pourquoi pas redondée), et LVM vous permettra de gérer autant de sous-partitions « logiques » (virtuelles) en son sein, affranchies des contraintes listées ci-dessus.

C'est surtout pour nous permettre de chiffrer toute notre installation avec une seule clé qu'il nous est précieux, LVM : on crée une partition immense, avec de la redondance, on la chiffre, on met LVM dessus, et on découpe nos partitions systèmes là-dedans, comme on veut.

Même si vous ne souhaitez pas chiffrer vos données, LVM peut être une bonne idée, parce qu'il offre beaucoup plus de flexibilité que le partitionnement « à l'ancienne » pour pas cher.

## Partitionnement en pratique

Personnellement, aujourd'hui, j'ai deux configurations de serveur à partitionner :

* _3 disques de 2 To_ (machine Bebop) : 
  * Déjà, je n'ai pas vraiment besoin de *swap* (32 Go de RAM), mais on va en mettre quelques Go quand même.
  * Ce serveur étant loué dans un centre de données, je compte chiffrer mes données. Je ne veux pas avoir X clés de déchiffrement (une par partition) à rentrer à chaque démarrage, donc je vais utiliser LVM pour ne créer qu'une seule grosse partition chiffrée (dont LVM gèrera le sous-partitionnement pour mon système).
  * J'ai 3 disques, je vais donc pouvoir utiliser RAID5 pour la redondance de cette méga-partition LVM (RAID1 aurait toléré un *crash* de disque de plus, mais j'aurais eu moins d'espace disque).
  * Dans LVM, on aura presque 4 To d'espace ! On va faire une partition `/`, une `/home`, une `/var`, une `/data` et la `swap`. Leurs tailles seront facilement adaptables avec LVM à l'usage, donc on s'en fiche à ce stade.
  * Le chiffrement va nécessiter un partition `/boot`, pour que je puisse me connecter au serveur et le déchiffrer avant chaque démarrage (cf. [guide du chiffrement à froid](@/formations/chiffrement_froid.md)). La partition `/boot` sera redondée en RAID1.
  * Il va me falloir une petite partition d'amorçage BIOS/EFI de quelques Mo.

  Graphiquement, ça ressemblera à ça :
  
![Partitionnement de 3 disques avec partition /boot et LVM](/img/partitionnement_3_disques_lvm.svg)

* _2 disques de 2 To_ (machines Swordfish & Red Tail) : 

  * Ici, je mettrai 8 Go de *swap* (seulement 4 Go de RAM).
  * Pour le système, on va mettre une méga-partition LVM en RAID1 avec chiffrement, ne disposant que de deux disques.
  * `/boot` en RAID1 et une partition BIOS/EFI. Bref, à peu près pareil que pour la configuration avec 3 disques.
  
  En image :

![partitionnement_2_disques_lvm.svg](/img/partitionnement_2_disques_lvm.svg)


> L'article est en réécriture à partir de ce point !
{.is-warning}

#### BIOS ou EFI ?

Ces deux acronymes sont deux versions du système d'amorçage. Ils démarrent l'ordinateur et vont chercher l'OS à lancer sur disque. BIOS est plus vieux, donc on préfère utiliser EFI quand c'est supporté. (Pour les détails, faudra aller voir ailleurs.)

Pour savoir si votre système supporte BIOS ou EFI ou les deux, lancez la commande `dmidecode | less` en tant qu'administrateur :

```
# dmidecode 3.0
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.
61 structures occupying 2578 bytes.
Table at 0x....

Handle 0x0000, DMI type 0, 24 bytes
BIOS Information
        Vendor: Intel Corp.
        # [...]
        Characteristics:
                # [...]
                BIOS boot specification is supported
                UEFI is supported
# [...]
```

Comme vous pouvez le voir, on lit à la fois `BIOS boot specification is supported` et `UEFI is supported`. J'ai donc le choix.

#### Kibioctets (Kio ou KiB) ou Kilooctets (Ko ou KB) ?

Dans les commandes suivantes, j'utiliserai des `KiB` (kibioctets) et non des `KB` (kilooctets). [Cela n'a pas grande importance.](https://forums.commentcamarche.net/forum/affich-24162713-conversion-mo-en-mio)
La notation "kilo, méga..." du système métrique fonctionne en base 10 : « 1 Ko = 1000 (10³) octets ». 
En informatique, tout tourne en base 2 : « 1 Kio = 1024 (2¹⁰) octets ».
En conséquence, on a deux notations qui veulent dire presque la même chose : 1 Mio =  1,048576 Mo.

Ça serait quand même plus simple si tout le monde comptait en base 2 !

### Création des partitions sur le Bebop (3 disques)

* On commence par créer les tables de partition de chaque disque, au format GPT :

  **Attention, créer une table de partition sur un disque en supprime l'intégralité des données !**

  ```bash
  # ATTENTION, CETTE COMMANDE EFFACE L'ENTIÈRETÉ DE CHAQUE DISQUE
  parted -sa optimal /dev/sda mklabel gpt
  parted -sa optimal /dev/sdb mklabel gpt
  parted -sa optimal /dev/sdc mklabel gpt
  ```

* Partitionnement de `/dev/sda`, où réside le système :

  ```bash
  # Création de la première partition BIOS, de 1 Mio
  # syntaxe : parted -sa optimal [disque] mkpart [nom de la partition] [début] [fin]
  # "-s" pour "silent", sinon parted se permet de me poser des questions (le lourd)
  # "-a optimal" pour que parted aligne les partitions sur le disque de façon optimale
  parted -sa optimal /dev/sda mkpart bios 0% 1MiB
  # Seconde partition /boot, 512 Mio
  parted -sa optimal /dev/sda mkpart boot 1MiB 513MiB
  # Troisième partition /, 100 Gio
  parted -sa optimal /dev/sda mkpart system 513MiB 100513MiB
  # Dernière partition /home, de 1.5 Tio, alignée à la fin du disque
  parted -sa optimal /dev/sda mkpart home 500GiB 100%
  
  # Ajout du drapeau "bios_grub" à la partition BIOS :
  # (Permet au BIOS de savoir que cette partition lui est dédiée.)
  # syntaxe : parted [disque] set [partition] [drapeau] [état]
  parted /dev/sda set 1 bios_grub on
  ```
  
  Résultat : 
  
  ```bash
  # fdisk -l /dev/sda
  Disk /dev/sda: 1.8 TiB, 2000398934016 bytes, 3907029168 sectors
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 512 bytes
  I/O size (minimum/optimal): 512 bytes / 512 bytes
  Disklabel type: gpt
  Disk identifier: 7DE23465-3A2B-4145-99F3-0A232C2DD37D

  Device          Start        End    Sectors  Size Type
  /dev/sda1          34       2047       2014 1007K BIOS boot
  /dev/sda2        2048    1050623    1048576  512M Linux filesystem
  /dev/sda3     1050624  205850623  204800000 97.7G Linux filesystem
  /dev/sda4  1048576000 3907028991 2858452992  1.3T Linux filesystem
  ```


* Partitionnement de `/dev/sdb` et `/dev/sdc`, et création du volume logique en RAID1 :

  ```bash
  # Création d'une partition occupant tout l'espace, sur chaque disque
  parted -sa optimal /dev/sdb mkpart primary 0% 100%
  parted -sa optimal /dev/sdc mkpart primary 0% 100%
  # Ajout du drapeau "raid" sur chaque partition nouvellement créée
  parted /dev/sdb set 1 raid on
  parted /dev/sdc set 1 raid on
  # Création du volume RAID1
  mdadm --verbose --create /dev/md0 --level=1 --raid-devices=2 /dev/sdb1 /dev/sdc1
  # Répondez "y" si on vous emande confirmation.
  ```
  
  Résultat : 
  
  ```bash
  # fdisk -l
  Disk /dev/sdb: 1.8 TiB, 2000398934016 bytes, 3907029168 sectors
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 512 bytes
  I/O size (minimum/optimal): 512 bytes / 512 bytes
  Disklabel type: gpt
  Disk identifier: 44D8D3D8-6EF3-4052-AFB6-850DEED66C81

  Device     Start        End    Sectors  Size Type
  /dev/sdb1   2048 3907028991 3907026944  1.8T Linux RAID

  Disk /dev/sdc: 1.8 TiB, 2000398934016 bytes, 3907029168 sectors
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 512 bytes
  I/O size (minimum/optimal): 512 bytes / 512 bytes
  Disklabel type: gpt
  Disk identifier: A89A5F18-5383-41F8-A8C8-3960E12D9AF2

  Device     Start        End    Sectors  Size Type
  /dev/sdc1   2048 3907028991 3907026944  1.8T Linux RAID

  Disk /dev/md0: 1.8 TiB, 2000263577600 bytes, 3906764800 sectors
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 512 bytes
  I/O size (minimum/optimal): 512 bytes / 512 bytes
  ```
  
Et pour tout le disque, nous obtenons :
  
```bash
# lsblk
NAME    MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
sdb       8:16   0  1.8T  0 disk
└─sdb1    8:17   0  1.8T  0 part
  └─md0   9:0    0  1.8T  0 raid1
sdc       8:32   1  1.8T  0 disk
└─sdc1    8:33   1  1.8T  0 part
  └─md0   9:0    0  1.8T  0 raid1
sda       8:0    0  1.8T  0 disk
├─sda4    8:4    0  1.3T  0 part
├─sda2    8:2    0  512M  0 part
├─sda3    8:3    0 97.7G  0 part
└─sda1    8:1    0 1007K  0 part    
```
  
### Création des partitions sur Swordfish & Red Tail (2 disques)


* On commence par créer les tables de partition de chaque disque, au format GPT :

  **Attention, créer une table de partition sur un disque en supprime l'intégralité des données !**

  ```bash
  # ATTENTION, CETTE COMMANDE EFFACE L'ENTIÈRETÉ DE CHAQUE DISQUE
  parted -sa optimal /dev/sda mklabel gpt
  parted -sa optimal /dev/sdb mklabel gpt
  ```
  
* Partitionnement des volumes système :
	
	```bash
  # partition BIOS, 1 Mio, sur /dev/sda
	parted -sa optimal /dev/sda mkpart bios 0% 1MiB
  parted /dev/sda set 1 bios_grub on
  # partition /boot, 512 Mio, sur /dev/sda
  parted -sa optimal /dev/sda mkpart boot 1MiB 513MiB
  #  partition swap, 8 Gio, sur /dev/sda
  parted -sa optimal /dev/sda mkpart swap 513MiB 8513MiB
	# partition /, 100 Gio, sur /dev/sda
  parted -sa optimal /dev/sda mkpart system 8513MiB 108513MiB
	# partition /home, 500 Gio, sur /dev/sdb
  parted -sa optimal /dev/sdb mkpart home 0% 500GiB
	```

* Création des deux partitions d'1.5 Tio à la fin de chaque disque :

	```bash
  parted -sa optimal /dev/sda mkpart primary 500GiB 100%
  parted -sa optimal /dev/sdb mkpart primary 500GiB 100%
  ```
  
* Création du volume logique en RAID1 :
  
  
	Pour commencer, il ne faut pas se tromper de numéro de partition ! 
  
  ```bash
  # lsblk
  NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
  sdb      8:16   0  1.8T  0 disk
  ├─sdb2   8:18   0  1.3T  0 part            # <-- futur RAID1
  └─sdb1   8:17   0  500G  0 part
  sda      8:0    0  1.8T  0 disk
  ├─sda4   8:4    0 97.7G  0 part
  ├─sda2   8:2    0  512M  0 part
  ├─sda5   8:5    0  1.3T  0 part            # <-- futur RAID1
  ├─sda3   8:3    0  7.8G  0 part
  └─sda1   8:1    0 1007K  0 part
  ```
  
  On va mettre le RAID1 sur la **5e** partition du disque `sda`, et sur la **2e** partition de `sdb`.
  
  Il reste seulement à ajouter les drapeaux `raid` sur les deux partitions, avant d'invoquer `mdadm` qui va créer le volume RAID1 :
	
  ```bash
  parted /dev/sda set 5 raid on
  parted /dev/sdb set 2 raid on

  mdadm --verbose --create /dev/md0 --level=1 --raid-devices=2 /dev/sda5 /dev/sdb2
  ```

Et voilà le travail !

```bash
# lsblk
NAME    MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
sdb       8:16   0  1.8T  0 disk
├─sdb2    8:18   0  1.3T  0 part
│ └─md0   9:0    0  1.3T  0 raid1
└─sdb1    8:17   0  500G  0 part
sda       8:0    0  1.8T  0 disk
├─sda4    8:4    0 97.7G  0 part
├─sda2    8:2    0  512M  0 part
├─sda5    8:5    0  1.3T  0 part
│ └─md0   9:0    0  1.3T  0 raid1
├─sda3    8:3    0  7.8G  0 part
└─sda1    8:1    0 1007K  0 part
```

## Installation des systèmes de fichier 

C'est magnifique, nous avons des partitions. Mais pour l'instant, elles ne servent à rien : il leur manque un système de fichier pour être utilisables par un système d'exploitation. 

Si vous comptez chiffrer les disques de votre futur serveur (ce qu'on recommande chaudement si votre serveur est dans un centre de données), il faut configurer le chiffrement *avant* d'installer un système de fichier. Rendez-vous donc sur le [guide du chiffrement à froid](@/formations/chiffrement_froid.md), où on traitera aussi de l'installation du système de fichier de A à Z : vous avez donc fini ce guide, bravo !

---

Si vous êtes encore là, vous n'avez pas l'intention de chiffrer vos disques. Il ne vous reste donc plus qu'à ajouter ce satané système de fichiers à vos partitions, et vous pourrez enfin installer votre système d'exploitation préféré !

### Oui mais quel système de fichiers ? `ext4` ou `ZFS` ou autre ?

> TODO: ext4 vs. ZFS. [Un guide en anglais](https://nascompares.com/2021/07/21/zfs-vs-ext4-for-nas-what-is-the-difference-in-your-file-system/).

# Références

* ADRN, [Guide d'installation de Debian avec chiffrement à froid dans le centre de données Kimsufi](https://plume.deuxfleurs.fr/~/WebTrotter/installing-a-cloud-server-with-full-disk-encryption), Blog Deuxfleurs, avril 2021, anglais.
