---
title: "Dépôt officiel"
description: "Le dépôt officiel nixpkgs"
weight: 40
extra:
  parent: 'formations/nix.md'
---
