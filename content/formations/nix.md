---
title: "Écosystème Nix"
description: "L'écosysème Nix : nixlang, nixpkgs, nixos, flakes, etc."
sort_by: "weight"
weight: 60
extra:
  parent: 'formations/_index.md'
---

# Apprendre

Suivez les liens dans le menu de gauche dans l'ordre

# Ressources

<https://search.nixos.org> pour trouver un paquet.

Se référer au wiki NixOS pour comprendre rapidement un domaine.
Par exemple domaine = nodejs ou domaine = audio linux.
