---
title: "Le libre en Europe"
description: "Le libre en Europe (libre hoster, hackmeeting, assos)"
weight: 10
extra:
  parent: 'formations/conf.md'
---

**Animatrice :** Florence de MarsNet  
**Prise de note :** Florence de MarsNet, Stéphane de Hadoly  
**Compte-rendu :** Quentin de Deuxfleurs  
**Document source :** [Libreto Camps CHATONS 2022](https://libreto.sans-nuage.fr/camp-chatons-2022/dimanche+21+-+ateliers)

# Plan 

 1. **Financer ses déplacements** - a des conf ou des hackmeeting, ou ses rencontres de consoeurs/frères europeen avec ERASMUS mobilité éducation des adultes, partage d'expérience d'Assodev-Marsnet - agenda des prochains événements

 2. **Libre hoster** - collectif d'hébergeurs anglophone - présentation

 3. **Open Minds** - Solutions numériques libres pour les associations européenne (plateforme de ressources libres + formations) projet financé par ERASMUS, et porté par 4 structures Européennes dont le CHATON ASSODEV-MARSNET

# 1. Financer ses mobilités

**But :** Financer ses déplacements a des conf ou des hackmeeting, ou ses rencontres de consoeurs/frères europeen avec ERASMUS mobilité éducation des adultes 

**À propos de Assodev** - 
Marsnet est un Hébergeur associatif et militant, situé à Marseille, créé en 2004 par des activistes du libre ayant contribué à la création de Globenet, No-Log et Gitoyen.
Assodev est l'association qui gère Marsnet. Créée en 2001, son but est le développement des associations et la promotion de l'internet solidaire et de l'informatique libre.

*But :* promotion du numérique libre - développement des associations - éducation populaire  
*Activités :* Information, Accompagnement, Formation et Services numériques  
tout public mais spécialiste du numérique libre à usage associatif, social et solidaire  
*Personnes* : 3 permanents (1 salarié + 2 bénévoles). 42 contributeurs dont 20 bénévoles actifs (CA, tech et groupe de travail). 250 membres, principalement des associations.


C'est l'asso qui porte le Chatons Marseille-Yeah Marsnet, propose de partager son expérience de financement de ses déplacements a des conf ou des hackmeeting, ou ses rencontres de consoeurs/frères europeen avec ERASMUS mobilité éducation des adultes.


Il y a une proximité avec d'autres acteurs du libre et d'autres chatons : Evolix et le collectif d'acteurs du libre à Marseille et environ aïolibre :)
April , Axul, Evolix, CercLL, Funkwhale, LQDN Technopolice Marseille, OSM PACA, Plug, Revelibre,...


**MarsNet finance ses mobilitiés via Erasmus** - Nous avons obtenu un financement pour de 40 000 e pour financer 26 mobilités en europe ( Voyage 275/p + hébergement + repas 106 + orga 350 / p ) de nos bénévoles acteur de l'éducation des adultes. Le but d'Erasmus c'est aussi l'éducation des adultes : dans le cas de MarsNet, développement des compétences numériques du personnel éducatif salarié ou bénévoles. Erasmus ne finance pas que les étudiants mais aussi les profs/formateurs donc.

**Projet Erasmus en cours** - Éducation au Numérique Libre, Éthique et Solidaire en Europe
Actions financées :
 - suivi de formation ( rencontres avec ateliers et les hackmeeting )
 - stage d'observation en situation de travail ( rencontre de confrere education num libre, pour partage de savoirs sur nos pratiques éducatives, les méthodes et outils pédagogiques et sur les contenus
 - dispense de formation

**MarsNet partage ses mobilités** - MarsNet propose au CHATONS de les rejoindre en tant que "personne éducatif d'Assodev-Marsnet" en contribuant à des actions d'éducation populaire pour assodev-marsnet (cela peut se faire à distance et être une contribution comme échanger des tutoriels ou autres doc pédagogique....). En retour, MarsNet peut financer des mobilités pour ces personnes, il faut s'engager à jouer le jeu de la subvention en retour, prendre des notes des ateliers ou stage d'observation, répondre au questionnaire de fin, etc.

[Voir aussi AioLibre](https://www.aiolibre.org)

# 2. Libre Hoster


Un peu comme chatons mais anglophone, existe depuis moins longtemps (2018) , plus informelle, moins organisé, moins nombreux, n'ont pas vraiment de site mais un forum avec 106 utilisateurs (mais pas de liste de structure), tres divers, des individu, des groupe d'amis , des asso, des coopérative, ils ont des réunion vituelle irréguliere et se rencontre lors de metting autour du libre, sont éloignés car répati dans le monde, surtout europe, pas vraiment de charte, un site et un github pas a jour, pas vraiment d'activité

Les Librehosters forment un réseau de coopération et de solidarité qui utilise le logiciel libre pour encourager la décentralisation par la fédération et les plateformes distribuées.

Nos valeurs associent la transparence, l'équité et le respect de la vie privée à une culture de la portabilité des données et des contributions publiques aux biens communs.

Exemple : Je suis realitygaps, administrateur d'une petite fondation à but non lucratif https://weho.st 15 basée à Amsterdam, aux Pays-Bas.
Nous avons notre propre matériel et nous hébergeons dans un centre de données local avec une association de colocation. Nous fournissons des services gratuits et des services de contributeurs sur la base de dons. Actuellement nous sommes deux admins principaux, moi et @mattronix.

**Ressources**
  - [Site web LibreHost](https://libreho.st/)
  - [Forum LibreHost](https://talk.libreho.st/)
  - [Liste référencement LibreHost (pas à jour)](https://github.com/libresh/awesome-librehosters)
  - [Dépôt Git Annuaire LibreHost pour demandé à être référencé](https://lab.libreho.st/librehosters/directory)

# 3. OPEN MINDS - Solutions numériques libres adaptées au secteur socio-culturel

*Open Minds - Solutions numériques libres pour les associations européenne (plateforme de ressources libres + formations) projet financé par ERASMUS, et porté par 4 structures Européennes dont le CHATON ASSODEV-MARSNET*


**Définition du projet européen**
*Objectif :* Promotion du numérique libre auprès des associations européenne  
*Partenaires :*
 - CESEP http://www.cesep.be/
 - ASSODEV (Marseille, France )
 - ACDC (Roumanie-Bucareste), http://acdcromania.ro/
 - Transit Project (Espagne - Barcelone) http://blog.transit.es
*Contenu* : 6 modules de formation et une plateforme de solutions et ressources  
*Public* : personnels éducatifs et acteurs de formation des structures socio-culturelles et associatives, leurs bénéficiaires, membres, bénévoles et partenaires.  
*Budget* : Total des 4 partenaires : 250 000 euros; Assodev environ 30 a 40 K€ /an sur 2 ans (va permettre la réembauche de 2 salariés)
*Dates* :  01/10/2022 au 31/01/25 (28 mois, soit un peu plus de 2 ans)

**En détails** - 
Une plateforme web pédagogique, destinée à mettre en libre disposition un ensemble de ressources et de solutions numériques libres spécifiques aux secteur socio-culturelle : logiciels, services en ligne et méthodes d'animation, bonnes pratiques, sélection de formations, documentation, annuaires d'adresses ( structures d’accompagnement et de formations, prestataires de services, associations usagères). 6 modules de formation en ligne à distance et en présentiel. Concrètement il s'agit de productions pédagogiques combinant formations en présentiel (3J par formation) et en ligne en utilisant des approches similaires au webinaire et MOOC : une dizaine de vidéo d'en moyenne 20 minutes, des tutoriels, des documentation, des quizz, QCM, documents en ligne, exercices, auto-évaluation, des méthode pédagogique, méthode d'évaluation

**En comparaison des autres initiatives**  
[Solidatech](https://www.solidatech.fr/) - Dispositif financé par TechSoup, l'ONG des GAFAM. Stratégie du cheval de Troie où les GAFAM se cachent dernière des asso "sympa" type Emmaüs qui portent le dispositif Solidatech. Les assos sympas sont prises au piège car elles en tirent des revenus.


[PANA](https://pana-asso.org/pana-parler-numerique-aux-associations/) - Pas le modèle prédateur de Solidatech mais ne requiert pas que le logiciel soit libre.

[Emancip'Asso](https://soutenir.emancipasso.org/) - complémentaire : là où émancip'asso s'adresse aux hébergeurs pour les former aux besoins des assos, open minds s'adresse aux assos pour qu'elles comprennent le fonctionnement des hébergeurs et du numérique.

**Recherche de partenariat**

MarsNet a les subventions mais besoin de partenariat avec tete de réseaux associatif pour

 - affiner l'identification des besoins
 - contribuer à la conception des formation, notamment en terme de besoin et appart de compétence
 - contribuer à la conception de la plateforme , notamment en terme de besoins
 - contributions à la plateforme (mise à jour et ajout de fiches ressources).
 - tester la plateforme et les modules de formation
 - promouvoir la plateforme et les formations

Partenaires potentiels :

 - La ligue de l'enseignement
 - Le Mouvement Associatf Sud
 - Le Mouvement de l'Economie Solidaire
 - Initiatives Europe Conseil,
 - La Fédération des Espaces Santé Jeunes,
 - La Fédération des acteurs de la Solidarité
 - CHATONS
 - Acteurs du libres
 - Les partenaires d'Emancip'Asso (CAC, Ceméa, Recia, chambres régionales de l'ESS - cress, e-graine, MFR, Ligue de l'Enseignement, le Mouvement Associatif, Maison des Actions Solidaires, Res Numerica, Oopale - DLA Culture, etc.) et CHATONS (Colibris, Sleto et Zourit).

**Le futur site web Open Minds**

La plateforme les regroupant sera structurée suivant divers angles :

 - Solutions numériques libres logiciels et services en ligne personnalisés, méthodes d'animation et de coopération : 100 à 200 fiches
 - Bonnes pratiques : (questionnaire + vidéo) exemples d’utilisation réussies 15-30 fiches
 - Ressources permettant au public de s’en emparer de manière autonome.
 - Sélection de formations et mise en ligne des formations Open Minds 20 à 40 fiches
 - Documentation, présentation didactiques et tutoriel réalisés dans le module de travail 2 formation et liens vers des documentations existantes 40 à 80 fiches
 - Annuaire d'acteurs du libre : prestataires de services, structures d’accompagnement et de formation) 100 à 150 fiches
 - Autres : Présentation, agenda, actualité, forum

**Formations**

6 modules de formation en ligne à distance et en présentiel, 5 modules seront développés de façon transversale, et 1 module sera développé pour répondre à des besoins spécifiques:

  1. Outils collaboratif et travail à distance (Espace collaboratif et partage de documents, d'agenda, de contacts, discussion, forum, gestion de projet, rédaction collaborative, vidéo conférence)
  2. Communication (sites web - réseaux sociaux - lettre d'information)
  3. Solutions multimédias (Vidéo, capsules vidéos, court métrage, communication incluant le son et l'image, PAO) 
  4. Bureautique et Internet (suite bureautique, outils de navigation/messagerie)
  5. Solutions de gestion (adhérents, contacts,clients, CRM, facturation, compta, gestion de projets)
  6. Autonomie numérique (hébergements décentralisés, protection de la vie privée). Support pédagogique,
