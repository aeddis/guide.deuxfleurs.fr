---
title: "Articles"
description: "Articles"
weight: 20
extra:
  parent: 'formations/mediatheque.md'
---

![Capture d'écran de l'article Big Other: Surveillance capitalism](/img/cover/zuboff.png)

🇬🇧 Zuboff, S. (2015) **‘Big other: Surveillance Capitalism and the Prospects of an Information Civilization’**, Journal of Information Technology, 30(1), pp. 75–89. doi: [10.1057/jit.2015.5](https://journals.sagepub.com/doi/10.1057/jit.2015.5).  

*This article describes an emergent logic of accumulation in the networked sphere, ‘surveillance capitalism,’ and considers its implications for ‘information civilization.’ The institutionalizing practices and operational assumptions of Google Inc. are the primary lens for this analysis as they are rendered in two recent articles authored by Google Chief Economist Hal Varian. Varian asserts four uses that follow from computer-mediated transactions: data extraction and analysis,’ ‘new contractual forms due to better monitoring,’ ‘personalization and customization, ’ and continuous experiments. ’ An examination of the nature and consequences of these uses sheds light on the implicit logic of surveillance capitalism and the global architecture of computer mediation upon which it depends. This architecture produces a distributed and largely uncontested new expression of power that I christen: Big Other. ’ It is constituted by unexpected and often illegible mechanisms of extraction, commodification, and control that effectively exile persons from their own behavior while producing new markets of behavioral prediction and modification. Surveillance capitalism challenges democratic norms and departs in key ways from the centuries-long evolution of market capitalism.*

![Capture d'écran de l'article Nothing to hide](/img/cover/solove.png)

🇬🇧 Solove, Daniel J., **'I've Got Nothing to Hide' and Other Misunderstandings of Privacy**. San Diego Law Review, Vol. 44, p. 745, 2007, GWU Law School Public Law Research Paper No. 289, [Available at SSRN](https://ssrn.com/abstract=998565)

*In this short essay, written for a symposium in the San Diego Law Review, Professor Daniel Solove examines the nothing to hide argument. When asked about government surveillance and data mining, many people respond by declaring: "I've got nothing to hide." According to the nothing to hide argument, there is no threat to privacy unless the government uncovers unlawful activity, in which case a person has no legitimate justification to claim that it remain private. The nothing to hide argument and its variants are quite prevalent, and thus are worth addressing. In this essay, Solove critiques the nothing to hide argument and exposes its faulty underpinnings.*

![Capture d'écran de l'article Gouvernementalité algorithmique](/img/cover/rouvroy.png)

🇫🇷 Rouvroy, A. & Berns, T. (2013). **Gouvernementalité algorithmique et perspectives d'émancipation: Le disparate comme condition d'individuation par la relation ?**. Réseaux, 177, 163-196. doi: [10.3917/res.177.0163 ](https://doi.org/10.3917/res.177.0163).

*La gouvernementalité algorithmique se caractérise notamment par le double mouvement suivant : a) l’abandon de toute forme d’« échelle », d’« étalon », de hiérarchie, au profit d’une normativité immanente et évolutive en temps réel, dont émerge un « double statistique » du monde et qui semble faire table rase des anciennes hiérarchies dessinée par l’homme normal ou l’homme moyen ; b) l’évitement de toute confrontation avec les individus dont les occasions de subjectivation se trouvent raréfiées.*


