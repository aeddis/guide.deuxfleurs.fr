---
title: "Médiathèque critique"
description: "Médiathèque critique"
weight: 20
sort_by: "weight"
extra:
  parent: 'formations/_index.md'
---

# Des livres

![Couverture de la Convivialité d'Illich](/img/cover/convivialite.jpg)
![Couverture du Macroscope](/img/cover/macroscope.jpg)
![Couverture de l'ordre moins le pouvoir](/img/cover/pouvoir.jpg)
![Couverture de technologie partout démocratie nulle part](/img/cover/techno-partout.jpg)
![Couverture de Cyberstructure](/img/cover/cyberstructure.jpg)
![Couverture de L'institution imaginaire de la société](/img/cover/imaginaire.jpg)
![Couverture de Telecommunications Reclaimed](/img/cover/telco.jpg)
![Couverture de Permanent Record](/img/cover/permanent-record.jpg)
![Couverture de La France contre les robots](/img/cover/France-contre-robots.jpg)
![Couverture de Carbon Democracy de Timothy Mitchell](/img/cover/carbon_democracy.jpg)
![Couverture de Surveiller et punir de Michel Foucault](/img/cover/surveiller-et-punir.jpg)
![Couverture d'Internet et libertés de Mathieu Labonde, Lou Malhuret, Benoît Piedallu et Axel Simon](/img/cover/internet-et-libertés.jpg)
![Couverture de À bout de flux, de Fanny Lopez](/img/cover/a-bout-de-flux.jpg)
![Couverture de Déclic, de Maxime Guedj et Anne-Sophie Jacques](/img/cover/déclic.jpg)
![Couverture de Technocritiques](/img/cover/technocritiques.jpg)
![Couverture de La Crise de la Culture](/img/cover/la-crise-de-la-culture.jpg)
![Couverture de Le Consumérisme à travers ses objets](/img/cover/le-consumerisme-a-travers-ses-objets.jpg)
![Couverture de Contre l'alternumérisme](/img/cover/contre-lalternumerisme.jpg)
![Couverture de Merci de Changer de Métier](/img/cover/merci-de-changer-de-metier.jpg)
![Couverture de L'âge des Low-Tech](/img/cover/lage-des-low-tech.jpg)
![Couverture de Le Soin des Choses](/img/cover/le-soin-des-choses.jpg)

[↣ Consulter la page détaillée sur les livres](@/formations/livres.md)

# Des articles

![Capture d'écran de l'article Big Other: Surveillance capitalism](/img/cover/zuboff.png)

[↣ Consulter la page détaillée sur les articles](@/formations/articles.md)

# Des podcasts

🇫🇷 **Xavier de la Porte, "Le code a changé", France Inter** 

![Vignette du podcast Le Code a changé <](/img/cover/code.jpg) *"Le code a changé" parle de numérique. Mais comment ? Pourquoi ? Et en quoi toutes ces technos changent quelque chose à nos vies ? Xavier de La Porte tourne autour de la question avec ses invités dans ce podcast original.*

[↣ Écouter sur France Inter](https://www.radiofrance.fr/franceinter/podcasts/le-code-a-change)

# En vidéo

**Chris Eley, 'Jurassic Web, Une préhistoire des réseaux sociaux', France, 2020**

![Miniature du docu Jurassic Web <](/img/cover/jurassic.jpg)
*Dès les années 1960, bien avant Facebook, Twitter et Instagram, des passionnés ont détourné les technologies de l'époque pour mieux communiquer, partager leurs passions et créer des communautés libres. En sept épisodes, cette websérie documentaire sort de l'oubli ces geeks de la première heure et explore la préhistoire des réseaux sociaux.*

[↣ Voir Jurassic Web sur invidious.fdn.fr](https://invidious.fdn.fr/watch?v=mLEbi2MAoL8)

# Dans les blogs

🇬🇧 **Mike Tully, 'On Community Memory', Blog are.na, 2022-05-19**

![Photograph by Gwen Bell. Courtesy of the Computer History Museum. A black-and-white photo of an old Community Memory terminal with flowers planted in the spot where the keyboard should be, pleasant and surreal. <](/img/cover/compmem.jpg)
*In the early 1970s in Berkeley, California, Lee Felsenstein, a computer science drop-out from UC Berkeley, started thinking about a more technological approach to community organizing. Felsenstein had participated in the Free Speech Movement and anti-Vietnam protests in the years prior and saw potential in the computer for efficiently bringing people together for social change. But at the time, computers were typically only found in government agencies and research institutions, not accessible to the general public. One notable exception was at the technological commune Project One.*

[↣ Lire en ligne](https://www.are.na/blog/community-memory)



# Notre bibliothèque sur Zotero

[Zotero](https://www.zotero.org/) est un gestionnaire de bibliographie libre et très sympathique, une fois assimilée son interface graphique. Nous l'utilisons pour stocker tout un paquet de ressources&mdash;des vidéos aux articles de recherche.

[**Notre bibliothèque Zotero**](https://www.zotero.org/groups/2826038/deuxfleurs/items/53EIQHFG/library)
