---
title: "Se former"
description: "Se former"
weight: 30
sort_by: "weight"
extra:
  parent: 'formations/_index.md'
---

Ce manuel vous propose de vous former sur les questions portées par l'association, que ce soit sur l'impact social du numérique ou l'administration d'une machine Linux, avec dans l'idée que vous pourrez vous impliquer d'avantage dans nos activités après, en faisant des ateliers ou en participant à opérer les machines et les logiciels.

