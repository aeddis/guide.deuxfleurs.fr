---
title: "Encore plus"
description: "Encore plus"
weight: 100
extra:
  parent: 'formations/mediatheque.md'
---

Des ressources qui ont l'air intéressantes mais qu'on n'a pas encore creusées :
 - [Instaurer des données, instaurer des publics : une enquête sociologique dans les coulisses de l'open data ](https://pastel.archives-ouvertes.fr/tel-01458098) - Samuel Goeta
 - [De briques et de blocs. La fonction éditoriale des interfaces de programmation (api) web : entre science combinatoire et industrie du texte](https://www.theses.fr/2017PA040188) - Samuel Goyet
 - [La politique des grands nombres](https://www.cairn.info/la-politique-des-grands-nombres--9782707165046.htm) - Alain Desrosières, ancien de l'INSEE
 - [Dominique Boullier](https://www.boullier.bzh/) sur les territoires et la neutralité du net
 - [En attendant les robots](https://www.seuil.com/ouvrage/en-attendant-les-robots-antonio-a-casilli/9782021401882) de Antonio Casilli
 - [Sociologie d'Internet](http://www.costech.utc.fr/CahiersCOSTECH/spip.php?article77) avec Beuscart
 - Dominique Cardon
 - [Twitter et les gaz lacrymogènes. Forces et fragilités de la contestation connectée](https://journals.openedition.org/lectures/38417) - Zeynep Tufekci
   - (En cours de lecture par Quentin)
 - [It's complicated](https://journals.openedition.org/lectures/17628) - Danah Boyd
 - [The wealth of networks](http://www.benkler.org/Benkler_Wealth_Of_Networks.pdf) - Yochai Benkler
 - [Les besoins artificiels - Comment sortir du consumérisme](https://www.editionsladecouverte.fr/les_besoins_artificiels-9782355221262) -  Razmig Keucheyan
 - [Aux sources de l'utopie numérique : De la contre-culture à la cyberculture, Stewart Brand, un homme d'influence](https://cfeditions.com/utopie-numerique/) - Fred Turner
 - [Retour d'utopie - De l'influence du livre de Fred Turner](https://cfeditions.com/retour-utopie/) -  Olivier Alexandre, Thomas Cazals, Anne Cordier, Adrian Daub, Xavier de La Porte, Hervé Le Crosnier, Christophe Masutti, Julie Momméja, Francesca Musiani, Valérie Schafer, Nicolas Taffin, Laurent Vannini et François Vescia.
 - [L'utopie déchue, une contre-histoire d'Internet](https://www.fayard.fr/livre/lutopie-dechue-9782213710044/) - Félix Tréguer
 - [La Tech, Quand la Silicon Valley refait le monde](https://www.seuil.com/ouvrage/la-tech-olivier-alexandre/9782021520187) - Olivier Alexandre
 - [Internet, année zéro](https://www.editionsdivergences.com/livre/internet-annee-zero) - Jonathan Bourguignon
 - [Perspectives low-tech](https://www.editionsdivergences.com/livre/perspectives-low-tech) - Quentin Mateus et Gauthier Roussilhe 
 - [Rhétorique du texte numérique](https://presses.enssib.fr/catalogue/rhetorique-du-texte-numerique) - Saemmer Alexandra


*Note : pour les livres, plutôt que de foncer tête baissée dedans, commencer par lire des notes de lecture, on en trouve sur cairn*
