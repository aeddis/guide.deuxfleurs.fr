---
title: "Administration Système"
description: "Administration Système"
sort_by: "weight"
weight: 50
extra:
  parent: 'formations/_index.md'
---

L'administration système est un concept aux contours flous.
Ici, on va considérer que c'est tout ce qui consiste à faire en sorte que les applications soient disponibles, que les données soient en sécurité, et que les ordinateurs puissent communiquer entre eux à travers le réseau qui nous est mis à disposition.
Et tout ça doit être garanti alors que les usages des services peuvent changer, que des machines peuvent tomber en panne, que des attaques informatiques sont tentées sur notre système, que des fibres sont coupées par des tractopelles, que des bugs existent dans les logiciels, etc.
Promis, on ne s'ennuie pas !

Dans d'autres domaines, il est courant de faire la différence entre "conception" et "production", et en réalité cette catégorisation s'applique très bien au monde de l'informatique tel qu'il est constitué aujourd'hui : le développement logiciel est la conception, et l'administration système est la production.
Cette séparation entre "conception" et "production" n'est pas intrinsèque à l'informatique, elle est artificielle et souvent remise en question.
Par exemple, les entreprises de la Silicon Valley ont remarqué que ce découpage était vecteur d'inefficiences. 
Formulé avec leurs mots, cette critique a donné des expressions comme "tu le développes, tu l'administres" ou encore les concepts de DevOps et SRE, qui sont parfois devenus plus des buzzwords pour cacher que rien d'autre n'avait changé.
On pourrait tout aussi bien parler de [division du travail](https://www.universalis.fr/encyclopedie/division-du-travail/).

Chez Deuxfleurs, si on identifie des tâches comme relevant de "la conception" ou de "la production", nos membres de sont pas spécialisés dans un domaine : une même personne développe et opère les logiciels.
Adopter cette attitude a plusieurs avantages importants : ça encourage à concevoir des logiciels faciles à opérer, robuste et résilients, ça permet aussi d'identifier, de remonter et de corriger directement des bugs ou anomalies identifiées en production, 
enfin ça permet de mobiliser les outils de développement (exemple : les langages de programmation) pour opérer le système de manière plus sûre et plus rapide.

Évidemment, pouvoir jongler entre conception et opération, ça demande un grand nombre de qualifications.
Heureusement, la plupart des ressources pour se former sont accessibles gratuitement sur Internet et sont d'excellentes qualité.
De plus, un grand nombre de ces dernières sont mêmes accessibles en Français.
Bref, avec un peu d'aide, il est tout à fait possible de s'auto-former.

Pour bien commencer, il faut des bases théoriques dans un ensemble de domaines précis : réseau, système d’exploitation, « ingénieurie de fiabilité », et sécurité/cryptographie.
Pour couvrir plus largement le domaine, il est bon d'inclure également un peu de programmation, un peu d’architecture matérielle, un peu de base de données, un peu d’algorithmie, un peu de structure de données, etc. Une fois cette base acquise, il est plus aisé de se focaliser sur le le fonctionnement précis d'une technologie en particulier car on pourra toujours s'appuyer sur ses bases théoriques.

Ces pages ont pour objectif de référencer les bases théoriques existantes et d'apporter des bases pratiques spécifiques complémentaires.

# Ressources pédagogiques

**Les réseaux de zéro - Zeste de savoir**. Ce livre abondamment illustré explique de manière simple et moderne les principes fondamentaux des réseaux (topologies, couches, protocoles...), avec à l'appui de nombreux schémas, exemples, exercices et cas pratiques. En particulier, il utilise le simulateur de matériel réseau Packet Tracer, outil gratuit édité par Cisco, pour proposer au lecteur des mises en situation et des exercices de configuration. Agréable à lire, cet ouvrage d'une grande pédagogie offre un panorama complet des réseaux informatiques, en accompagnant le lecteur jusqu'à la compréhension des concepts les plus complexes. Sans nécessiter de prérequis, il constituera un excellent support pour tous ceux et celles qui souhaitent acquérir un bagage solide dans ce domaine.

[Acheter le livre](https://www.eyrolles.com/Informatique/Livre/les-reseaux-de-zero-9782416005237/) - [Lire gratuitement en ligne](https://zestedesavoir.com/tutoriels/2789/les-reseaux-de-zero/)

**Système d'exploitation 3IF - INSA Lyon**. Contenu du cours :
  - Chap. 1: noyau, shell, appel système
  - Chap. 2: partage du temps, ordonnancement
  - Chap. 3: mémoire virtuelle
  - Chap. 4: allocation dynamique de mémoire
  - Chap. 5: threads et synchronisation
  - Chap. 6: stockage et systèmes de fichiers

[Accéder au cours sur le Moodle de l'INSA Lyon](https://moodle.insa-lyon.fr/course/view.php?id=4045)

**Linux - OpenClassrooms**. Pour avoir une approche pratique aux systèmes d'exploitations avec Linux après le cours théorique précédent.

[Lire "Initiez vous à Linux"](https://openclassrooms.com/fr/courses/7170491-initiez-vous-a-linux) - [Lire "Administrez un système Linux"](https://openclassrooms.com/fr/courses/7274161-administrez-un-systeme-linux)

**Initiation à Linux sur un serveur - Librecours**. Une autre approche à Linux, directement orientée serveurs

[Lire "Initiation à Linux sur un serveur"](https://librecours.net/parcours/linvps-001/)

**Le langage C - Zeste de Savoir**. Pour être proche du matériel et mieux appréhender comment ton système/OS a été conçu, et comment mieux concevoir et opérer tes applications.

[Lire "Le langage C"](https://zestedesavoir.com/tutoriels/755/le-langage-c-1/)

**Python - Zeste de Savoir & Site du Zéro**. Langage de script haut niveau très utilisé

[Cours Zeste de Savoir](https://zestedesavoir.com/tutoriels/799/apprendre-a-programmer-avec-python-3/) - [Cours Site du Zéro](https://user.oc-static.com/ftp/livre/python/apprenez_a_programmer_en_python.pdf)

**Ingénieurie de la fiabilité - Google**. Google publie un livre de référence en libre accès sur comment opérer des systèmes de manière fiable.

[Lire les livres de la collection SRE](https://sre.google/books/)

**Sécurité - ANSSI et Univ Savoie**. Pour la sécurité, aller voir du côté de l'ANSSI ou des cours d'école d'ingé en libre accès :

[MOOC de l'ANSSI](https://secnumacademie.gouv.fr/) - [Guide bonnes pratiques ANSSI](https://www.ssi.gouv.fr/entreprise/bonnes-pratiques/) - [Cours Cryptologie et Sécurité](http://www.lama.univ-savoie.fr/pagesmembres/lachaud/Cours/INFO006/Cours/cours.pdf)

**Trouver les livres de référence**. Dans chaque domaine, et parfois pour des technologies installées, des livres de référence existent. Ce sont souvent des ressources beaucoup plus efficaces qu'un simple tutoriel sur un blog.

[Liste de livres](https://notes.shichao.io/books/) - [Exemple Linux](https://man7.org/tlpi/) - [Exemple Kubernetes](https://www.manning.com/books/kubernetes-in-action)

**MOOC**. Il existe pas mal de MOOC en informatique, que ce soit en français comme en anglais, sur des plateformes spécialisées comme une playlist Youtube.

[Exemple "France Université Numérique"](https://www.fun-mooc.fr/fr/cours/?limit=21&offset=0&subjects=2787)

**Debian**. Debian est une distribution Linux. Une fois que vous maitrisez les bases, vous pourriez vouloir comprendre en profondeur une de ces distributions.
Debian publie des "cahiers" qui ont le bon goût de permettre à ces lecteur-ices de ne pas s'éparpiller en cherchant des fragments d'informations à droite et à gauche sur Internet.

[Le cahier du débutant sur Debian](https://lescahiersdudebutant.arpinux.org/bullseye-fr/les_cahiers_du_debutant.html) - [Le cahier de l'administrateur Debian](https://debian-handbook.info/browse/fr-FR/stable/)

# Les documentations des autres CHATONS

Recommandé pour apprendre :
 - [Picasoft](https://wiki.picasoft.net/doku.php?id=technique:start) - Une approche très didactique, avec des schémas et en partant des bases. Je recommande si vous coommencez, vous apprendrez plein de choses.
 - [Automario](https://www.automario.eu/doc/) - Suppose une maitrise basique de Linux, docker et des logiciels serveurs standards (exemple : Apache). À partir de là, c'est très didactique !
 - [Evolix](https://wiki.evolix.org/) - Pour les utilisateur-ices avancées mais documente des technos un peu exotiques / plus rares, par exemple VRRP
 - [Lautre.net](https://www.lautre.net/spip.php?article4) - Intéressant pour voir à quoi ça ressemble un hébergeur à l'ancienne
 - [Nubo](https://nubo.coop/fr/news/2021-07-20_techtransparency/) - Seulement un court article mais avec un angle inhabituel, celui de la transparence technique

Les autres, pour la curiosité : [Altertek](https://docs.altertek.org/#/) - [Anancus](https://anancus.ynh.fr/wiki/documentations) - [Association 42l](https://42l.fr/Rapport-technique) - [Bastet de Parinux](https://wiki.parinux.org/) - [Bechamail](https://bechamail.fr/site/#infrastructure-1) - [Caracos](https://caracos.net/caraweb/fr/technique) - [Chalec de Association Libre En Commun](https://git.a-lec.org/a-lec/commissions/infrastructure/doc-infra) - [Clawd](https://clawd.fr/transparence) - [Clis XXI](https://doc.cliss21.com/wiki/Accueil) - [Distrilab](https://distrilab.fr/?Technique) - [Domaine Public](https://doc.domainepublic.net/) - [Enough](https://enough-community.readthedocs.io/en/latest/) - [Exarius](https://exarius.org/wiki/tech/) - [Facil](https://code.facil.services/facil/ansible) - [FLAP](https://docs.flap.cloud/) - [Framasoft](https://docs.framasoft.org/fr/) ou [Framacloud](https://framacloud.org/fr/) - [Galilee](https://galilee.eedf.fr/aide-galilee/) et son [git](https://framagit.org/eedf_intercomcom) - [Garbaye](https://garbaye.fr/docs/) - [Gozdata](https://gozdata.log.bzh/) aka Gozmail - [Greli.net](https://greli.net/doku.php) - [Immae.eu](https://git.immae.eu/?p=perso/Immae/Config/Nix.git;a=tree) -  [Libre.sh](https://libre.sh/) aka Indie Hosters - [Infini](https://wiki.infini.fr/index.php/Accueil) - [Isidorus](https://wiki.isidorus.fr/infra) - [Jabber](https://wiki.jabberfr.org/Accueil) - [Katzei](https://katzei.fr/Informations-techniques/Auto-h%C3%A9bergement.html) - [Kaz](https://wiki.kaz.bzh/) - [Le cloud girofle](https://framagit.org/CloudGirofle/girofle.cloud/-/wikis/home) - [Ma data](https://madata.defis.info/techniques.html) - [liberta.vip](https://doc.liberta.vip/tech/infrastructure) - [Libreon](https://libreon.fr/infra) - [libreservice.eu](https://forge.devinsy.fr/libre-service.eu/documentation) - [Numericloud](https://numericloud.eu/documentation/) - [opendoor](https://opendoor.fr/pages/documentation.html) - [Le pic](https://www.le-pic.org/spip.php?article811) - [ResiLien](https://notes.resilien.fr/s/R%C3%A9silien.md#) - [Retzo](https://retzo.net/services/hebergement/) - [Roflcopter](https://wtf.roflcopter.fr/chatons.html) - [Sans nuage](https://wiki.arn-fai.net/benevoles:menu) par Alsace Réseau Neutre - [Sequanux](https://chaton.sequanux.org/fr/documentation) - [siick.fr](https://wiki.siick.fr/doku.php?id=start) - [Deblan](https://wiki.deblan.org/doku.php?id=service-hebergement:offre) - [SimpleHosting.me](https://simplehosting.me/lhebergement/) - [Sleto](https://wiki.sleto.net/doku.php) - [Tedomum](https://www.tedomum.net/documentation/infra/) - [Tila.im](https://thefool.tila.im/) - [Underworld](https://www.underworld.fr/infra) - Zourit : [logiciels](https://zourit.net/logiciels/) et [sauvegardes](https://zourit.net/sauvegardes/)
