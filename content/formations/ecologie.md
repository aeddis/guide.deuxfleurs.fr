---
title: "Écologie"
description: "Écologie : comment tenir un discours pertinent en tant qu'hébergeur"
weight: 31
extra:
  parent: 'formations/conf.md'
---

**Animateur :** Quentin de Deuxfleurs  
**Prise de note :** Margaux du Cloud de Girofle  
**Compte-rendu :** Quentin de Deuxfleurs  
**Document source :** [Libreto Camps CHATONS 2022](https://libreto.sans-nuage.fr/camp-chatons-2022/lundi+22+-+ateliers)

# Les limites de notre démarche écologique

**Dur de porter un discours décroissant dans la tech** - Discours majoritaire GAFAM: on mise sur l'efficacité énergétique, réduction de la conso des data center. Ca n'est pas durable, internet au sens large ne peut pas continuer d'exister sous sa forme actuelle. Notion de "commun négatif", un commun dont les impacts sont néfastes.
Comment porter un discours du ralentissement global? celui des terminaux, des tuyaux, du nombre de personnes qui en dépendent...
Discours pas (encore) audible dans le milieu de la tech? pas de représentation?

Comment faire converger le fait qu'on souhaite être "+ écologique" et qu'on fasse partie d'un collectif dont le numérique est un élément central ?

**Le libre, aussi responsable ?** - Les GAFAM n'existeraient pas aujourd'hui sous cette forme sans le libre. Est-ce que le libre a pas perdu sa force subversive? ne sert pas la stratégie des GAFAM?
Quelle distribution libre fonctionne sur les vieux terminaux? c'est pas non plus une priorité pour les gros éditeurs du libre.



**La hiérarchisation des priorités** - CHATONS n'est pas un collectif écologique à la base, son action vise aussi d'autres fins égalements. Comment hiérarchiser ces objectifs ? À quels points sont ils orthogonaux ou alors complémentaires ?
 
*L'exemple des luttes* - Framasoft et PeerTube  encouragent l'usage du numérique et de la vidéo, à la fin ça consomme plus, même si on reste moins impactant que Youtube et qu'on propose des fonctionnalités pour réduire l'impact environnemental.
Le numérique est un outil politique qui permet de partager des luttes, dont une partie n'aurait pas été possibles sans.
S'extraire du numérique est-ce que c'est pas un truc de privilégié? En faisant sobre et résilient, est-ce qu'on peut faire passer nos message ?

*L'exemple de l'accessibilité* - 
L'accessibilité permet la sobriété: les sites internet épurés, sans tous les éléments marketing, sont à la fois beaucoup plus accessibles (lisibles par un lecteur d'écran) et plus légers, donc plus écologiques!

# Aborder la question sous forme collective


**Prendre en considération la matérialité** - La vraie question c'est l'alimentation des data centers et le matériel. 

**Porter un regard critique** - Il y a plein de chose dans notre domaine qu'on prend pour acquis, qu'on ne remet jamais en question, et pourtant qui poussent à une forme ou une autre d'obsolescence. Un de ces exemple concerne les mises à jour : anciens systèmes plus supportés, nouveaux logiciels trop gourmands, reste du réseau qui n'est plus compatible avec les anciens ordinateurs pas à jour. Dans ce domaine, la sécurité tient une responsabilité toute particulière : elle rend les mises à jour inévitables tout en imposant un coût de calcul non négligeable lié au chiffrement.

*Note : si les processeurs ont des optimisations pour le chiffrement, ce n'était pas le cas il y a 10 ans, deuxfleurs a des machines en production et voit sur ses graphes le coût du chiffrement dès qu'on a de la charge*

Pour la sécurité sans limite, on peut faire une analogie avec notre habitation. Là où on habite, la sécurité est souvent très faillible : serrure crochetables, parfois on oublie de fermer la porte, mais ça va, je fais confiance au monde autour de moi. Sur Internet, tout le monde peut communiquer avec ma machine et je dois craindre la terre entière, est-ce qu'il ne faudrait pas avoir une démarche plus locale ?

*Ne faudrait-il pas questionner fortement le concept même de mise à jour ? Descendre la sécurité de son piedestal ?*

**Hégémonie culturelle** - 
Ça se comprend en regard d'un des limites de notre démarche écologique : le fait qu'on est pas ou peu de représentation dans la tech' d'entités qui se revendiquent décroissantes. Reviennent souvent le [Solar Low Tech Magazine](https://solar.lowtechmagazine.com/) comme exemple, on peut noter le [Solar Protocol](http://solarprotocol.net/index.html) aussi, et ces initiatives sont limités à une autonomie technique en énergie, dont on est pas sûr qu'elle soit plus durable vu les coûts à la fabrication. À nuancer cependant, le Solar Low Tech Magazine porte un discours décroissant en parallèle de son approche technique, qui est basée sur la récupération et le minimalisme en plus du solaire. Bref, est-ce que Solar Low-Tech Magazine devrait nous inspirer ? Comment ?


**S'opposer** - On ne laisse pas forcément le choix aujourd'hui de se passer des services. La question des usages est importante mais il faut aller taper plus haut, il y a des choses imposées.
Les CHATONS pourraient se positionner contre la 5G par exemple. 

[↣ Notre article "5 échecs de la 5G"](https://plume.deuxfleurs.fr/~/PiedDeVent/5-%C3%A9checs-de-la-5g)  
[↣ "La controverse de la 5G", rapport par Gauthier Roussilhe](https://gauthierroussilhe.com/ressources/la-controverse-de-la-5g)

L'influence de la technique sur les usages. Argument final pour la 5G: saturation des réseaux qui risquent de tomber ce que personne ne veut.
On pourrait dire coté CHATONS qu'on hébergera moins de contenu vidéo: en s'opposant à certains usages, ça fait levier pour s'opposer à la technique.


*Point de vigilance* : ne pas accabler/pointer du doigt les personnes précaires, qui sont les plus écolos/sobres.

# Les actions à mener en commun

**Se projeter dans le futur** - S'inspirer des fictions écrites à plusieurs mains comme Bâtir Aussi, des Ateliers de l'Antémonde. Manque d'un imaginaire, d'un discours global. Un texte de positionnement? une réflexion porté par un GT CHATONS?

**Documenter nos pratiques et démarches** - Le faire bien en s'appuyant sur et en diffusant des ressources fiables - s'appuyer sur des outils d'éduc pop comme l'arpentage pour construire notre pensée collective

**Créer un Groupe de Travail** - Sur la question de l'écologie, Anne-So et Max lancent un appel sur le forum pour une première rencontre :)
Dans le cadre du travail du GT Ecologie : lister les ressources qui nous semblent pertinentes pour créer un "contre-discours" à celui du capitalisme qui essaie de se "réconcilier" avec l'écologie, mais avec les angles qui l'arrange.

**Emancip'Asso** - Intégrer une partie sur l'impact environnemental dans les prochaines sessions du MOOC CHATONS (en lien avec Emancip'Asso)


# Les démarches déjà en cours mais pas forcément documentées

**Accompagner à la dénumérisation** - 
Comment accompagner les utilisateurs à se dénumériser? on se pose la question au chaton Cloud Girofle.

**S'opposer à la vidéo** - pour s'opposer au discours culpabilisant et au faux dilemme posé par les opérateurs. En retour, faire émerger des modes de communications différents : blogs, podcasts, etc.


**Ne pas créer d'obsolescence, créer/utiliser des protocoles simples/retro-compatibles** - En créeant ou installant de nouveaux outils, on peut créer de l'obsolescence sans s'en rendre compte. Par exemple, un CHATONS qui déploie une nouvelle application de chat plus gourmande côté client peut pousser au renouvellement de l'ordinateur de ses usager-es. Pareil pour des sites webs trop lourds. Le même problème se pose au niveau des protocoles : tous les smartphones, même les vieux d'il y a 10 ans, supportent le protocole email actuel. Ce n'est pas vrai pour nos applications de chat : même si une vieille version est installée sur le téléphone, ce dernier sera incompatible avec le serveur qui a la dernière version. Voir la vidéo de Numerama qui ressort le premier iPhone pour comprendre.


# Ressources

[↣ La sobriété technologique par les logiciels libres](https://fyouzan.ci/index.php?post/2019/04/07/La-sobriete-technologique-par-les-logiciels-libres)  
[↣ L'insoutenable impact environnmental de la raclette (vidéo)](https://www.dailymotion.com/video/x8asnpm)  
[↣ Faire sa part, étude de Carbone 4](http://www.carbone4.com/wp-content/uploads/2019/06/Publication-Carbone-4-Faire-sa-part-pouvoir-responsabilite-climat.pdf)
