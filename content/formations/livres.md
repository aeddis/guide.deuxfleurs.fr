---
title: "Livres"
description: "Livres"
weight: 10
extra:
  parent: 'formations/mediatheque.md'
---

🇫🇷 **Ivan Illich, « La convivialité », 1973, Seuil.**

![Couverture de la Convivialité d'Illich](/img/cover/convivialite.jpg)
*L'analyse critique de la société industrielle doit beaucoup à Ivan Illich. Il est l'un des premiers à avoir dénoncé le productivisme, le culte de la croissance, l'apologie de la consommation et toutes les formes d'aliénation nées du mode de production capitaliste. "La Convivialité" montre comment l'organisation de la société tend à produire des consommateurs passifs, qui ont délégué aux institutions le pouvoir de décider et renoncé à assumer la responsabilité des orientations de leur société. Cette analyse critique se transforme en un manifeste. Il s'agit de réveiller politiquement les citoyens endormis, afin qu'ils se réapproprient leur destin.* 

[Fiche Babelio](https://www.babelio.com/livres/Illich-La-Convivialite/2485) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb43897048t)


🇫🇷 **Joël de Rosnay, « Le macroscrope &ndash; vers une vision globale », 1975, Seuil.**

![Couverture du Macroscope](/img/cover/macroscope.jpg)
*Qu'y a-t-il de commun entre l'écologie, le système économique, l'entreprise, la ville, l'organisme, la cellule ? Rien, si on se contente de les examiner avec l'instrument habituel de la connaissance, l'approche analytique. Mais beaucoup, en revanche, si l'on dépasse cette démarche classique pour faire ressortir les grandes règles d'organisation et de régulation de tous ces "systèmes". Pour Joël de Rosnay, l'instrument symbolique de cette nouvelle manière de voir, de comprendre et d'agir est le "macroscope", qui devrait être aussi précieux aujourd'hui aux grands responsables de la politique, de la science, de l'industrie, et à chacun de nous, que le sont le microscope et le télescope pour la connaissance scientifique de l'univers.*

[Fiche Babelio](https://www.babelio.com/livres/Rosnay-Le-macroscope/2738) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb437965432)

🇫🇷 **Normand Baillargeon, « L'Ordre moins le Pouvoir &ndash; Histoire et actualité de l'anarchisme », 2001, Agone, Troisième édition.**

![Couverture de L'Ordre moins le Pouvoir](/img/cover/pouvoir.jpg)
*Affirmez que vous êtes anarchiste et presque immanquablement on vous assimilera à un nihiliste, à un partisan du chaos voire à un terroriste.
Or, il faut bien le dire : rien n'est plus faux que ce contresens, qui résulte de décennies de confusion savamment entretenue autour de l'idée d'anarchisme. En première approximation, disons que l'anarchisme est une théorie politique au cœur vibrant de laquelle loge l'idée d'anti-autoritarisme, c'est-à-dire le refus conscient et raisonné de toute forme illégitime d'autorité et de pouvoir. Une vieille dame ayant combattu lors de la guerre d'Espagne disait le plus simplement du monde : " Je suis anarchiste : c'est que je n'aime ni recevoir ni donner des ordres."
On le devine, cette idée est impardonnable, cet idéal inadmissible pour tous les pouvoirs. On ne l'a donc ni pardonné ni admis.*

[Fiche Babelio](https://www.babelio.com/livres/Baillargeon-Lordre-moins-le-pouvoir--Histoire-et-actualite-d/12412) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb41377101w)

🇫🇷 **Irénée Régnauld, Yaël Benayoun, « Technologies partout, démocratie nulle part : plaidoyer pour que les choix technologiques deviennent l'affaire de tous », 2020, FYP éditions.**

![Couverture de technologie partout démocratie nulle part](/img/cover/techno-partout.jpg)
*L’urgence climatique, l’ubérisation, l’économie des petits boulots, les smart cities et la surveillance algorithmique nous ont brutalement fait prendre conscience des répercussions dramatiques des technologies. Alors que le progrès était censé servir le bien commun, il nous échappe. Nous le subissons. Malgré cela, la réponse apportée à tous les problèmes économiques et sociaux se borne à des solutions purement techniques. Irénée Régnauld et Yaël Benayoun révèlent et dénoncent les dogmes et les manœuvres qui permettent aux industries et aux pouvoirs publics de maintenir les citoyens et les travailleurs à l’écart des choix technologiques, en excluant tout processus démocratique. Ils montrent que notre arsenal juridique et nos institutions apeurées, voire serviles, sont incapables de contrer les servitudes imposées par les plateformes et les industries hyper capitalistes. Pour sortir de cette confiscation du progrès, les auteurs proposent des actions concrètes et réalistes qui replacent le débat démocratique et les revendications citoyennes au cœur du développement technologique, afin que la question du progrès devienne l’affaire de tous.*

[Fiche Babelio](https://www.babelio.com/livres/Benayoun-Technologies-partout-democratie-nulle-part/1276508) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb46655701v)

🇫🇷 **Stéphane Bortzmeyer, « Cyberstructure : L'Internet, un espace politique », 2018, C&F éditions.**

![Couverture de Cyberstructure](/img/cover/cyberstructure.jpg)
*Une grande partie des activités humaines se déroule aujourd'hui sur l'Internet. On y fait des affaires, de la politique, on y bavarde, on travaille, on s'y distrait, on drague... L'Internet n'est donc pas un outil qu'on utilise, c'est un espace où se déroulent nos activités.
Les outils de communication ont d'emblée une dimension politique : ce sont les relations humaines, les idées, les échanges commerciaux ou les désirs qui s'y expriment. L'ouvrage de Stéphane Bortzmeyer montre les relations subtiles entre les décisions techniques concernant l'Internet et la réalisation — ou au contraire la mise en danger - des droits fondamentaux. Après une description précise du fonctionnement de l'Internet sous les aspects techniques, économiques et de la prise de décision, l'auteur évalue l'impact des choix informatiques sur l'espace politique du réseau.
Un ouvrage pour appuyer une citoyenneté informée, adaptée aux techniques du XXIe siècle et en mesure de défendre les droits humains.*

[Fiche Babelio](https://www.babelio.com/livres/Bortzmeyer-Cyberstructure--LInternet-un-espace-politique/1108683) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb45637569v)


🇫🇷 **Cornelius Castoriadis, « L'institution imaginaire de la société », 1975, Seuil.**

![Couverture de L'institution imaginaire de la société](/img/cover/imaginaire.jpg)
*Dans cet essai, qui fut son maître livre, Cornélius Castoriadis s'attache à un réexamen fondamental des bases philosophiques de la pensée révolutionnaire. Il retrouve, chez Marx notamment, les préceptes de la "pensée héritée", cette logique identitaire qui, depuis les Grecs, inspire la philosophie classique. Et il propose une "auto-institution" de la société qui laisserait cours, enfin, à l'imaginaire radical. Fruit d'une analyse précise de l'histoire et des luttes sociales à l'heure de l'effondrement des repères traditionnels de la révolution, ce livre, devenu un classique, propose un point de départ pour penser à neuf le projet de transformation de la société.*

[Fiche Babelio](https://www.babelio.com/livres/Castoriadis-Linstitution-imaginaire-de-la-societe/17835) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb345747395)


🇬🇧 **Mélanie Dulong de Rosnay, Félix Tréguer, et al, « Telecommunications Reclaimed: A hands-on guide to networking communities », 2019, autoédition.**

![Couverture de Telecommunications Reclaimed](/img/cover/telco.jpg)
*This book is a guide on how to build a community network, a shared local telecommunications infrastructure, managed as a commons, to access the internet and other digital communications services. It was written collectively by a group of community network pioneers in Europe, activists and researchers during a writing residency week held in Vic, Catalonia in October 2018. Meant for a wide audience, the book includes practical knowledge illustrated by several hands-on experiences – a set of 32 real- life stories – as well as legal, technical, governance, economic and policy material extracted from netCommons, a three-year- long research project supported by the European Commission. Its goal is to guide the reader through a set of actions aimed at setting up and fostering the growth of a community network, but also, for policy makers, local administrations and the general public, to create the right conditions to let community networks bloom and flourish.*

[Lire en ligne](https://netcommons.eu/?q=telecommunications-reclaimed)


🇺🇸 **Edward Snowden, « Permanent Record », 2019, Metropolitan Books.**

![Couverture de Permanent Record](/img/cover/permanent-record.jpg)
*Edward Snowden, the man who risked everything to expose the US government’s system of mass surveillance, reveals for the first time the story of his life, including how he helped to build that system and what motivated him to try to bring it down.*

[Fiche goodreads](https://www.goodreads.com/book/show/46223297-permanent-record)

🇫🇷 **Georges Bernanos, « La France contre les robots », 1944, Castor Astral.**

![Couverture de La France contre les robots](/img/cover/France-contre-robots.jpg)
*Plus d'un demi-siècle après sa parution, ce pamphlet reste d'une incroyable actualité. Cette apologie de la liberté est un défi jeté aux idolâtries du profit et de la force. Georges Bernanos, dans une violente critique de la société industrielle, s'adresse à la «France immortelle» face à la «France périssable», celle des combinaisons politiques et des partis. L'auteur y estime que le progrès technique forcené limite la liberté humaine. Bernanos conteste l'idée selon laquelle la libre entreprise conduirait automatiquement au bonheur de l'humanité. En effet, selon lui, «il y aura toujours plus à gagner à satisfaire les vices de l'homme que ses besoins». Visionnaire, il explique ainsi qu'«un jour, on plongera dans la ruine du jour au lendemain des familles entières parce qu'à des milliers de kilomètres pourra être produite la même chose pour deux centimes de moins à la tonne» ; une étonnante préfiguration de ce que seront les délocalisations un demi-siècle plus tard !*

[Fiche Babelio](https://www.babelio.com/livres/Bernanos-La-France-contre-les-robots/34162) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb416243136)

🇫🇷 **Timothy Mitchell, « Carbon Democracy », 2013, La Découverte, traduit de l'anglais 🇬🇧.**

![Couverture de Carbon Democracy de Timothy Mitchell](/img/cover/carbon_democracy.jpg)
*Ceci est un « livre à thèse », une thèse forte et iconoclaste, qui déplace radicalement notre vision de l’histoire du XXe siècle : les contours et les transformations des régimes politiques dits « démocratiques » ont été largement déterminés par les propriétés géophysiques des principales énergies carbonées, le charbon d’abord, puis le pétrole.
Ainsi, la pesanteur du charbon, la nécessité de l’extraire des mines puis de le charger dans des convois, etc. ont donné à ses producteurs un pouvoir considérable ; en utilisant la menace d’en interrompre les flux, ils créèrent syndicats et partis de masse, à l’origine des premières démocraties de l’ère moderne. En face, les classes dominantes ont cherché à organiser la transition énergétique à l’échelle mondiale. En effet, grâce à sa fluidité, sa légèreté et son exceptionnelle concentration en énergie, le pétrole permettait de contourner les réseaux et pouvoirs anciens.
Un autre régime s’est ainsi progressivement mis en place, dans lequel la vie politique s’est retrouvée anémiée, la paix sociale et la prospérité des « démocraties » occidentales ont reposé sur l’autoritarisme moyen-oriental, et où la croissance illimitée s’est transformée en religion. Aujourd’hui, ce système est au bord de l’effondrement et nous pose une question cruciale : comment les énergies postpétrole pourront-elles donner naissance à des régimes réellement démocratiques ?*

[Fiche Babelio](https://www.babelio.com/livres/Mitchell-Carbon-Democracy/931332) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb43611319m)

🇫🇷 **Michel Foucault, « Surveiller et punir », 1975, Gallimard.**

![Couverture de Surveiller et Punir de Michel Foucault](/img/cover/surveiller-et-punir.jpg)
*Surveillance, exercices, manoeuvres, notations, rangs et places, classements, examens, enregistrements, toute une manière d'assujettir les corps, de maîtriser les multiplicités humaines et de manipuler leurs forces s'est développée au cours des siècles classiques, dans les hôpitaux, à l'armée, dans les écoles, les collèges ou les ateliers : la discipline. La prison est à replacer dans la formation de cette société de surveillance.
La pénalité moderne n'ose plus dire qu'elle punit des crimes ; elle prétend réadapter des délinquants. Peut-on faire la généalogie de la morale moderne à partir d'une histoire politique des corps ?*

[Fiche Babelio](https://www.babelio.com/livres/Foucault-Surveiller-et-punir/1516) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb374882365.public)

🇫🇷 **Mathieu Labonde, Lou Malhuret, Benoît Piedallu et Axel Simon, « Internet et libertés », 2022, Vuibert.**

![Couverture d'Internet et libertés de Mathieu Labonde, Lou Malhuret, Benoît Piedallu et Axel Simon](/img/cover/internet-et-libertés.jpg)
*Partout où le numérique est venu changer nos vies, le respect de nos libertés fondamentales est un combat.
Pendant que Facebook, Google et compagnie se targuent de protéger nos données tout en les exploitant pour booster la publicité ciblée, les lois sécuritaires s’enchaînent et les expérimentations illégales aussi : des micros dans les rues, des tests de reconnaissance faciale dans les stades ou les transports, des drones aux mains des policiers… La dérive vient des pouvoirs publics autant que des entreprises.*

[Fiche Babelio](https://www.babelio.com/livres/Labonde-Internet-et-libertes--15-ans-de-combat-de-la-Quad/1451258) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb47143115g)

🇫🇷 **Fanny Lopez, « À bout de flux », 2022, Divergences.**

![Couverture de À bout de flux, de Fanny Lopez](/img/cover/a-bout-de-flux.jpg)
*Le numérique a un double : l’infrastructure électrique. Le rapport immédiat aux objets connectés (smartphone, ordinateur) invisibilise le continuum infernal d’infrastructures qui se cachent derrière : data centers, câbles sous-marins, réseaux de transmission et de distribution d’électricité. Alors que le numérique accompagne une électrification massive des usages, le système électrique dépend lui-même de plus en plus du numérique pour fonctionner. Pour comprendre ce grand système et imaginer comment le transformer, il nous faut aller au bout des flux, là où se révèle la matérialité des machines et des câbles.*

[Sur la boutique de l'éditeur](https://www.editionsdivergences.com/livre/a-bout-de-flux) — [Fiche Babelio](https://www.babelio.com/auteur/Fanny-Lopez/505984) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb471399555)

🇫🇷 **Maxime Guedj et Anne-Sophie Jacques, « Déclic », 2020, Les arènes.**

![Couverture de Déclic](/img/cover/déclic.jpg)
*Exploitation des données privées, surveillance généralisée, addiction au smartphone, disparition de pans entiers de l'économie... Les critiques du monde numérique ne cessent de s'amplifier; difficile d'ignorer les conséquences de l'utilisation d'Amazon, Facebook, Google, Instagram ou Netflix. Et s'il existait un autre Internet, respectueux de nos libertés ? L'ancien startuppeur Maxime Guedj et la journaliste Anne-Sophie Jacques proposent de s'engager sur une nouvelle voie.*

[Fiche Babelio](https://www.babelio.com/livres/Guedj-Declic/1201805) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb46530396g)


🇫🇷 **François Jarrige, « Technocritiques », 2016, La découverte.**

![Couverture de Technocritiques](/img/cover/technocritiques.jpg)
*La nuit, vous rêvez de casser des machines, mais n'osez pas vous en ouvrir à vos proches de peur de passer pour un dangereux _luddite_ à tendance _Amish_ ?<br>
L'historien François Jarrige est là pour vous ! Il a étudié des siècles de critiques des techniques de leur temps (du tissage mécanisé en passant par l'imprimerie) et en sort le glaçant constat : il y aurait de bonnes raisons de ne pas faire de l'innovation une fin en soi.<br>
Vous voilà prêt⋅e à briller en société tout en parlant technocritiques.*


[Sur le site de l'éditeur](https://www.editionsladecouverte.fr/technocritiques-9782707178237) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb45020905s)

🇫🇷 **Hannah Arendt, « La Crise de la Culture », 1972, Gallimard, traduit de l'anglais 🇺🇸.**

![Couverture de La Crise de la Culture](/img/cover/la-crise-de-la-culture.jpg)
*L'homme se tient sur une brèche, dans l'intervalle entre le passé révolu et l'avenir infigurable. Il ne peut s'y tenir que dans la mesure où il pense, brisant ainsi, par sa résistance aux forces du passé infini et du futur infini, le flux du temps indifférent.<br>
Chaque génération nouvelle, chaque homme nouveau doit redécouvrir laborieusement l'activité de pensée. Longtemps, pour ce faire, on put recourir à la tradition. Or nous vivons, à l'âge moderne, l'usure de la tradition, la crise de la culture.<br>
Il ne s'agit pas de renouer le fil rompu de la tradition, ni d'inventer quelque succédané ultra-moderne, mais de savoir s'exercer à penser pour se mouvoir dans la brèche.<br>
Hannah Arendt, à travers ces essais d'interprétation critique - notamment de la tradition et des concepts modernes d'histoire, d'autorité et de liberté, des rapports entre vérité et politique, de la crise de l'éducation -, entend nous aider à savoir comment penser en notre époque.*

[Sur le site de l'éditeur](https://www.gallimard.fr/Catalogue/GALLIMARD/Folio/Folio-essais/La-crise-de-la-culture) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb351964359)

🇫🇷 **Jeanne Guien, « Le consumérisme à travers ses objets », 2021, Divergences.**

![Couverture de Le Consumérisme à travers ses objets](/img/cover/le-consumerisme-a-travers-ses-objets.jpg)
*Qu'est ce que le consumérisme ? Comment s'habitue-t-on à surconsommer - au point d'en oublier comment faire sans, comment on faisait avant, comment on fera après ? Pour répondre à ces questions, Jeanne Guien se tourne vers des objets du quotidien : gobelets, vitrines, mouchoirs, déodorants, smartphones. Cinq objets auxquels nos gestes et nos sens ont été éduqués, cinq objets banals mais opaques, utilitaires mais surchargés de valeurs, sublimés mais bientôt jetés. En retraçant leur histoire, ce livre entend montrer comment naît le goût pour tout ce qui est neuf, rapide, personnalisé et payant. Car les industries qui fabriquent notre monde ne se contentent pas de créer des objets, elles créent aussi des comportements. Ainsi le consumérisme n'est-il pas tant le vice moral de sociétés gâtées qu'une affaire de production et de conception. Comprendre comment nos gestes sont déterminés par des produits apparemment anodins, c'est questionner la possibilité de les libérer.*

[Sur le site de l'éditeur](https://www.editionsdivergences.com/livre/le-consumerisme-a-travers-ses-objets) — [Fiche Babelio](https://www.babelio.com/livres/Guien-Le-consumerisme-a-travers-ses-objets/1383986) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb47035735v)


🇫🇷 **Julia Laïnae & Nicolas Alep, « Contre l'alternumérisme », 2020, La Lenteur.**

![Couverture de Contre l'alternumérisme](/img/cover/contre-lalternumerisme.jpg)
*L’alternumérisme a le vent en poupe : cyberminimalisme, smartphones équitables, inclusion numérique, ateliers de détox digitale, logiciel libre, open data, démocratie en ligne, neutralité du net… ils sont nombreux à croire, (ou à vouloir nous faire croire), que la nouvelle économie de l’information pourrait tourner à l’avantage de tous ou, du moins, qu’on pourrait en contenir les effets les plus néfastes. Ce livre détruit méthodiquement cette théorie dangereuse, appelle à s’extraire de l’utopie numérique, à refuser la numérisation du monde.*

[Fiche Babelio](https://www.babelio.com/livres/Lainae-Contre-lalternumerisme/1217230) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb465941050)

🇫🇷 **Celia Izoard, « Merci de changer de métier », 2020, La Dernière Lettre.**

![Couverture de Merci de Changer de Métier](/img/cover/merci-de-changer-de-metier.jpg)
*Interpeller directement des chercheurs, ingénieurs et startuppers sur les implications politiques de leur activité, tel est l'objet de ce livre, composé de lettres ouvertes rédigées dans un style piquant, qui mêle la satire et l'analyse. Celia Izoard ouvre ici un dialogue avec les concepteurs des nouvelles technologies pour les interroger sur le sens de leur travail et analyser l'impact social et écologique des grands projets industriels de la décennie, dans un monde en proie à la crise climatique et à l'exploitation au travail. Elle les enjoint à « changer de métier », à l'instar d'Olivier Lefebvre, salarié d'une start-up de véhicules autonomes qui raconte à la fin de l'ouvrage son chemin vers la démission.*

[Sur le site de l'éditeur](https://ladernierelettre.fr/produit/merci-de-changer-de-metier/) — [Fiche Babelio](https://www.babelio.com/livres/Izoard-Merci-de-changer-de-metier/1265539) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb46837084v)

🇫🇷 **Philippe Bihouix, « L'âge des Low-Tech », 2014, Seuil.**

![Couverture de L'âge des Low-Tech](/img/cover/lage-des-low-tech.jpg)
*Face aux signaux alarmants de la crise environnementale globale – changement climatique, effondrement de la biodiversité, dégradation des sols, pollution généralisée, tensions sur l'énergie et les matières premières –, nous fondons nos espoirs sur les technologies " vertes " et le numérique. <br>
Plus consommatrices de ressources rares, plus difficiles à recycler, trop complexes, ces nouvelles technologies nous conduisent pourtant, à terme, dans l'impasse. Ce livre démonte les mirages des innovations high tech, et propose de questionner la course en avant technologique en développant les low tech, les " basses technologies ", plus sobres et plus résilientes. Il ne s'agit pas de revenir à la bougie, mais d'explorer les voies possibles vers un système économique et industriel compatible avec les limites planétaires.*

[Sur le site de l'éditeur](https://www.seuil.com/ouvrage/l-age-des-low-tech-philippe-bihouix/9782021160727) — [Fiche Babelio](https://www.babelio.com/livres/Bihouix-Lage-des-low-tech/1339788) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb43812685t)

🇫🇷 **Jérome Denis & David Pontille, « Le Soin des Choses », 2022, La Découverte.**

![Couverture de Le Soin des Choses](/img/cover/le-soin-des-choses.jpg)
*Qu’ont en commun une chaudière, une voiture, un panneau de signalétique, un smartphone, une cathédrale, une œuvre d’art, un satellite, un lave-linge, un pont, une horloge, un serveur informatique, le corps d’un illustre homme d’État, un tracteur ? Presque rien, si ce n’est qu’aucune de ces choses, petite ou grande, précieuse ou banale, ne perdure sans une forme d’entretien.<br>
Tout objet s’use, se dégrade, finit par se casser, voire par disparaître. Pour autant, mesure-t-on bien l’importance de la maintenance ? Contrepoint de l’obsession contemporaine pour l’innovation, moins spectaculaire que l’acte singulier de la réparation, cet art délicat de faire durer les choses n’est que très rarement porté à notre attention.<br>
Ce livre est une invitation à décentrer le regard en mettant au premier plan la maintenance et celles et ceux qui l’accomplissent. En suivant le fil de différentes histoires, ses auteurs décrivent les subtilités du « soin des choses » pour en souligner les enjeux éthiques et la portée politique. Parce que s’y cultive une attention sensible à la fragilité et que s’y invente au jour le jour une diplomatie matérielle qui résiste au rythme effréné de l’obsolescence programmée et de la surconsommation, la maintenance dessine les contours d’un monde à l’écart des prétentions de la toute-puissance des humains et de l’autonomie technologique. Un monde où se déploient des formes d’attachement aux choses bien moins triviales que l’on pourrait l’imaginer.*

[Sur le site de l'éditeur](https://www.editionsladecouverte.fr/le_soin_des_choses-9782348064838) — [Fiche Babelio](https://www.babelio.com/livres/Denis-Le-soin-des-choses/1446551) — [Notice BNF](https://catalogue.bnf.fr/ark:/12148/cb47175848g)

