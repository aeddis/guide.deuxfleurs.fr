---
title: Guide Deuxfleurs
sort_by: weight
---
# Découvrir

[Prise en main](@/prise_en_main/_index.md) - Ce manuel vous accompagne dans la découverte de nos outils. C'est par là que vous devriez commencer si vous venez d'arriver, on vous explique comment utiliser nos outils pour rester en contact avec votre famille, organiser une réunion avec votre association ou encore publier une tribune sur le web.

[Se former](@/formations/_index.md) - Ce manuel vous propose de vous former sur les questions portées par l'association, que ce soit sur l'impact social du numérique ou l'administration d'une machine Linux, avec dans l'idée que vous pourrez vous impliquer d'avantage dans nos activités après, en faisant des ateliers ou en participant à opérer les machines et les logiciels.

[Vie associative](@/vie_associative/_index.md) - Ce manuel traite de tout ce qui concerne l'association, comme ses aspects légaux, les délibérations, ou l'organisation des personnes.

[Infrastructures](@/infrastructures/_index.md) - Ce manuel documente la dimension matérielle du numérique chez Deuxfleurs. On y recense les ordinateurs, le lieu où ils sont, les connexions réseaux nécessaires, l'énergie consommée, l'impact de fabrication, de fin de vie, etc.

[Opérations](@/operations/_index.md) - Ce manuel recense notre savoir-faire technique, il a pour but d'accompagner nos opérateur·ices dans la réalisation de leurs tâches.

# Contribuer

Afin d'apporter une contribution impactante, il est recommandé de lire [Diátaxis](https://diataxis.fr/), un site web qui théorise la documentation. Pour le style d'écriture, les conseils de [StaticCMS](https://www.staticcms.org/docs/writing-style-guide) sont intéressants.

Pour contribuer à ce guide, le plus simple est d'agréger les modifications que vous voulez apporter dans un fichier texte ou LibreOffice et de nous l'envoyer par email à coucou (arobase) deuxfleurs.fr. Un membre de l'association reportera alors vos propositions sur le site web.

Si vous êtes plus expert·e, vous pouvez demander des accès à [l'interface d'administration](https://guide.deuxfleurs.fr/admin/) pour faire directement vos modifications. Si vous connaissez git, vous pourriez aussi vouloir [forker notre dépôt](https://git.deuxfleurs.fr/Deuxfleurs/guide.deuxfleurs.fr).
