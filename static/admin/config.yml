backend:
  name: gitea
  repo: deuxfleurs/guide.deuxfleurs.fr
  base_url: https://teabag.deuxfleurs.fr
  api_root: https://git.deuxfleurs.fr/api/v1
  branch: main

media_folder: 'static/img/'
site_url: https://guide.deuxfleurs.fr
display_url: https://guide.deuxfleurs.fr
locale: 'fr'

collections:
  - name: 'special'
    label: 'Accueil'
    editor:
      preview: false
    files:
      - name: accueil
        label: Accueil
        file: 'content/_index.md'
        fields:
        - label: Title
          name: title
          widget: string
        - label: Sort by
          name: sort_by
          widget: string
        - label: Body
          name: body
          widget: markdown


  - name: 'prise_en_main'
    label: 'Prise en main'
    description: "Les tutoriels pour se lancer"
    folder: 'content/prise_en_main'
    editor:
      preview: false
    create: true
    view_filters:
      - label: 'Brouillons'
        field: draft
        pattern: true
    view_groups:
      - label: 'Section'
        field: 'extra.parent'
        pattern: '.*'
    fields:
      - label: Titre menu
        name: title
        widget: string
      - label: Titre page (description)
        name: description
        widget: string
      - label: Poids
        name: weight
        default: 50
        widget: number
      - label: Brouillon (ne pas publier encore)
        name: draft
        widget: boolean
        default: false
      - label: Date
        name: date
        widget: datetime
        date_format: 'dd/MM/yyyy' # e.g. 24/12/2022
        time_format: false
        format: 'yyyy-MM-dd' # e.g. 2022-12-24
      - label: Extra
        name: extra
        widget: object
        fields:
          - name: parent
            label: Parent
            widget: relation
            collection: 'prise_en_main'
            value_field: 'prise_en_main/{{filename}}.{{extension}}'
            search_fields: [ 'title', 'filename', 'slug' ]
            default: 'prise_en_main/_index.md'
            display_fields: ['{{title}} ({{filename}})', 'filename']
      - label: Corps
        name: body
        widget: markdown

  - name: 'formations'
    label: 'Se former'
    description: 'Des ressources pour apprendre en profondeur'
    folder: 'content/formations'
    editor:
      preview: false
    create: true
    view_filters:
      - label: 'Brouillons'
        field: draft
        pattern: true
    view_groups:
      - label: 'Section'
        field: 'extra.parent'
        pattern: '.*'
    fields:
      - label: Titre
        name: title
        widget: string
      - label: Poids
        default: 50
        name: weight
        widget: number
      - label: Brouillon (ne pas publier encore)
        name: draft
        widget: boolean
        default: false
      - label: Date
        name: date
        widget: datetime
        date_format: 'dd/MM/yyyy' # e.g. 24/12/2022
        time_format: false
        format: 'yyyy-MM-dd' # e.g. 2022-12-24 
      - label: Extra
        name: extra
        widget: object
        fields:
          - name: parent
            label: Parent
            widget: relation
            collection: 'formations'
            value_field: 'formations/{{filename}}.{{extension}}'
            search_fields: [ 'title', 'filename', 'slug' ]
            default: 'formations/_index.md'
            display_fields: ['{{title}} ({{filename}})', 'filename']
      - label: Corps
        name: body
        widget: markdown

  - name: 'vie_associative'
    label: 'Vie associative'
    description: "Tout ce qui a rapport à l'organisation et l'administration de l'asso"
    folder: 'content/vie_associative'
    editor:
      preview: false
    create: true
    view_filters:
      - label: 'Brouillons'
        field: draft
        pattern: true
    view_groups:
      - label: 'Section'
        field: 'extra.parent'
        pattern: '.*'
    fields:
      - label: Titre
        name: title
        widget: string
      - label: Poids
        default: 50
        name: weight
        widget: number
      - label: Brouillon (ne pas publier encore)
        name: draft
        widget: boolean
        default: false
      - label: Date
        name: date
        widget: datetime
        date_format: 'dd/MM/yyyy' # e.g. 24/12/2022
        time_format: false
        format: 'yyyy-MM-dd' # e.g. 2022-12-24 
      - label: Extra
        name: extra
        widget: object
        fields:
          - name: parent
            label: Parent
            widget: relation
            collection: 'vie_associative'
            value_field: 'vie_associative/{{filename}}.{{extension}}'
            search_fields: [ 'title', 'filename', 'slug' ]
            default: 'vie_associative/_index.md'
            display_fields: ['{{title}} ({{filename}})', 'filename']
      - label: Corps
        name: body
        widget: markdown

  - name: 'infrastructures'
    label: 'Infrastructures'
    description: "Des informations d'intéret générale sur la technique"
    folder: 'content/infrastructures'
    editor:
      preview: false
    create: true
    view_filters:
      - label: 'Brouillons'
        field: draft
        pattern: true
    view_groups:
      - label: 'Section'
        field: 'extra.parent'
        pattern: '.*'
    fields:
      - label: Titre
        name: title
        widget: string
      - label: Poids
        default: 50
        name: weight
        widget: number
      - label: Brouillon (ne pas publier encore)
        name: draft
        widget: boolean
        default: false
      - label: Date
        name: date
        widget: datetime
        date_format: 'dd/MM/yyyy' # e.g. 24/12/2022
        time_format: false
        format: 'yyyy-MM-dd' # e.g. 2022-12-24 
      - label: Extra
        name: extra
        widget: object
        fields:
          - name: parent
            label: Parent
            widget: relation
            collection: 'infrastructures'
            value_field: 'infrastructures/{{filename}}.{{extension}}'
            search_fields: [ 'title', 'filename', 'slug' ]
            default: 'infrastructures/_index.md'
            display_fields: ['{{title}} ({{filename}})', 'filename']
      - label: Corps
        name: body
        widget: markdown

  - name: 'operations'
    label: 'Opérations'
    description: "Base de connaissance technique à destination des operateur-ices"
    folder: 'content/operations'
    editor:
      preview: false
    create: true
    view_filters:
      - label: 'Brouillons'
        field: draft
        pattern: true
    view_groups:
      - label: 'Section'
        field: 'extra.parent'
        pattern: '.*'
    fields:
      - label: Titre
        name: title
        widget: string
      - label: Poids
        default: 50
        name: weight
        widget: number
      - label: Brouillon (ne pas publier encore)
        name: draft
        widget: boolean
        default: false
      - label: Date
        name: date
        widget: datetime
        date_format: 'dd/MM/yyyy' # e.g. 24/12/2022
        time_format: false
        format: 'yyyy-MM-dd' # e.g. 2022-12-24 
      - label: Extra
        name: extra
        widget: object
        fields:
          - name: parent
            label: Parent
            widget: relation
            collection: 'operations'
            value_field: 'operations/{{filename}}.{{extension}}'
            search_fields: [ 'title', 'filename', 'slug' ]
            default: 'operations/_index.md'
            display_fields: ['{{title}} ({{filename}})', 'filename']
      - label: Corps
        name: body
        widget: markdown
