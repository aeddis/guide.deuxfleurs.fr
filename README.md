# [guide.deuxfleurs.fr](https://guide.deuxfleurs.fr)

## Construire

```bash
zola build
```

## Déployer

```bash
aws s3 sync ./public s3://guide.deuxfleurs.fr
```

## Développer

```bash
zola serve
```
